<img src="https://gitlab.com/uploads/-/system/project/avatar/17463605/logo.jpg"/></br>

<br>
<br>

# Epic Forum project by Petar Popov and Petar Petrov!

## Description

```
end-to-end-project-pp_pp-team-3
├───avatars
├───src
│   ├───article
│   ├───auth
│   │   └───strategy
│   ├───comment
│   ├───common
│   │   ├───decorators
│   │   ├───exceptions
│   │   ├───filters
│   │   ├───guards
│   │   ├───pipes
│   │   └───types
│   ├───data
│   │   ├───entities
│   │   ├───migration
│   │   └───seed
│   ├───models               
│   │   ├───article
│   ├───comment
│   ├───friends
│   │   ├───user
│   │   └───votes
│   └───user       
├── .env
├── .eslintrc.js
├── .gitignore
├── .prettierrc
├── nest-cli.json
├── ormconfig.json
├── package-lock.json
├── package.json
├── PP_PP.postman_collection.json
├── README.md
├── tsconfig.build.json
└── tsconfig.json 
```

## Installation

```
git clone https://gitlab.com/Petyr5rov/end-to-end-project-pp_pp-team-3.git
```

1 . Setup MySQL Database with new Schema.
<br>

2 . Setup `.env` and `оrmconfig.json` files in Server folder. They need to be on root level in api folder where is `package.json` and other config files.

- `.env` file with your settings:

   ```typescript
       PORT=3000
       DB_TYPE=mysql
       DB_HOST=localhost
       DB_PORT=3306
       DB_USERNAME=root
       DB_PASSWORD=password
       DB_DATABASE_NAME=namedb
       EXPIRESIN=12h
       SECRETKEY=ppv3r7s3cret!pp
   ```

- `ormconfig.json` file with your settings:

   ```typescript
   {
    "type": "mysql",
    "host": "localhost",
    "port": 3306,
    "username": "root",
    "password": "password",
    "database": "namedb",
    "synchronize": false,
    "entities": ["src/data/entities/*.entity.ts"],
    "migrations": ["src/data/migration/*.ts"],
    "cli": {
      "entitiesDir": "src/data/entities",
      "migrationsDir": "src/data/migration"
    }
  }
   ```

4 . After files are setup go in terminal and run:

- `npm run startup`  -  this command does the following:

  - install all the dependencies (command should be run in the same folder where package.json is).
  - will execute  migrations sequentially ordered by timestamp.
  - Will add one admin and guest to the table users, add two articles to the article and add a comment to them.
  - will launch the application in dev mode.
 

 
5 . Start the Client from it's folder:

- `ng serve`

6 . You can test each of the functions with all the written tests.

- `npm run majestic`

7 . Use Postman

- In order to test the API use the Postman tool. We provided a postman collection with Project features and commands.

8 . EER Diagram

<img src="./server/avatars/diagram.png"/></br>

9 . Sample requests and response:
 - Users 
   - requests 
  ```
  GET / localhost:3000/users/1
  ```
   - response

  ```
    {
        "id": 1,
        "username": "Admin",
        "avatarUrl": "http://localhost:3000/avatars/download.png",
        "role": "ADMIN"
    }
  ```
 - Articles 
   - requests 
  ```
  GET / localhost:3000/articles?userId=2
  ```
   - response
  
  ```
    {
    "id": 1,
    "title": "Malkia Princ",
    "content": "This Article is flagged, please edit it's content or it will be deleted!",
    "createTime": "2020-04-10T16:48:04.510Z",
    "updateTime": "2020-04-10T18:45:41.000Z",
    "user": {
        "id": 2,
        "username": "test1new",
        "avatarUrl": "http://localhost:3000/avatars/download.png",
        "role": "ADMIN"
    },
    "comments": [
        {
            "id": 3,
            "content": "Test Test",
            "createTime": "2020-04-10T17:48:04.147Z",
            "updateTime": "2020-04-10T18:19:16.000Z",
            "user": {
                "id": 1,
                "username": "Admin",
                "avatarUrl": "http://localhost:3000/avatars/download.png",
                "role": "ADMIN"
            },
            "votes": []
        },
        {
            "id": 1,
            "content": "comment Malkia Princ",
            "createTime": "2020-04-10T16:48:04.858Z",
            "updateTime": "2020-04-10T16:48:04.858Z",
            "user": {
                "id": 2,
                "username": "test1new",
                "avatarUrl": "http://localhost:3000/avatars/download.png",
                "role": "ADMIN"
            },
            "votes": []
        }
    ],
    "votes": [
        {
            "positive": false,
            "user": {
                "id": 1,
                "username": "Admin",
                "avatarUrl": "http://localhost:3000/avatars/download.png",
                "role": "ADMIN"
            }
        }
    ]
}
  ```
  