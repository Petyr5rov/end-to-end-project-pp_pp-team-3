import {MigrationInterface, QueryRunner} from "typeorm";

export class ConvertContentToText1587636778239 implements MigrationInterface {
    name = 'ConvertContentToText1587636778239'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `comments` DROP COLUMN `content`", undefined);
        await queryRunner.query("ALTER TABLE `comments` ADD `content` varchar(1000) NOT NULL", undefined);
        await queryRunner.query("ALTER TABLE `comments` ADD UNIQUE INDEX `IDX_b6496f77981b10109fd0fff325` (`content`)", undefined);
        await queryRunner.query("ALTER TABLE `articles` DROP COLUMN `content`", undefined);
        await queryRunner.query("ALTER TABLE `articles` ADD `content` varchar(1000) NOT NULL", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `articles` DROP COLUMN `content`", undefined);
        await queryRunner.query("ALTER TABLE `articles` ADD `content` varchar(255) NOT NULL", undefined);
        await queryRunner.query("ALTER TABLE `comments` DROP INDEX `IDX_b6496f77981b10109fd0fff325`", undefined);
        await queryRunner.query("ALTER TABLE `comments` DROP COLUMN `content`", undefined);
        await queryRunner.query("ALTER TABLE `comments` ADD `content` text NOT NULL", undefined);
    }

}
