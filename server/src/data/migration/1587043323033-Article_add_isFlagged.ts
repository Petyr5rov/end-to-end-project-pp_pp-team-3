import {MigrationInterface, QueryRunner} from "typeorm";

export class ArticleAddIsFlagged1587043323033 implements MigrationInterface {
    name = 'ArticleAddIsFlagged1587043323033'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `articles` ADD `isLocked` tinyint NOT NULL DEFAULT 0", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `articles` DROP COLUMN `isLocked`", undefined);
    }

}
