import {MigrationInterface, QueryRunner} from "typeorm";

export class LogoDefaultAvatar1588365517291 implements MigrationInterface {
    name = 'LogoDefaultAvatar1588365517291'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `users` CHANGE `avatarUrl` `avatarUrl` varchar(255) NOT NULL DEFAULT 'http://localhost:3000/avatars/801084197526636736281984701149187.jpg'", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `users` CHANGE `avatarUrl` `avatarUrl` varchar(255) NOT NULL DEFAULT 'http://localhost:3000/avatars/download.png'", undefined);
    }

}
