import {MigrationInterface, QueryRunner} from "typeorm";

export class BanUser1587997268539 implements MigrationInterface {
    name = 'BanUser1587997268539'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `users` ADD `isBanned` tinyint NOT NULL DEFAULT 0", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `users` DROP COLUMN `isBanned`", undefined);
    }

}
