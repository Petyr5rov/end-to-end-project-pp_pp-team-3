import {MigrationInterface, QueryRunner} from "typeorm";

export class AvatarWithURL1588088792763 implements MigrationInterface {
    name = 'AvatarWithURL1588088792763'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `users` CHANGE `avatarUrl` `avatarUrl` varchar(255) NOT NULL DEFAULT 'http://localhost:3000/avatars/download.png'", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `users` CHANGE `avatarUrl` `avatarUrl` varchar(255) NOT NULL DEFAULT 'download.png'", undefined);
    }

}
