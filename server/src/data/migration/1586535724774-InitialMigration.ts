import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialMigration1586535724774 implements MigrationInterface {
    name = 'InitialMigration1586535724774'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `comment_vote` (`id` int NOT NULL AUTO_INCREMENT, `positive` tinyint NOT NULL, `userId` int NOT NULL, `commentId` int NOT NULL, UNIQUE INDEX `IDX_9194f426d41fb9a8abc3aae511` (`userId`, `commentId`), PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `comments` (`id` int NOT NULL AUTO_INCREMENT, `content` varchar(255) NOT NULL, `createTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updateTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `isDeleted` tinyint NOT NULL DEFAULT 0, `isFlagged` tinyint NOT NULL DEFAULT 0, `articleId` int NOT NULL, `userId` int NULL, UNIQUE INDEX `IDX_b6496f77981b10109fd0fff325` (`content`), PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `articles` (`id` int NOT NULL AUTO_INCREMENT, `title` varchar(255) NOT NULL, `content` varchar(255) NOT NULL, `createTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updateTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `isDeleted` tinyint NOT NULL DEFAULT 0, `isFlagged` tinyint NOT NULL DEFAULT 0, `userId` int NULL, UNIQUE INDEX `IDX_3c28437db9b5137136e1f6d609` (`title`), PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `roles` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `friends` (`id` int NOT NULL AUTO_INCREMENT, `userId_1` int NOT NULL, `userId_2` int NOT NULL, UNIQUE INDEX `IDX_eb136507de7a87538dfd7dd084` (`userId_1`, `userId_2`), PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `users` (`id` int NOT NULL AUTO_INCREMENT, `username` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `isDeleted` tinyint NOT NULL DEFAULT 0, `createAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updateAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `avatarUrl` varchar(255) NOT NULL DEFAULT 'download.png', `roleId` int NULL, UNIQUE INDEX `IDX_fe0bb3f6520ee0469504521e71` (`username`), UNIQUE INDEX `IDX_97672ac88f789774dd47f7c8be` (`email`), PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `article_vote` (`id` int NOT NULL AUTO_INCREMENT, `positive` tinyint NOT NULL, `userId` int NOT NULL, `articleId` int NOT NULL, UNIQUE INDEX `IDX_45cbbc05b3526150bfe7c06dc9` (`userId`, `articleId`), PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("ALTER TABLE `comment_vote` ADD CONSTRAINT `FK_ade7498b89296b9fb63bcd8dbdd` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `comment_vote` ADD CONSTRAINT `FK_5d77d92a6925ae3fc8da14e1257` FOREIGN KEY (`commentId`) REFERENCES `comments`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `comments` ADD CONSTRAINT `FK_7e8d7c49f218ebb14314fdb3749` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `comments` ADD CONSTRAINT `FK_b0011304ebfcb97f597eae6c31f` FOREIGN KEY (`articleId`) REFERENCES `articles`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `articles` ADD CONSTRAINT `FK_a9d18538b896fe2a6762e143bea` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `friends` ADD CONSTRAINT `FK_13fb33190c3333cbad7e5c8e750` FOREIGN KEY (`userId_1`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `friends` ADD CONSTRAINT `FK_1867d7b94efcf02ed2c0bd183f5` FOREIGN KEY (`userId_2`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_368e146b785b574f42ae9e53d5e` FOREIGN KEY (`roleId`) REFERENCES `roles`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `article_vote` ADD CONSTRAINT `FK_b590a3c3b7c7c46f39dc7f9974e` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `article_vote` ADD CONSTRAINT `FK_12ec1b35858ae4e4edabe4c47bd` FOREIGN KEY (`articleId`) REFERENCES `articles`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `article_vote` DROP FOREIGN KEY `FK_12ec1b35858ae4e4edabe4c47bd`", undefined);
        await queryRunner.query("ALTER TABLE `article_vote` DROP FOREIGN KEY `FK_b590a3c3b7c7c46f39dc7f9974e`", undefined);
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_368e146b785b574f42ae9e53d5e`", undefined);
        await queryRunner.query("ALTER TABLE `friends` DROP FOREIGN KEY `FK_1867d7b94efcf02ed2c0bd183f5`", undefined);
        await queryRunner.query("ALTER TABLE `friends` DROP FOREIGN KEY `FK_13fb33190c3333cbad7e5c8e750`", undefined);
        await queryRunner.query("ALTER TABLE `articles` DROP FOREIGN KEY `FK_a9d18538b896fe2a6762e143bea`", undefined);
        await queryRunner.query("ALTER TABLE `comments` DROP FOREIGN KEY `FK_b0011304ebfcb97f597eae6c31f`", undefined);
        await queryRunner.query("ALTER TABLE `comments` DROP FOREIGN KEY `FK_7e8d7c49f218ebb14314fdb3749`", undefined);
        await queryRunner.query("ALTER TABLE `comment_vote` DROP FOREIGN KEY `FK_5d77d92a6925ae3fc8da14e1257`", undefined);
        await queryRunner.query("ALTER TABLE `comment_vote` DROP FOREIGN KEY `FK_ade7498b89296b9fb63bcd8dbdd`", undefined);
        await queryRunner.query("DROP INDEX `IDX_45cbbc05b3526150bfe7c06dc9` ON `article_vote`", undefined);
        await queryRunner.query("DROP TABLE `article_vote`", undefined);
        await queryRunner.query("DROP INDEX `IDX_97672ac88f789774dd47f7c8be` ON `users`", undefined);
        await queryRunner.query("DROP INDEX `IDX_fe0bb3f6520ee0469504521e71` ON `users`", undefined);
        await queryRunner.query("DROP TABLE `users`", undefined);
        await queryRunner.query("DROP INDEX `IDX_eb136507de7a87538dfd7dd084` ON `friends`", undefined);
        await queryRunner.query("DROP TABLE `friends`", undefined);
        await queryRunner.query("DROP TABLE `roles`", undefined);
        await queryRunner.query("DROP INDEX `IDX_3c28437db9b5137136e1f6d609` ON `articles`", undefined);
        await queryRunner.query("DROP TABLE `articles`", undefined);
        await queryRunner.query("DROP INDEX `IDX_b6496f77981b10109fd0fff325` ON `comments`", undefined);
        await queryRunner.query("DROP TABLE `comments`", undefined);
        await queryRunner.query("DROP INDEX `IDX_9194f426d41fb9a8abc3aae511` ON `comment_vote`", undefined);
        await queryRunner.query("DROP TABLE `comment_vote`", undefined);
    }

}
