import {MigrationInterface, QueryRunner} from "typeorm";

export class AddFriendshipPendingColumn1587139933317 implements MigrationInterface {
    name = 'AddFriendshipPendingColumn1587139933317'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `friends` ADD `pending` tinyint NOT NULL DEFAULT 1", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `friends` DROP COLUMN `pending`", undefined);
    }

}
