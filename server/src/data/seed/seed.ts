/* eslint-disable no-console */
import { Role } from '../entities/role.entity';
import { Repository, createConnection } from 'typeorm';
import { User } from '../entities/user.entity';
import * as bcrypt from 'bcrypt';
import { UserRole } from '../../common/user-role';
import { Article } from '../entities/article.entity';
import { Comment } from '../entities/comment.entity';

const seedRoles = async (connection: any) => {
  const rolesRepo: Repository<Role> = connection.manager.getRepository(Role);

  const roles: Role[] = await rolesRepo.find();
  if (roles.length) {
    console.log('The DB already has roles!');
    return;
  }

  const rolesSeeding: Role[] = Object.keys(UserRole).map((roleName: string) =>
    rolesRepo.create({ name: roleName }),
  );

  await rolesRepo.save(rolesSeeding);
  console.log('Seeded roles successfully!');
};

const seedAdmin = async (connection: any) => {
  const userRepo: Repository<User> = connection.manager.getRepository(User);
  const rolesRepo: Repository<Role> = connection.manager.getRepository(Role);

  const admin = await userRepo.findOne({
    where: {
      username: 'Admin',
    },
  });

  if (admin) {
    console.log('The DB already has an admin!');
    return;
  }

  const adminRole: Role = await rolesRepo.findOne({
    where: {
      name: 'ADMIN',
    },
  });

  if (!adminRole) {
    console.log('The DB does not have any roles!');
    return;
  }

  const username = 'Admin';
  const email = 'pp_pp@pppp.bg';
  const password = 'asdASD123';
  const hashedPassword = await bcrypt.hash(password, 10);
  

  const newAdmin: User = userRepo.create({
    username,
    email,
    password: hashedPassword,
    role: adminRole,
  });

  await userRepo.save(newAdmin);
  console.log('Seeded admin successfully!');
};
const seedUserGuest = async (connection: any) => {
  const userRepo: Repository<User> = connection.manager.getRepository(User);
  const rolesRepo: Repository<Role> = connection.manager.getRepository(Role);

  const guest = await userRepo.findOne({
    where: {
      username: 'Guest',
    },
  });

  if (guest) {
    console.log('The DB already has a guest!');
    return;
  }

  const guestRole: Role = await rolesRepo.findOne({
    where: {
      name: UserRole.GUEST,
    },
  });

  if (!guestRole) {
    console.log('The DB does not have any roles!');
    return;
  }

  const username = 'Guest';
  const email = 'pp_pp@asd.bg';
  const password = 'asdASD123';
  const hashedPassword = await bcrypt.hash(password, 10);

  const newUser: User = userRepo.create({
    username,
    email,
    password: hashedPassword,
    role: guestRole,
  });

  await userRepo.save(newUser);
  console.log('Seeded guest successfully!');
};

const seedArticles = async (connection: any) => {
  const userRepo: Repository<User> = connection.manager.getRepository(User);
  const articleRepo: Repository<Article> = connection.manager.getRepository(
    Article,
  );

  const a1 = await articleRepo.findOne({
    where: {
      title: 'Malkia Princ',
    },
  });

  if (a1) {
    console.log('The DB already has an article!');
    return;
  }

  const article1 = articleRepo.create({
    title: 'Malkia Princ',
    content: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy.',
    user: await userRepo.findOne({
      where: {
        username: 'Guest',
      },
    }),
  });

  const a2 = await articleRepo.findOne({
    where: {
      title: 'Kumcho Valcho',
    },
  });

  if (a2) {
    console.log('The DB already has an article!');
    return;
  }

  const article2 = articleRepo.create({
    title: 'Kumcho Valcho',
    content: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy.',
    user: await userRepo.findOne({
      where: {
        username: 'Admin',
      },
    }),
  });

  await articleRepo.save(article1);
  await articleRepo.save(article2);

  console.log('Seeded  article successfully!');
};

const seedComments = async (connection: any) => {
  const userRepo: Repository<User> = connection.manager.getRepository(User);
  const articleRepo: Repository<Article> = connection.manager.getRepository(
    Article,
  );
  const commentRepo: Repository<Comment> = connection.manager.getRepository(
    Comment,
  );

  const c1 = await commentRepo.findOne({
    where: {
      content: 'comment Malkia Princ',
    },
  });

  if (c1) {
    console.log('The DB already has a comment!');
    return;
  }

  const comment1 = commentRepo.create({
    content: 'comment Malkia Princ',

    user: await userRepo.findOne({
      where: {
        username: 'Guest',
      },
    }),
    article: await articleRepo.findOne({
      where: {
        title: 'Malkia Princ',
      },
    }),
  });

  const c2 = await commentRepo.findOne({
    where: {
      content: 'comment Kumcho Valcho',
    },
  });

  if (c2) {
    console.log('The DB already has a comment!');
    return;
  }

  const comment2 = commentRepo.create({
    content: 'comment Kumcho Valcho',

    user: await userRepo.findOne({
      where: {
        username: 'Admin',
      },
    }),
    article: await articleRepo.findOne({
      where: {
        title: 'Kumcho Valcho',
      },
    }),
  });
  await commentRepo.save(comment1);
  await commentRepo.save(comment2);
  console.log('Seeded comment successfully!');
};

const seed = async () => {
  console.log('Seed started!');
  const connection = await createConnection();

  await seedRoles(connection);
  await seedAdmin(connection);
  await seedUserGuest(connection);
  await seedArticles(connection);
  await seedComments(connection);

  await connection.close();
  console.log('Seed completed!');
};

seed().catch(console.error);
