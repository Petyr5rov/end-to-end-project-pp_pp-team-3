import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  Index,
} from 'typeorm';
import { User } from './user.entity';
import { Comment } from './comment.entity';

@Entity()
@Index(['userId', 'commentId'], { unique: true })
export class CommentVote {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column('boolean')
  positive: boolean;

  @Column('int')
  userId: number;

  @ManyToOne(
    () => User,
    user => user.commentsVotes,
    { eager: true },
  )
  @JoinColumn({ name: 'userId' })
  user: User;

  @Column('int')
  commentId: number;

  @ManyToOne(
    () => Comment,
    comment => comment.votes,
  )
  @JoinColumn({ name: 'commentId' })
  comment: Comment;
}
