import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';
import { Comment } from './comment.entity';
import { User } from './user.entity';
import { IsNotEmpty, IsBoolean } from 'class-validator';
import { ArticleVote } from './article-vote.entity';


@Entity('articles')
export class Article {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @IsNotEmpty()
  @Column('nvarchar', { unique: true })
  title: string;

  @IsNotEmpty()
  @Column({type: 'nvarchar' , length: 1000})
  content: string;

  @CreateDateColumn()
  createTime: Date;

  @UpdateDateColumn()
  updateTime: Date;

  @IsBoolean()
  @Column({ default: false })
  isDeleted: boolean;

  @IsBoolean()
  @Column({ default: false })
  isFlagged: boolean;

  @IsBoolean()
  @Column({ default: false })
  isLocked: boolean;

  @ManyToOne(
    () => User,
    user => user.articles,
    { eager: true },
  )
  user: User;

  @OneToMany(
    () => Comment,
    comment => comment.article,
    { eager: true },
  )
  comments: Comment[];

  @OneToMany(
    () => ArticleVote,
    articleVote => articleVote.article,
    { eager: true },
  )
  votes: ArticleVote[];
}
