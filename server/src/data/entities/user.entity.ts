import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne
} from 'typeorm';
import { Article } from './article.entity';
import { Comment } from './comment.entity';
import { Role } from './role.entity';
import { CommentVote } from './comment-vote.entity';
import { ArticleVote } from './article-vote.entity';
import { Friends } from './friends.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'nvarchar', unique: true })
  username: string;

  @Column({ type: 'nvarchar', unique: true })
  email: string;

  @Column({ type: 'nvarchar', nullable: false })
  password: string;

  @Column({ type: 'boolean', default: false })
  isDeleted: boolean;

  @Column('boolean', { default: false })
  isBanned: boolean;

  @CreateDateColumn()
  createAt: Date;

  @UpdateDateColumn()
  updateAt: Date;

  @Column({ default: `http://${process.env.DB_HOST}:${process.env.PORT}/avatars/801084197526636736281984701149187.jpg` })
   avatarUrl: string;

  @OneToMany(() => Article, (article) => article.user)
  articles: Article[];

  @OneToMany(() => Comment, (comment) => comment.user)
  comments: Comment[];

  @ManyToOne(() => Role, { eager: true })
  role: Role;

  @OneToMany(() => ArticleVote, (articleVote) => articleVote.user)
  articlesVotes: ArticleVote[];

  @OneToMany(() => CommentVote, (commentVote) => commentVote.user)
  commentsVotes: CommentVote[];


  @OneToMany(() => Friends, (friends) => friends.user1)
  addedFriends: Friends[];

  @OneToMany(() => Friends, (friends) => friends.user2)
  receivedFriends: Friends[];
}
