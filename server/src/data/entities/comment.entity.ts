import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  OneToMany,
} from 'typeorm';
import { User } from './user.entity';
import { IsNotEmpty, IsBoolean } from 'class-validator';
import { Article } from './article.entity';
import { CommentVote } from './comment-vote.entity';

@Entity('comments')
export class Comment {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @IsNotEmpty()
  @Column({type: 'nvarchar' , length: 1000, unique: true })
  content: string;

  @CreateDateColumn()
  createTime: Date;

  @UpdateDateColumn()
  updateTime: Date;

  @IsBoolean()
  @Column({ default: false })
  isDeleted: boolean;

  @IsBoolean()
  @Column({ default: false })
  isFlagged: boolean;

  @ManyToOne(
    () => User,
    user => user.comments,
    { eager: true },
  )
  user: User;

  @Column()
  articleId: number;
  @ManyToOne(
    () => Article,
    article => article.comments,
  )
  @JoinColumn({ name: 'articleId' })
  article: Article;

  @OneToMany(
    () => CommentVote,
    commentVote => commentVote.comment,
    { eager: true },
  )
  votes: CommentVote[];
}
