import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  Index
} from 'typeorm';
import { User } from './user.entity';
import { IsBoolean } from 'class-validator';

@Entity()
@Index(['userId_1', 'userId_2'], { unique: true })
export class Friends {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column('int')
  userId_1: number;

  @Column('int')
  userId_2: number;

  @IsBoolean()
  @Column({ default: true })
  pending: boolean;

  @ManyToOne(() => User, user => user.addedFriends, { eager: true })
  @JoinColumn({ name: 'userId_1' })
  user1: User;

  @ManyToOne(() => User, user => user.receivedFriends, { eager: true })
  @JoinColumn({ name: 'userId_2' })
  user2: User;
}
