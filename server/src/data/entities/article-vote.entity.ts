import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  Index,
} from 'typeorm';
import { User } from './user.entity';
import { Article } from './article.entity';

@Entity()
@Index(['userId', 'articleId'], { unique: true })
export class ArticleVote {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column('boolean')
  positive: boolean;

  @Column('int')
  userId: number;

  @ManyToOne(
    () => User,
    user => user.articlesVotes,
    { eager: true },
  )
  @JoinColumn({ name: 'userId' })
  user: User;

  @Column('int')
  articleId: number;

  @ManyToOne(
    () => Article,
    article => article.votes,
  )
  @JoinColumn({ name: 'articleId' })
  article: Article;
}
