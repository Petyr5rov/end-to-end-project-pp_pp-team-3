import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { User } from '../data/entities/user.entity';
import { Repository } from 'typeorm';
import { LoginUserDTO } from '../models/user/user-login-dto';
import { JwtPayload } from '../common/types/jwt-payload';
import { MySystemError } from '../common/exceptions/my-system.error';

@Injectable()
export class AuthService {
  private readonly blacklist: string[] = [];
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    private readonly jwtService: JwtService,
  ) {}

   async findUserByName(username: string) {
    
    return await this.userRepository.findOne({ username });
  }

   async validateUser(username: string, password: string) {
    const user = await this.findUserByName(username);
    if (!user) {

      return null;
    }
    const isUserValidated = await bcrypt.compare(password, user.password);

    return isUserValidated ? user : null;
  }

  async login(loginUser: LoginUserDTO): Promise<any> {

    const user = await this.validateUser(
      loginUser.username,
      loginUser.password,
    );

    if (!user) {
      throw new UnauthorizedException('Wrong credentials!');
    }

    if (user.isBanned) {
      throw new MySystemError(`Sorry you can't log in, because you are BANNED!`, 400);
  }

    const payload: JwtPayload = {
      id: user.id,
      username: user.username,
      email: user.email,
      avatarUrl: user.avatarUrl,
      role: user.role.name
    };

    return {
      token: await this.jwtService.signAsync(payload),
    };
  }

  public blacklistToken(token: string): void {
    this.blacklist.push(token);
  }

  public isTokenBlacklisted(token: string): boolean {

     return this.blacklist.includes(token);
  }
}
