import { AuthController } from "./auth.controller";
import { TestingModule, Test } from "@nestjs/testing";
import { AuthService } from "./auth.service";
import { UserReturnDTO } from '../models/user/user-return-dto';
import { UserCreateDTO } from "../models/user";

describe('AuthController', () => {
    let controller: AuthController;
  
    let authService: any;
  
    beforeEach(async () => {
      authService = {
        login() {
          /* empty */
        },
        blacklistToken() {
          /* empty */
        },
      };
  
      const module: TestingModule = await Test.createTestingModule({
        controllers: [AuthController],
        providers: [
          {
            provide: AuthService,
            useValue: authService,
          },
        ],
      }).compile();
  
      controller = module.get<AuthController>(AuthController);
    });
  
    it('should be defined', () => {
      // Arrange & Act & Assert
      expect(controller).toBeDefined();
    });
  
    describe('loginUser()', () => {
      it('should call authService login() with the passed user from the client once', async () => {
        // Arrange
        const fakeUser = new UserCreateDTO();
        const spy = jest.spyOn(authService, 'login');
  
        // Act
        await controller.loginUser(fakeUser);
  
        // Assert
        expect(spy).toBeCalledWith(fakeUser);
        expect(spy).toBeCalledTimes(1);
      });
  
      it('should return the result from authService login()', async () => {
        // Arrange
        const fakeUser = new UserCreateDTO();
        const result = new UserReturnDTO();
  
        jest.spyOn(authService, 'login').mockReturnValue(Promise.resolve(result));
  
        // Act
        const actualResult = await controller.loginUser(fakeUser);
  
        // Assert
        expect(result).toEqual(actualResult);
      });
    });
  
    describe('logoutUser()', () => {
      it('should call authService blacklistToken() with the passed token from the client once', async () => {
        // Arrange
        const fakeToken = 'token';
        const spy = jest.spyOn(authService, 'blacklistToken');
  
        // Act
        await controller.logoutUser(fakeToken);
  
        // Assert
        expect(spy).toBeCalledWith(fakeToken);
        expect(spy).toBeCalledTimes(1);
      });
  
      it('should call authService blacklistToken() with the passed token from the client once', async () => {
        // Arrange
        const fakeToken = 'token';
        jest.spyOn(authService, 'blacklistToken');
  
        // Act
        const result = await controller.logoutUser(fakeToken);
  
        // Assert
        expect(result).toBeDefined();
        expect(result.msg).toBeDefined();
        expect(result.msg.includes('logout')).toEqual(true);
      });
    });
  });
  