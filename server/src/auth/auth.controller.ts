import { Controller, Post, Body, Delete, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginUserDTO } from '../models/user/user-login-dto';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklist.guard';
import { Token } from '../common/decorators/token.decorator';

@Controller('session')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post()
  async loginUser(@Body() user: LoginUserDTO) {
    return await this.authService.login(user);
  }

  @Delete()
  @UseGuards(AuthGuardWithBlacklisting)
  public async logoutUser(@Token() token: string) {
    this.authService.blacklistToken(token);
    return {
      msg: 'Successful logout!',
    };
  }
}
