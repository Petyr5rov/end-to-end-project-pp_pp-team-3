import { AuthService } from './auth.service';
import { TestingModule, Test } from '@nestjs/testing';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '../user/user.service';
import { LoginUserDTO } from '../models/user/user-login-dto';
import { User } from '../data/entities/user.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { JwtPayload } from '../common/types/jwt-payload';
describe('AuthService', () => {
  let service: AuthService;

  const userRepo: Partial<Repository<User>> = {};

  let userService: any;
  let jwtService: any;

  beforeEach(async () => {
    userService = {
      validateUser() {
        /* empty */
      }
    };

    jwtService = {
      signAsync() {
        /* empty */
      }
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        { provide: UserService, useValue: userService },
        { provide: JwtService, useValue: jwtService },
        {
          provide: getRepositoryToken(User),
          useValue: userRepo
        }
      ]
    }).compile();

    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(service).toBeDefined();
  });

  describe('login()', () => {
    it('should call userService validate() once with correct user', async () => {
      // Arrange
      const validateFakeUser: User = {
        username: 'asd',
        password: 'password',
        avatarUrl: 'mockAvatar',
        email: 'test@test.com',
        role: { name: 'test' }
      } as User;

      const loginFakeUser: LoginUserDTO = {
        username: 'asd',
        password: 'password'
      };

      const validateUserSpy = jest
        .spyOn(service, 'validateUser')
        .mockReturnValue(Promise.resolve(validateFakeUser));

      const fakeToken = 'token';
      jest
        .spyOn(jwtService, 'signAsync')
        .mockReturnValue(Promise.resolve(fakeToken));

      // Act
      await service.login(loginFakeUser);

      // Assert
      expect(validateUserSpy).toBeCalledWith(
        loginFakeUser.username,
        loginFakeUser.password
      );
      expect(validateUserSpy).toBeCalledTimes(1);
    });

    it('should throw error if user with such username does not exists', async () => {
      // Arrange

      const validateUserSpy = jest
        .spyOn(userService, 'validateUser')
        .mockReturnValue(undefined);

      const fakeToken = 'token';
      jest
        .spyOn(jwtService, 'signAsync')
        .mockReturnValue(Promise.resolve(fakeToken));

      // Act & Assert
      expect(service.login(validateUserSpy as any)).rejects.toThrow();
    });

    it('should call jwtService signAsync() once with correct payload', async () => {
      // Arrange
      const validateFakeUser = {
        id: 1,
        username: 'asd',
        password: 'password',
        avatarUrl: 'mockAvatar',
        email: 'test@test.com',
        role: { name: 'test' }
      } as User;

      const payload: JwtPayload = {
        id: 1,
        username: 'asd',
        avatarUrl: 'mockAvatar',
        email: 'test@test.com',
        role: 'test'
      };
      
      const loginFakeUser: LoginUserDTO = {
        username: 'asd',
        password: 'password'
      };

      jest
        .spyOn(service, 'validateUser')
        .mockReturnValue(Promise.resolve(validateFakeUser));

      const fakeToken = 'token';
      const signAsyncSpy = jest
        .spyOn(jwtService, 'signAsync')
        .mockReturnValue(Promise.resolve(fakeToken));

      // Act
      await service.login(loginFakeUser);

      // Assert
      expect(signAsyncSpy).toBeCalledWith(payload);
      expect(signAsyncSpy).toBeCalledTimes(1);
    });

    it('should return the token from the jwtService signAsync()', async () => {
      // Arrange
      const validateFakeUser = {
        username: 'asd',
        password: 'password',
        avatarUrl: 'mockAvatar',
        email: 'test@test.com',
        role: { name: 'test' }
      } as User;

      const loginFakeUser: LoginUserDTO = {
        username: 'asd',
        password: 'password'
      };

      jest
        .spyOn(service, 'validateUser')
        .mockReturnValue(Promise.resolve(validateFakeUser));

      const fakeToken = 'token';
      jest
        .spyOn(jwtService, 'signAsync')
        .mockReturnValue(Promise.resolve(fakeToken));

      // Act
      const result = await service.login(loginFakeUser);

      // Assert
      expect(result).toEqual({ token: fakeToken });
    });
  });

  describe('blacklistToken()', () => {
    it('should add the passed token to the blacklist collection', () => {
      // Arrange
      const fakeToken = 'token';

      // Act
      service.blacklistToken(fakeToken);

      // Assert
      expect((service as any).blacklist.includes(fakeToken)).toEqual(true);
    });
  });

  describe('isTokenBlacklisted()', () => {
    it('should return true if the passed token exist in the blacklist collection', () => {
      // Arrange
      const fakeToken = 'token';
      (service as any).blacklist.push(fakeToken);

      // Act
      const result = service.isTokenBlacklisted(fakeToken);

      // Assert
      expect(result).toEqual(true);
    });

    it('should return false if the passed token does not exist in the blacklist collection', () => {
      // Arrange
      const fakeToken = 'token';

      // Act
      const result = service.isTokenBlacklisted(fakeToken);

      // Assert
      expect(result).toEqual(false);
    });
  });
});
