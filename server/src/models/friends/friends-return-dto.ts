import { Expose } from 'class-transformer';
import { UserReturnDTO } from '../user';

export class FriendsReturnDTO {
  @Expose()
  public user2: UserReturnDTO;

  @Expose()
  public pending: boolean;
}
