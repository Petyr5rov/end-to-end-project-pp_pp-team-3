import { UserReturnDTO } from '../user';
import { Expose } from 'class-transformer';
import { CommentVote } from '../../data/entities/comment-vote.entity';

export class CommentShowDTO {
  @Expose()
  id: number;

  @Expose()
  content: string;

  @Expose()
  createTime: Date;

  @Expose()
  updateTime: Date;

  @Expose()
  user: UserReturnDTO;

  @Expose()
  votes: CommentVote[];
}
