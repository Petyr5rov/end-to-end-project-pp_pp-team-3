import { MinLength, MaxLength, IsOptional } from 'class-validator';

export class CommentPutContentDTO {
  @IsOptional()
  @MinLength(4)
  @MaxLength(500)
  content: string;
}
