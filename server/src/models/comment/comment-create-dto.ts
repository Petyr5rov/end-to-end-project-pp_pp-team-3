import { IsNotEmpty, MinLength, MaxLength } from 'class-validator';

export class CommentCreatedDTO {

  @IsNotEmpty()
  @MinLength(4)
  @MaxLength(500)
  content: string;

  @IsNotEmpty()
  articleId: number;
}
