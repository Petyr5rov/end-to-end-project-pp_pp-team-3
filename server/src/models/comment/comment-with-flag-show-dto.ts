import { UserReturnDTO } from '../user';
import { Expose } from 'class-transformer';
import { CommentVote } from '../../data/entities/comment-vote.entity';

export class CommentWithFlagShowDTO {
  @Expose()
  id: number;

  @Expose()
  content: string;

  @Expose()
  createTime: Date;

  @Expose()
  updateTime: Date;

  @Expose()
  isFlagged: boolean;
  
  @Expose()
  user: UserReturnDTO;

  @Expose()
  votes: CommentVote[];
}
