import { IsOptional, IsBooleanString } from 'class-validator';

export class CommentPutQueryDTO {
  @IsOptional()
  @IsBooleanString()
  vote: string;

  @IsOptional()
  @IsBooleanString()
  flag: string;
}
