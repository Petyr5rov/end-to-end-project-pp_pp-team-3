import { UserReturnDTO } from '../user/user-return-dto';
import { Expose } from 'class-transformer';
import { ArticleVoteDTO } from '../votes/article-vote.dto';
import { CommentShowDTO } from '../comment/comment-show-dto';

export class ArticleShowDTO {
  @Expose()
  id: number;

  @Expose()
  title: string;

  @Expose()
  content: string;

  @Expose()
  createTime: Date;

  @Expose()
  updateTime: Date;

  @Expose()
  isLocked: boolean;

  @Expose()
  user: UserReturnDTO;

  @Expose()
  comments: CommentShowDTO[];

  @Expose()
  votes: ArticleVoteDTO[];
}
