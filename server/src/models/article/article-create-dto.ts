import { IsNotEmpty, MinLength, MaxLength } from 'class-validator';

export class ArticleCreatedDTO {
  @IsNotEmpty()
  @MinLength(4)
  @MaxLength(100)
  title: string;

  @IsNotEmpty()
  @MinLength(4)
  @MaxLength(1000)
  content: string;
}
