import { MinLength, MaxLength, IsOptional } from 'class-validator';

export class ArticlePutContentDTO {
  @IsOptional()
  @MinLength(4)
  @MaxLength(1000)
  content: string;
}
