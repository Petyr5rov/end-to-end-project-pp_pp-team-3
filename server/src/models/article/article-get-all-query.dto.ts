import { MinLength, MaxLength, IsOptional, IsPositive } from 'class-validator';
import { Transform } from 'class-transformer';

export class ArticleGetAllQueryDTO {
  @IsOptional()
  @Transform(val => Number(val))
  @IsPositive()
  items: number;

  @IsOptional()
  @Transform(val => Number(val))
  @IsPositive()
  page: number;

  @IsOptional()
  @MinLength(3)
  @MaxLength(50)
  searchContent: string;

  @IsOptional()
  @MinLength(3)
  @MaxLength(50)
  searchTitle: string;

  @IsOptional()
  @Transform(val => Number(val))
  @IsPositive()
  userId: number;
}
