import { IsOptional, IsBooleanString } from 'class-validator';

export class ArticlePutQueryDTO {
  @IsOptional()
  @IsBooleanString()
  vote: string;

  @IsOptional()
  @IsBooleanString()
  flag: string;
}
