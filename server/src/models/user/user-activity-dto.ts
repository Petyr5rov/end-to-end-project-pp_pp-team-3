import { Expose } from 'class-transformer';

export class UserActivityDTO {
  @Expose()
  userId: number;

  @Expose()
  articles: number;

  @Expose()
  comments: number;

  @Expose()
  upvoteArticles: number;

  @Expose()
  downvoteArticles: number;

  @Expose()
  upvoteComments: number;

  @Expose()
  downvoteComments: number;
}