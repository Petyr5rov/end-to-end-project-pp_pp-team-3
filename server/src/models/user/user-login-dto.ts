import { IsString, IsNotEmpty, Matches } from 'class-validator';

export class LoginUserDTO {
  @IsString()
  @IsNotEmpty()
  readonly username: string;

  @IsNotEmpty()
  @IsString()
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message: 'password too weak',
  })
  readonly password: string;
}
