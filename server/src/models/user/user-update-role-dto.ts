import { UserRole } from '../../common/user-role';
import { IsString } from 'class-validator';

export class UpdateUserRolesDTO {
  @IsString()
  public role: UserRole;
}
