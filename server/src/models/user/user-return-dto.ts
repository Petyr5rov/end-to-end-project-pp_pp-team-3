import { Expose } from 'class-transformer';

export class UserReturnDTO {
  @Expose()
  id: number;
  @Expose()
  public username: string;

  @Expose()
  public avatarUrl: string;

  @Expose()
  public role: string;

  @Expose()
  public isBanned: string;
}
