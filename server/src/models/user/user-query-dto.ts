import { IsOptional } from 'class-validator';

export class UserQueryDTO {
  @IsOptional()
  username: string;
  @IsOptional()
  email: string;
}
