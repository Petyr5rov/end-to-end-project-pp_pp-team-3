import { IsOptional, MinLength, MaxLength } from 'class-validator';

export class UserUpdateDTO {
  @IsOptional()
  @MinLength(4)
  @MaxLength(20)
  username?: string;

  @IsOptional()
  avatarUrl?: string;
}
