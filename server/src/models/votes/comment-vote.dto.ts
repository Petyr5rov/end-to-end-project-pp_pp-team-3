import { UserReturnDTO } from '../user/user-return-dto';
import { Expose } from 'class-transformer';

export class CommentVoteDTO {
  @Expose()
  positive: boolean;

  @Expose()
  user: UserReturnDTO;
}
