import { UserReturnDTO } from '../user/user-return-dto';
import { Expose } from 'class-transformer';

export class ArticleVoteDTO {
  @Expose()
  positive: boolean;

  @Expose()
  user: UserReturnDTO;
}
