import { Controller, Get, UseGuards, Request } from '@nestjs/common';
import { AuthGuardWithBlacklisting } from './common/guards/blacklist.guard';

@Controller('')
export class AppController {
  @Get()
  @UseGuards(AuthGuardWithBlacklisting)
  async test(@Request() request: any): Promise<{ message: string }> {
    return {
      message:
        `Only authenticated users can see this. => ` + request.user.username
    };
  }
}
