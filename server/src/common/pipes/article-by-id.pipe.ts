import { PipeTransform, Injectable } from '@nestjs/common';
import { getConnection } from 'typeorm';
import { Article } from '../../data/entities/article.entity';
import { MySystemError } from '../exceptions/my-system.error';

@Injectable()
export class ArticleByIdPipe
  implements PipeTransform<string, Promise<Article>> {
  async transform(value: string): Promise<Article> {
    if (isNaN(Number(value))) {
      throw new MySystemError('No articleId provided!', 404);
    }
    const connection = getConnection();
    const article = await connection.manager
      .getRepository(Article)
      .findOne(value, { where: { isDeleted: false } });

    if (!article) {
      throw new MySystemError('No such Article found!', 404);
    }
    return article;
  }
}
