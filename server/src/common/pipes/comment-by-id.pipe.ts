import { PipeTransform, Injectable } from '@nestjs/common';
import { getConnection } from 'typeorm';
import { MySystemError } from '../exceptions/my-system.error';
import { Comment } from '../../data/entities/comment.entity';

@Injectable()
export class CommentByIdPipe
  implements PipeTransform<string, Promise<Comment>> {
  async transform(value: string): Promise<Comment> {
    if (isNaN(Number(value))) {
      throw new MySystemError('No commentId provided!', 404);
    }
    const connection = getConnection();
    const comment = await connection.manager
      .getRepository(Comment)
      .findOne(value, { where: { isDeleted: false } });

    if (!comment) {
      throw new MySystemError('No such Comment found!', 404);
    }
    return comment;
  }
}
