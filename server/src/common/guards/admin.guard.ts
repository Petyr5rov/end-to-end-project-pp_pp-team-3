import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { UserRole } from '../user-role';
import { Observable } from 'rxjs';

@Injectable()
export class AdminGuard implements CanActivate {
  public canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();

    const user = request.user;

    return user && user.role.name === UserRole.ADMIN;
  }
}
