export class JwtPayload {
  id: number;
  username: string;
  email: string;
  avatarUrl: string;
  role: string;
}
