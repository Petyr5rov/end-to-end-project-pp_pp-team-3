import {
  Controller,
  Post,
  Body,
  Delete,
  Param,
  Put,
  UsePipes,
  ValidationPipe,
  Query,
  UseGuards,
  Get
} from '@nestjs/common';
import { CommentService } from './comment.service';
import { AdminGuard } from '../common/guards/admin.guard';
import { CommentByIdPipe } from '../common/pipes/comment-by-id.pipe';
import { ArticleByIdPipe } from '../common/pipes/article-by-id.pipe';
import { Article } from '../data/entities/article.entity';
import { Comment } from '../data/entities/comment.entity';
import { UserDecorator } from '../common/decorators/user.decorator';
import { User } from '../data/entities/user.entity';
import { CommentShowDTO } from '../models/comment/comment-show-dto';
import { CommentPutContentDTO } from '../models/comment/comment-put-content.dto';
import { CommentCreatedDTO } from '../models/comment/comment-create-dto';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklist.guard';

@UseGuards(AuthGuardWithBlacklisting)
@Controller('comments')
export class CommentController {
  constructor(private readonly commentService: CommentService) {}

  @UseGuards(AdminGuard)
  @Get('/flagged')
  async getFlaggedByAdmin(): Promise<CommentShowDTO[]> {
    return await this.commentService.getFlaggedByAdmin();
  }

  @Post()
  async create(
    @Body('articleId', ArticleByIdPipe) article: Promise<Article>,
    @Body() body: CommentCreatedDTO,
    @UserDecorator() user: User
  ): Promise<CommentShowDTO> {
    return await this.commentService.create(body.content, await article, user);
  }

  // localhost:3000/articles/1?vote=true&flag=true
  @Put(':id')
  @UsePipes(ValidationPipe)
  async editQuery(
    @Param('id', CommentByIdPipe) comment: Promise<Comment>,
    @UserDecorator() user: User,
    @Query() query: any
  ): Promise<{ message: string }> {
    const vote = query.vote === 'true' ? true : undefined ? undefined : false;
    const flag = query.flag === 'true' ? true : false;
    await this.commentService.editQuery(await comment, user, vote, flag);
    return {
      message: `Comment updated!`
    };
  }

  @Put(':id/editContent')
  @UsePipes(ValidationPipe)
  async editContent(
    @Param('id', CommentByIdPipe) comment: Promise<Comment>,
    @UserDecorator() user: User,
    @Body('content') content: CommentPutContentDTO
  ): Promise<{ message: string }> {
    await this.commentService.editContent(await comment, user, content);
    return {
      message: `Comment updated!`
    };
  }

  @UseGuards(AdminGuard)
  @Put(':id/editContentByAdmin')
  @UsePipes(ValidationPipe)
  async editContentByAdmin(
    @Param('id', CommentByIdPipe) comment: Promise<Comment>,
    @UserDecorator() user: User,
    @Body('content') content: CommentPutContentDTO
  ): Promise<{ message: string }> {
    await this.commentService.editContentByAdmin(await comment, user, content);
    return {
      message: `Comment updated!`
    };
  }

  @UseGuards(AdminGuard)
  @Put(':id/removeFlagByAdmin')
  @UsePipes(ValidationPipe)
  async removeFlagByAdmin(
    @Param('id', CommentByIdPipe) comment: Promise<Comment>
  ): Promise<{ message: string }> {
    await this.commentService.removeFlagByAdmin(await comment);
    return {
      message: `Comment updated!`
    };
  }

  @UseGuards(AdminGuard)
  @Delete(':id')
  async delete(
    @Param('id', CommentByIdPipe) comment: Promise<Comment>
  ): Promise<{ message: string }> {
    await this.commentService.delete(await comment);

    return {
      message: `Comment deleted!`
    };
  }
}
