import { Article } from '../data/entities/article.entity';
import { Comment } from '../data/entities/comment.entity';
import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../data/entities/user.entity';
import { CommentVote } from '../data/entities/comment-vote.entity';
import { plainToClass } from 'class-transformer';
import { CommentVoteDTO } from '../models/votes/comment-vote.dto';
import { MySystemError } from '../common/exceptions/my-system.error';
import { CommentPutContentDTO } from '../models/comment/comment-put-content.dto';
import { CommentShowDTO } from '../models/comment/comment-show-dto';
import { CommentWithFlagShowDTO } from '../models/comment/comment-with-flag-show-dto';
import { UserService } from '../user/user.service';

@Injectable()
export class CommentService {
  constructor(
    @Inject(UserService) private readonly userService: UserService,
    @InjectRepository(Article)
    private readonly articleRepository: Repository<Article>,
    @InjectRepository(Comment)
    private readonly commentRepository: Repository<Comment>,
    @InjectRepository(CommentVote)
    private readonly commentVoteRepository: Repository<CommentVote>,
  ) {}

  async create(
    content: string,
    article: Article,
    user: User,
  ): Promise<CommentShowDTO> {
    if (article.isLocked) {
      throw new MySystemError(`This article is locked!`, 400)
    }
    
    let saveComment: Partial<Comment> = {
      content: content,
      article: article,
      user: user,
    };
    try {
      saveComment = await this.commentRepository.save(saveComment);
    } catch (err) {
      throw new MySystemError(
        `Each comment should be with unique content!`,
        400,
      );
    }
    return this.convertToShowDTO(
      await this.commentRepository.findOne(saveComment.id),
    );
  }

  async getFlaggedByAdmin(): Promise<CommentShowDTO[]> {
    const flaggedComments = await this.commentRepository.find({
      where: { isDeleted: false, isFlagged: true },
    });
    return flaggedComments.map((comm: Comment) =>
      this.convertUnmaskedToShowDTO(comm),
    );
  }

  async editQuery(
    comment: Comment,
    user: User,
    vote?: boolean,
    flag?: boolean,
  ): Promise<void> {
    if (vote) {
      const upVote: CommentVote = await this.commentVoteRepository
        .createQueryBuilder('commentVote')
        .where('commentVote.userId = :userId', { userId: user.id })
        .andWhere('commentVote.commentId = :commentId', {
          commentId: comment.id,
        })
        .getOne();

      if (!upVote) {
        await this.commentVoteRepository.save({
          userId: user.id,
          commentId: comment.id,
          positive: true,
        });
      } else if (!upVote.positive) {
        await this.commentVoteRepository
          .createQueryBuilder()
          .update()
          .where('userId = :userId', { userId: user.id })
          .andWhere('commentId = :commentId', { commentId: comment.id })
          .set({ positive: true })
          .execute();
      }
    } else if (vote === false) {
      const downVote: CommentVote = await this.commentVoteRepository
        .createQueryBuilder('commentVote')
        .where('commentVote.userId = :userId', { userId: user.id })
        .andWhere('commentVote.commentId = :commentId', {
          commentId: comment.id,
        })
        .getOne();

      if (!downVote) {
        await this.commentVoteRepository.save({
          userId: user.id,
          commentId: comment.id,
          positive: false,
        });
      } else if (downVote.positive) {
        await this.commentVoteRepository
          .createQueryBuilder()
          .update()
          .where('userId = :userId', { userId: user.id })
          .andWhere('commentId = :commentId', { commentId: comment.id })
          .set({ positive: false })
          .execute();
      }
    }
    if (flag) {
      try {
        await this.commentRepository
          .createQueryBuilder()
          .update()
          .where('id = :id', { id: comment.id })
          .set({ isFlagged: true })
          .execute();
      } catch (err) {
        throw new MySystemError(
          err.message ||
            `Database took too much to respond, please try again later`,
          408,
        );
      }
    }
  }

  async editContent(
    comment: Comment,
    user: User,
    content: CommentPutContentDTO,
  ): Promise<void> {
    if (comment.user.id !== user.id) {
      throw new MySystemError(
        "This is not your comment so you can't edit it!",
        401,
      );
    }
    if (!content) {
      try {
        await this.commentRepository
          .createQueryBuilder()
          .update()
          .where('id = :id', { id: comment.id })
          .set({ isFlagged: true })
          .execute();
      } catch (err) {
        throw new MySystemError(
          err.message ||
            `Database took too much to respond, please try again later`,
          408,
        );
      }
    } else {
      try {
        await this.commentRepository
          .createQueryBuilder()
          .update()
          .where('id = :id', { id: comment.id })
          .set({ isFlagged: false, content: content as any })
          .execute();
      } catch (err) {
        throw new MySystemError(
          err.message ||
            `Database took too much to respond, please try again later`,
          408,
        );
      }
    }
  }

  async editContentByAdmin(
    comment: Comment,
    user: User,
    content: CommentPutContentDTO,
  ): Promise<void> {
    if (!content) {
      try {
        await this.commentRepository
          .createQueryBuilder()
          .update()
          .where('id = :id', { id: comment.id })
          .set({ isFlagged: true })
          .execute();
      } catch (err) {
        throw new MySystemError(
          err.message ||
            `Database took too much to respond, please try again later`,
          408,
        );
      }
    } else {
      try {
        await this.commentRepository
          .createQueryBuilder()
          .update()
          .where('id = :id', { id: comment.id })
          .set({
            isFlagged: false,
            content: `${content}\n\n edited by Admin ${user.username}`,
          })
          .execute();
      } catch (err) {
        throw new MySystemError(
          err.message ||
            `Database took too much to respond, please try again later`,
          408,
        );
      }
    }
  }

  async removeFlagByAdmin(comment: Comment): Promise<void> {
    try {
      await this.commentRepository
        .createQueryBuilder()
        .update()
        .where('id = :id', { id: comment.id })
        .set({ isFlagged: false })
        .execute();
    } catch (err) {
      throw new MySystemError(
        err.message ||
          `Database took too much to respond, please try again later`,
        408,
      );
    }
  }

  async delete(comment: Comment): Promise<void> {
    try {
      await this.commentRepository
        .createQueryBuilder()
        .update()
        .set({ isDeleted: true })
        .where('id = :id', { id: comment.id })
        .execute();
    } catch (err) {
      throw new MySystemError(
        err.message ||
          `Database took too much to respond, please try again later`,
        408,
      );
    }
  }

  public convertToShowDTO = (comment: any): CommentShowDTO => {
    comment = this.convertUnmaskedToShowDTO(comment);
    if (comment.isFlagged) {
      comment.content = `This Comment is flagged, please edit it's content or it will be deleted!`;
    }
    return plainToClass(CommentShowDTO, comment, {
      excludeExtraneousValues: true
    });
  };

  public convertUnmaskedToShowDTO = (comment: any): CommentWithFlagShowDTO => {
    comment.user = this.userService.convertToShowDTO(comment.user);
    comment.votes = comment.votes.map((vote: CommentVote) => {
      vote.user = this.userService.convertToShowDTO(vote.user) as any;
      return plainToClass(CommentVoteDTO, vote, {
        excludeExtraneousValues: true,
      });
    });
    return plainToClass(CommentWithFlagShowDTO, comment, {
      excludeExtraneousValues: true,
    });
  };
}
