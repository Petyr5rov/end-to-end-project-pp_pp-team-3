import { Article } from '../data/entities/article.entity';
import { Comment } from '../data/entities/comment.entity';
import { CommentController } from './comment.controller';
import { CommentService } from './comment.service';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../data/entities/user.entity';
import { UserService } from '../user/user.service';
import { UserModule } from '../user/user.module';
import { Role } from '../data/entities/role.entity';
import { CommentVote } from '../data/entities/comment-vote.entity';
import { Friends } from '../data/entities/friends.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Article, Comment, CommentVote, User, Role, Friends]),
    UserModule,
  ],
  controllers: [CommentController],
  providers: [CommentService, UserService],
  exports: [CommentService],
})
export class CommentModule {}
