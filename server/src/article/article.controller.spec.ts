import { Test, TestingModule } from '@nestjs/testing';
import { ArticleController } from './article.controller';
import { ArticleService } from './article.service';
import { ArticleGetAllQueryDTO } from '../models/article/article-get-all-query.dto';
import { User } from '../data/entities/user.entity';
import { Article } from '../data/entities/article.entity';
import { ArticleCreatedDTO } from '../models/article/article-create-dto';
import { ArticlePutQueryDTO } from '../models/article/article-put-query.dto';
import { ArticlePutContentDTO } from '../models/article/article-put-content.dto';

describe('Article Controller', () => {
  let controller: ArticleController;
  const articleService: Partial<ArticleService> = {
    all() {
      return null;
    },
    create() {
      return null;
    },
    editQuery() {
      return null;
    },
    convertToShowDTO() {
      return null;
    },
    editContent() {
      return null;
    },
    editContentByAdmin() {
      return null;
    },
    toggleFlagByAdmin() {
      return null;
    },
    toggleLockByAdmin() {
      return null;
    },
    delete() {
      return null;
    },
    getFlaggedByAdmin() {
      return null;
    }
  };
  const fakeUser = new User();
  const fakeQuery = new ArticleGetAllQueryDTO();
  const fakeAtricle = new Article();
  const fakeArticleCreatedDTO = new ArticleCreatedDTO();
  const fakeArticlePutQueryDTO = new ArticlePutQueryDTO();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ArticleController],
      providers: [
        {
          provide: ArticleService,
          useValue: articleService
        }
      ]
    }).compile();

    controller = module.get<ArticleController>(ArticleController);

    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('Test metod all()', () => {
    it('all() should return all() result', async () => {
      jest
        .spyOn(articleService, 'all')
        .mockImplementation(() => Promise.resolve('test') as any);
      const result = await controller.all(fakeQuery);

      expect(result).toBe('test');
    });
    it('should be call all()', async () => {
      const spy = jest.spyOn(articleService, 'all');

      await controller.all(fakeQuery);

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(
        fakeQuery.items,
        fakeQuery.page,
        fakeQuery.searchContent,
        fakeQuery.searchTitle,
        fakeQuery.userId
      );
    });
  });

  describe('Test metod create()', () => {
    it('create() should return create() result', async () => {
      jest
        .spyOn(articleService, 'create')
        .mockImplementation(() => Promise.resolve('test') as any);
      const result = await controller.create(fakeAtricle, fakeUser);

      expect(result).toBe('test');
    });
    it('should be call create()', async () => {
      jest.spyOn(articleService, 'create');

      await controller.create(fakeArticleCreatedDTO, fakeUser);

      expect(articleService.create).toHaveBeenCalledTimes(1);
      expect(articleService.create).toHaveBeenCalledWith(
        fakeArticleCreatedDTO,
        fakeUser
      );
    });
  });

  describe('Test metod getById', () => {
    it('should return getById results', async () => {
      jest
        .spyOn(articleService, 'convertToShowDTO')
        .mockImplementation(() => Promise.resolve('test') as any);

      const result = await controller.getById(fakeAtricle as any);

      expect(result).toBe('test');
    });
    it('should return  call convertToShowDTO', async () => {
      jest.spyOn(articleService, 'convertToShowDTO');

      await controller.getById((await Promise.resolve(fakeAtricle)) as any);

      expect(articleService.convertToShowDTO).toHaveBeenCalledTimes(1);
      expect(articleService.convertToShowDTO).toHaveBeenLastCalledWith(
        fakeAtricle
      );
    });
  });

  describe('Test metod editQuery()', () => {
    it('editQuery() should return editQuery() result', async () => {
      const expectedResult = { message: `Article updated!` };

      jest.spyOn(articleService, 'editQuery');

      const result = await controller.editQuery(
        Promise.resolve(fakeAtricle) as any,
        fakeUser,
        fakeArticlePutQueryDTO
      );

      expect(result).toEqual(expectedResult);
    });
    it('should be call editQuery()', async () => {
      const spy = jest.spyOn(articleService, 'editQuery');

      await controller.editQuery(
        Promise.resolve(fakeAtricle),
        fakeUser,
        fakeArticlePutQueryDTO
      );

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(fakeAtricle, fakeUser, false, false);
    });
  });
  
  describe('Test metod editContent()', () => {
    it('editContent() should return editContent() result', async () => {
      const expectedResult = { message: `Article updated!` };

      jest.spyOn(articleService, 'editContent');

      const result = await controller.editContent(
        Promise.resolve(fakeAtricle),
        fakeUser,
        'editContent'
      );

      expect(result).toEqual(expectedResult);
    });
    it('should be call editContent()', async () => {
      jest.spyOn(articleService, 'editContent');

      await controller.editContent(
        Promise.resolve(fakeAtricle),
        fakeUser,
        'editContent'
      );

      expect(articleService.editContent).toHaveBeenCalledTimes(1);
      expect(articleService.editContent).toHaveBeenCalledWith(
        fakeAtricle,
        fakeUser,
        'editContent'
      );
    });
  });

  describe('Test metod editContentByAdmin()', () => {
    it('editContentByAdmin() should return editContentByAdmin() result', async () => {
      const expectedResult = { message: `Article updated!` };

      jest.spyOn(articleService, 'editContentByAdmin');

      const result = await controller.editContent(
        Promise.resolve(fakeAtricle),
        fakeUser,
        'editContent'
      );

      expect(result).toEqual(expectedResult);
    });
    it('should be call editContentByAdmin()', async () => {
      jest.spyOn(articleService, 'editContentByAdmin');

      await controller.editContentByAdmin(
        Promise.resolve(fakeAtricle),
        fakeUser,
        'editContent'
      );

      expect(articleService.editContentByAdmin).toHaveBeenCalledTimes(1);
      expect(articleService.editContentByAdmin).toHaveBeenCalledWith(
        fakeAtricle,
        fakeUser,
        'editContent'
      );
    });
  });

  describe('Test metod toggleFlagByAdmin()', () => {
    it('toggleFlagByAdmin() should return toggleFlagByAdmin() result', async () => {
      const expectedResult = { message: `Article updated!` };

      jest.spyOn(articleService, 'toggleFlagByAdmin');

      const result = await controller.toggleFlagByAdmin(
        Promise.resolve(fakeAtricle)
      );

      expect(result).toEqual(expectedResult);
    });
    it('should be call toggleFlagByAdmin()', async () => {
      jest.spyOn(articleService, 'toggleFlagByAdmin');

      await controller.toggleFlagByAdmin(Promise.resolve(fakeAtricle));

      expect(articleService.toggleFlagByAdmin).toHaveBeenCalledTimes(1);
      expect(articleService.toggleFlagByAdmin).toBeCalledWith(fakeAtricle);
    });
  });

  describe('Test metod toggleLockByAdmin()', () => {
    it('toggleLockByAdmin() should return toggleLockByAdmin() result', async () => {
      const expectedResult = { message: `Article updated!` };

      jest.spyOn(articleService, 'toggleLockByAdmin');

      const result = await controller.toggleLockByAdmin(Promise.resolve(fakeAtricle));

      expect(result).toEqual(expectedResult);
    });
    it('should be call toggleLockByAdmin()', async () => {
      jest.spyOn(articleService, 'toggleLockByAdmin');

      await controller.toggleLockByAdmin(Promise.resolve(fakeAtricle));

      expect(articleService.toggleLockByAdmin).toHaveBeenCalledTimes(1);
      expect(articleService.toggleLockByAdmin).toBeCalledWith(fakeAtricle);
    });
  });

  describe('Test metod delete()', () => {
    it('delete() should return delete() result', async () => {
      const expectedResult = { message: `Article deleted!` };

      jest.spyOn(articleService, 'delete');

      const result = await controller.delete(Promise.resolve(fakeAtricle));

      expect(result).toEqual(expectedResult);
    });
    it('should be call delete()', async () => {
      jest.spyOn(articleService, 'delete');

      await controller.delete(Promise.resolve(fakeAtricle));

      expect(articleService.delete).toHaveBeenCalledTimes(1);
      expect(articleService.delete).toHaveBeenCalledWith(fakeAtricle);
    });
  });

  describe('Test metod getFlaggedByAdmin()', () => {
    it('getFlaggedByAdmin() should return getFlaggedByAdmin() result', async () => {
      jest
        .spyOn(articleService, 'getFlaggedByAdmin')
        .mockImplementation(() => Promise.resolve('test') as any);

      const result = await controller.getFlaggedByAdmin();

      expect(result).toEqual('test');
    });
    it('should be call getFlaggedByAdmin()', async () => {
      jest.spyOn(articleService, 'getFlaggedByAdmin');

      await controller.getFlaggedByAdmin();

      expect(articleService.getFlaggedByAdmin).toHaveBeenCalledTimes(1);
      expect(articleService.getFlaggedByAdmin).toHaveBeenCalledWith();
    });
  });
});
