import { Injectable, Inject } from '@nestjs/common';
import { Repository, In } from 'typeorm';
import { Article } from '../data/entities/article.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../data/entities/user.entity';
import { ArticleShowDTO } from '../models/article/article-show-dto';
import { ArticleCreatedDTO } from '../models/article/article-create-dto';
import { ArticleVote } from '../data/entities/article-vote.entity';
import { CommentService } from '../comment/comment.service';
import { MySystemError } from '../common/exceptions/my-system.error';
import { ArticlePutContentDTO } from '../models/article/article-put-content.dto';
import { plainToClass } from 'class-transformer';
import { ArticleVoteDTO } from '../models/votes/article-vote.dto';
import { CommentShowDTO } from '../models/comment/comment-show-dto';
import { ArticleWithFlagShowDTO } from '../models/article/article-with-flag-show-dto';
import { UserService } from '../user/user.service';

@Injectable()
export class ArticleService {
  constructor(
    @Inject(CommentService) private readonly commentService: CommentService,
    @Inject(UserService) private readonly userService: UserService,
    @InjectRepository(Article)
    private readonly articleRepository: Repository<Article>,
    @InjectRepository(ArticleVote)
    private readonly articleVoteRepository: Repository<ArticleVote>
  ) {}

  async all(
    items = 10,
    page = 1,
    searchContent?: string | undefined,
    searchTitle?: string | undefined,
    userId?: number | undefined
  ): Promise<[ArticleShowDTO[], number]> {
    let query = this.articleRepository
      .createQueryBuilder('article')
      .where('article.isDeleted = :del', { del: false });

    if (searchContent)
      query = query.andWhere('article.content like :search', {
        search: `%${searchContent}%`
      });

    if (searchTitle)
      query = query.andWhere('article.title like :search', {
        search: `%${searchTitle}%`
      });

    if (userId)
      query = query.andWhere('article.userId = :userId', {
        userId: userId
      });

    const [articles, count]: any = await query
      .skip((page - 1) * items)
      .take(items)
      .orderBy('article.updateTime', 'DESC')
      .getManyAndCount();

    const articleIds = articles.map((art: Article) => art.id);

    if (articleIds.length === 0) {
      return [[], 0];
    }

    const foundArticles: any = await this.articleRepository.find({
      where: { id: In(articleIds) },
      order: {
        updateTime: 'DESC'
      }
    });

    return [
      foundArticles.map((art: Article) => this.convertToShowDTO(art)),
      count
    ];
  }

  async getFlaggedByAdmin(): Promise<[ArticleWithFlagShowDTO[], number]> {
    const flaggedArticles = await this.articleRepository.findAndCount({
      where: { isDeleted: false, isFlagged: true },
      order: { updateTime: 'DESC' }
    });
    (flaggedArticles[0] as any) = flaggedArticles[0].map((art: Article) =>
      this.convertUnmaskedToShowDTO(art)
    );
    return flaggedArticles as any;
  }

  async create(
    article: ArticleCreatedDTO,
    user: User
  ): Promise<ArticleShowDTO> {
    const checkTitle = await this.articleRepository.find({
      where: { title: article.title }});
    
    if(checkTitle) {
      throw new MySystemError(`Article with such name exists, please try something else !`, 400);
    }

    const createdArticle: Partial<Article> = this.articleRepository.create(
      article
    );
    createdArticle.user = user;
    createdArticle.comments = [];
    createdArticle.votes = [];

    const savedArticle: Article = await this.articleRepository.save(
      createdArticle
    );

    return this.convertToShowDTO(
      await this.articleRepository.findOne(savedArticle.id)
    );
  }

  async editQuery(
    article: Article,
    user: User,
    vote?: boolean,
    flag?: boolean
  ): Promise<void> {
    if (article.isLocked) {
      throw new MySystemError(`This article is locked!`, 400);
    }

    if (vote) {
      const upVote: ArticleVote = await this.articleVoteRepository
        .createQueryBuilder('articleVote')
        .where('articleVote.userId = :userId', { userId: user.id })
        .andWhere('articleVote.articleId = :articleId', {
          articleId: article.id
        })
        .getOne();
      if (!upVote) {
        await this.articleVoteRepository.save({
          userId: user.id,
          articleId: article.id,
          positive: true
        });
      } else if (!upVote.positive) {
        await this.articleVoteRepository
          .createQueryBuilder()
          .update()
          .where('userId = :userId', { userId: user.id })
          .andWhere('articleId = :articleId', { articleId: article.id })
          .set({ positive: true })
          .execute();
      }
    } else if (vote === false) {
      const downVote: ArticleVote = await this.articleVoteRepository
        .createQueryBuilder('articleVote')
        .where('articleVote.userId = :userId', { userId: user.id })
        .andWhere('articleVote.articleId = :articleId', {
          articleId: article.id
        })
        .getOne();

      if (!downVote) {
        await this.articleVoteRepository.save({
          userId: user.id,
          articleId: article.id,
          positive: false
        });
      } else if (downVote.positive) {
        await this.articleVoteRepository
          .createQueryBuilder()
          .update()
          .where('userId = :userId', { userId: user.id })
          .andWhere('articleId = :articleId', { articleId: article.id })
          .set({ positive: false })
          .execute();
      }
    }
    if (flag) {
      try {
        await this.articleRepository
          .createQueryBuilder()
          .update()
          .where('id = :id', { id: article.id })
          .set({ isFlagged: true })
          .execute();
      } catch (err) {
        throw new MySystemError(
          err.message ||
            `Database took too much to respond, please try again later`,
          408
        );
      }
    }
  }

  async editContent(
    article: Article,
    user: User,
    content: string
  ): Promise<void> {
    if (article.user.id !== user.id) {
      throw new MySystemError(
        `This is not your article so you can't edit it!`,
        401
      );
    }
    if (!content) {
      try {
        await this.articleRepository
          .createQueryBuilder()
          .update()
          .where('id = :id', { id: article.id })
          .set({ isFlagged: true })
          .execute();
      } catch (err) {
        throw new MySystemError(
          err.message ||
            `Database took too much to respond, please try again later`,
          408
        );
      }
    } else {
      try {
        await this.articleRepository
          .createQueryBuilder()
          .update()
          .where('id = :id', { id: article.id })
          .set({
            isFlagged: false,
            content: content
          })
          .execute();
      } catch (err) {
        throw new MySystemError(
          err.message ||
            `Database took too much to respond, please try again later`,
          408
        );
      }
    }
  }

  async editContentByAdmin(
    article: Article,
    user: User,
    content: string
  ): Promise<void> {
    if (!content) {
      try {
        await this.articleRepository
          .createQueryBuilder()
          .update()
          .where('id = :id', { id: article.id })
          .set({ isFlagged: true })
          .execute();
      } catch (err) {
        throw new MySystemError(
          err.message ||
            `Database took too much to respond, please try again later`,
          408
        );
      }
    } else {
      try {
        await this.articleRepository
          .createQueryBuilder()
          .update()
          .where('id = :id', { id: article.id })
          .set({
            isFlagged: false,
            content: `${content}\n\n edited by Admin ${user.username}`
          })
          .execute();
      } catch (err) {
        throw new MySystemError(
          err.message ||
            `Database took too much to respond, please try again later`,
          408
        );
      }
    }
  }

  async toggleFlagByAdmin(article: Article): Promise<void> {
    try {
      await this.articleRepository
        .createQueryBuilder()
        .update()
        .where('id = :id', { id: article.id })
        .set({ isFlagged: !article.isFlagged })
        .execute();
    } catch (err) {
      throw new MySystemError(
        err.message ||
          `Database took too much to respond, please try again later`,
        408
      );
    }
  }

  async toggleLockByAdmin(article: Article): Promise<void> {
    try {
      await this.articleRepository
        .createQueryBuilder()
        .update()
        .where('id = :id', { id: article.id })
        .set({ isLocked: !article.isLocked })
        .execute();
    } catch (err) {
      throw new MySystemError(
        err.message ||
          `Database took too much to respond, please try again later`,
        408
      );
    }
  }

  async delete(article: Article): Promise<void> {
    try {
      await this.articleRepository
        .createQueryBuilder()
        .update()
        .set({ isDeleted: true })
        .where('id = :id', { id: article.id })
        .execute();
    } catch (err) {
      throw new MySystemError(
        err.message ||
          `Database took too much to respond, please try again later`,
        408
      );
    }
  }

  public convertToShowDTO = (article: any): ArticleShowDTO => {
    article = this.convertUnmaskedToShowDTO(article);
    if (article.isFlagged) {
      article.content = `This Article is flagged, please edit it's content or it will be deleted!`;
    }
    return plainToClass(ArticleShowDTO, article, {
      excludeExtraneousValues: true
    });
  };

  public convertUnmaskedToShowDTO = (article: any): ArticleWithFlagShowDTO => {
    article.user = this.userService.convertToShowDTO(article.user);
    article.comments = article.comments.map((item: CommentShowDTO) =>
      this.commentService.convertToShowDTO(item)
    );
    article.votes = article.votes.map((vote: ArticleVote) => {
      vote.user = this.userService.convertToShowDTO(vote.user) as any;
      return plainToClass(ArticleVoteDTO, vote, {
        excludeExtraneousValues: true
      });
    });
    return plainToClass(ArticleWithFlagShowDTO, article, {
      excludeExtraneousValues: true
    });
  };
}
