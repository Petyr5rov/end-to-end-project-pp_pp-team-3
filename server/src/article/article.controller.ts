import {
  Controller,
  Get,
  Post,
  Body,
  Delete,
  Param,
  Put,
  Query,
  UsePipes,
  ValidationPipe,
  UseGuards
} from '@nestjs/common';
import { ArticleService } from './article.service';
import { ArticleCreatedDTO } from '../models/article/article-create-dto';
import { ArticleShowDTO } from '../models/article/article-show-dto';
import { AdminGuard } from '../common/guards/admin.guard';
import { ArticleGetAllQueryDTO } from '../models/article/article-get-all-query.dto';
import { ArticlePutQueryDTO } from '../models/article/article-put-query.dto';
import { UserDecorator } from '../common/decorators/user.decorator';
import { ArticlePutContentDTO } from '../models/article/article-put-content.dto';
import { User } from '../data/entities/user.entity';
import { ArticleByIdPipe } from '../common/pipes/article-by-id.pipe';
import { Article } from '../data/entities/article.entity';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklist.guard';
import { ArticleWithFlagShowDTO } from '../models/article/article-with-flag-show-dto';

@Controller('articles')
export class ArticleController {
  constructor(private readonly articleService: ArticleService) {}

  // localhost:3000/articles?userId=1&items=10&page=3&searchContent=abc&searchTitle=xyz
  @Get()
  @UsePipes(ValidationPipe)
  async all(
    @Query() query: ArticleGetAllQueryDTO
  ): Promise<[ArticleShowDTO[], number]> {
    return await this.articleService.all(
      query.items,
      query.page,
      query.searchContent,
      query.searchTitle,
      query.userId
    );
  }

  @UseGuards(AuthGuardWithBlacklisting, AdminGuard)
  @Get('/flagged')
  async getFlaggedByAdmin(): Promise<[ArticleWithFlagShowDTO[], number]> {
    return await this.articleService.getFlaggedByAdmin();
  }

  @Get(':id')
  async getById(
    @Param('id', ArticleByIdPipe) article: Promise<Article>
  ): Promise<ArticleShowDTO> {
    return this.articleService.convertToShowDTO(await article);
  }

  @UseGuards(AuthGuardWithBlacklisting)
  @Post()
  async create(
    @Body() article: ArticleCreatedDTO,
    @UserDecorator() user: User
  ): Promise<ArticleShowDTO> {
    return await this.articleService.create(article, user);
  }

  // localhost:3000/articles/1?vote=true&flag=true
  @UseGuards(AuthGuardWithBlacklisting)
  @Put(':id')
  @UsePipes(ValidationPipe)
  async editQuery(
    @Param('id', ArticleByIdPipe) article: Promise<Article>,
    @UserDecorator() user: User,
    @Query() query: ArticlePutQueryDTO
  ): Promise<{ message: string }> {
    const vote = query.vote === 'true' ? true : undefined ? undefined : false;
    const flag = query.flag === 'true' ? true : false;
    await this.articleService.editQuery(await article, user, vote, flag);
    return {
      message: `Article updated!`
    };
  }

  @UseGuards(AuthGuardWithBlacklisting)
  @Put(':id/editContent')
  @UsePipes(ValidationPipe)
  async editContent(
    @Param('id', ArticleByIdPipe) article: Promise<Article>,
    @UserDecorator() user: User,
    @Body('content') content: string
  ): Promise<{ message: string }> {
    await this.articleService.editContent(await article, user, content);
    return {
      message: `Article updated!`
    };
  }

  @UseGuards(AuthGuardWithBlacklisting, AdminGuard)
  @Put(':id/editContentByAdmin')
  @UsePipes(ValidationPipe)
  async editContentByAdmin(
    @Param('id', ArticleByIdPipe) article: Promise<Article>,
    @UserDecorator() user: User,
    @Body('content') content: string
  ): Promise<{ message: string }> {
    await this.articleService.editContentByAdmin(await article, user, content);
    return {
      message: `Article updated!`
    };
  }

  @UseGuards(AuthGuardWithBlacklisting, AdminGuard)
  @Put(':id/flagByAdmin')
  @UsePipes(ValidationPipe)
  async toggleFlagByAdmin(
    @Param('id', ArticleByIdPipe) article: Promise<Article>
  ): Promise<{ message: string }> {
    await this.articleService.toggleFlagByAdmin(await article);
    return {
      message: `Article updated!`
    };
  }

  @UseGuards(AuthGuardWithBlacklisting, AdminGuard)
  @Put(':id/lockByAdmin')
  @UsePipes(ValidationPipe)
  async toggleLockByAdmin(
    @Param('id', ArticleByIdPipe) article: Promise<Article>
  ): Promise<{ message: string }> {
    await this.articleService.toggleLockByAdmin(await article);
    return {
      message: `Article updated!`
    };
  }

  @UseGuards(AuthGuardWithBlacklisting, AdminGuard)
  @Delete(':id')
  async delete(
    @Param('id', ArticleByIdPipe) article: Promise<Article>
  ): Promise<{ message: string }> {
    await this.articleService.delete(await article);

    return {
      message: `Article deleted!`
    };
  }
}
