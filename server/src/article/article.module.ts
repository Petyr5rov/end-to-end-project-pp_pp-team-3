import { Article } from '../data/entities/article.entity';
import { ArticleController } from './article.controller';
import { ArticleService } from './article.service';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../data/entities/user.entity';
import { UserService } from '../user/user.service';
import { UserModule } from '../user/user.module';
import { Role } from '../data/entities/role.entity';
import { ArticleVote } from '../data/entities/article-vote.entity';
import { CommentModule } from '../comment/comment.module';
import { Friends } from '../data/entities/friends.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Article, ArticleVote, User, Role, Friends]),
    UserModule,
    CommentModule,
  ],
  controllers: [ArticleController],
  providers: [ArticleService, UserService],
})
export class ArticleModule {}
