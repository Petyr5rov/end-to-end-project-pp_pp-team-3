import { Test, TestingModule } from '@nestjs/testing';
import { ArticleService } from './article.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Article } from '../data/entities/article.entity';
import { ArticleVote } from '../data/entities/article-vote.entity';
import { CommentService } from '../comment/comment.service';
import { Repository, In } from 'typeorm';
import { User } from '../data/entities/user.entity';
import { UserReturnDTO } from '../models/user';
import { repositoryMockFactory, MockType } from '../common/types/mock-type';
import { ArticleShowDTO } from '../models/article/article-show-dto';
import { ArticleCreatedDTO } from '../models/article/article-create-dto';
import { MySystemError } from '../common/exceptions/my-system.error';
import { ArticleWithFlagShowDTO } from '../models/article/article-with-flag-show-dto';
import { UserService } from '../user/user.service';

describe('ArticleService', () => {
  let service: ArticleService;
  const commentService: Partial<CommentService> = {};
  const userService: Partial<UserService> = {};

  let articleRepo: MockType<Repository<Article>>;
  let articleVoteRepo: MockType<Repository<ArticleVote>>;

  const fakeUser = {
    id: 2,
    username: 'Test',
    password: 'testP@ss',
    avatarUrl: 'http://this.jpg',
    role: { name: 'GUEST' }
  } as User;

  const fakeReturnUser = {
    id: 2,
    username: 'Test',
    avatarUrl: 'http://this.jpg',
    role: 'GUEST'
  } as UserReturnDTO;

  let fakeArticle: Article;

  const fakeReturnArticle = {
    id: 3,
    createTime: ('' as unknown) as Date,
    updateTime: ('' as unknown) as Date,
    title: '',
    isLocked: false,
    user: fakeReturnUser,
    content: 'TestContent',
    comments: [],
    votes: []
  } as ArticleShowDTO;

  const fakeArticleCreatedDTO = {
    title: 'test',
    content: 'test'
  } as ArticleCreatedDTO;

  const fakePositiveVote = { positive: true } as ArticleVote;
  const fakeNegativeVote = { positive: false } as ArticleVote;

  let fakeFlaggedArticle: ArticleWithFlagShowDTO;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ArticleService,
        {
          provide: CommentService,
          useValue: commentService
        },
        {
          provide: UserService,
          useValue: userService
        },
        {
          provide: getRepositoryToken(Article),
          useFactory: repositoryMockFactory
        },
        {
          provide: getRepositoryToken(ArticleVote),
          useFactory: repositoryMockFactory
        }
      ]
    }).compile();

    service = module.get<ArticleService>(ArticleService);
    articleRepo = module.get(getRepositoryToken(Article));
    articleVoteRepo = module.get(getRepositoryToken(ArticleVote));

    fakeArticle = {
      id: 3,
      user: fakeUser,
      createTime: ('' as unknown) as Date,
      updateTime: ('' as unknown) as Date,
      title: '',
      isFlagged: false,
      content: 'TestContent',
      comments: [],
      votes: []
    } as Article;

    fakeFlaggedArticle = {
      id: 3,
      createTime: ('' as unknown) as Date,
      updateTime: ('' as unknown) as Date,
      title: '',
      content: 'TestContent',
      isFlagged: false,
      isLocked: false,
      user: fakeReturnUser,
      comments: [],
      votes: []
    };

    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(userService).toBeDefined();
    expect(articleRepo).toBeDefined();
    expect(articleVoteRepo).toBeDefined();
  });

  describe('Test metod all()', () => {
    it('should return correct data', async () => {
      articleRepo.createQueryBuilder.mockImplementation(() => ({
        where: jest.fn().mockReturnThis(),
        andWhere: jest.fn().mockReturnThis(),
        skip: jest.fn().mockReturnThis(),
        take: jest.fn().mockReturnThis(),
        orderBy: jest.fn().mockReturnThis(),
        getManyAndCount: jest.fn().mockReturnValue([[fakeArticle], 1])
      }));
      articleRepo.find.mockReturnValue([fakeArticle]);
      jest
        .spyOn(service, 'convertToShowDTO')
        .mockReturnValue(fakeReturnArticle);

      const result = await service.all(5, 1, 'test', 'test', 2);

      expect(result).toEqual([[fakeReturnArticle], 1]);
    });

    it('should return correct data when articleRepo returns empty array', async () => {
      articleRepo.createQueryBuilder.mockImplementation(() => ({
        where: jest.fn().mockReturnThis(),
        andWhere: jest.fn().mockReturnThis(),
        skip: jest.fn().mockReturnThis(),
        take: jest.fn().mockReturnThis(),
        orderBy: jest.fn().mockReturnThis(),
        getManyAndCount: jest.fn().mockReturnValue([[], 0])
      }));
      articleRepo.find.mockReturnValue([]);
      jest.spyOn(service, 'convertToShowDTO').mockReturnValue(null);

      const result = await service.all(5, 1, 'test', 'test', 2);

      expect(result).toEqual([[], 0]);
    });

    it('should call articleRepo.createQueryBuilder()', async () => {
      articleRepo.createQueryBuilder.mockImplementation(() => ({
        where: jest.fn().mockReturnThis(),
        andWhere: jest.fn().mockReturnThis(),
        skip: jest.fn().mockReturnThis(),
        take: jest.fn().mockReturnThis(),
        orderBy: jest.fn().mockReturnThis(),
        getManyAndCount: jest.fn().mockReturnValue([[fakeArticle], 1])
      }));
      articleRepo.find.mockReturnValue([fakeArticle]);
      jest
        .spyOn(service, 'convertToShowDTO')
        .mockReturnValue(fakeReturnArticle);
      const spy = jest.spyOn(articleRepo, 'createQueryBuilder');

      await service.all(undefined, undefined, undefined, undefined, undefined);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith('article');
    });

    it('should  call articleRepo.find() metod with correct data', async () => {
      articleRepo.createQueryBuilder.mockImplementation(() => ({
        where: jest.fn().mockReturnThis(),
        andWhere: jest.fn().mockReturnThis(),
        skip: jest.fn().mockReturnThis(),
        take: jest.fn().mockReturnThis(),
        orderBy: jest.fn().mockReturnThis(),
        getManyAndCount: jest.fn().mockReturnValue([[fakeArticle], 1])
      }));
      const spy = jest
        .spyOn(articleRepo, 'find')
        .mockReturnValue([fakeArticle]);
      jest
        .spyOn(service, 'convertToShowDTO')
        .mockReturnValue(fakeReturnArticle);

      await service.all(5, 1, 'test', 'test', 2);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith({
        where: { id: In([3]) },
        order: {
          updateTime: 'DESC'
        }
      });
    });
  });

  describe('Test metod getFlaggedByAdmin()', () => {
    it('should return correct data', async () => {
      articleRepo.findAndCount.mockReturnValue([[fakeArticle], 1]);
      jest
        .spyOn(service, 'convertUnmaskedToShowDTO')
        .mockReturnValue(fakeFlaggedArticle);

      const result = await service.getFlaggedByAdmin();

      expect(result).toEqual([[fakeFlaggedArticle], 1]);
    });

    it('should call articleRepo.findAndCount() with correct data', async () => {
      jest
        .spyOn(service, 'convertUnmaskedToShowDTO')
        .mockReturnValue(fakeFlaggedArticle);
      const spy = jest
        .spyOn(articleRepo, 'findAndCount')
        .mockReturnValue([[fakeArticle], 1]);

      await service.getFlaggedByAdmin();

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith({
        where: { isDeleted: false, isFlagged: true },
        order: { updateTime: 'DESC' }
      });
    });

    it('should call articleRepo.findAndCount() with correct data', async () => {
      articleRepo.findAndCount.mockReturnValue([[fakeArticle, fakeArticle], 2]);
      const spy = jest
        .spyOn(service, 'convertUnmaskedToShowDTO')
        .mockReturnValue(fakeFlaggedArticle);

      await service.getFlaggedByAdmin();

      expect(spy).toBeCalledTimes(2);
      expect(spy).toBeCalledWith(fakeArticle);
    });
  });

  describe('Test metod create()', () => {
    it('should return correct result articleRepo create() once with correct parameters', async () => {
      jest.spyOn(articleRepo, 'find').mockReturnValue(undefined);
      jest.spyOn(articleRepo, 'create').mockReturnValue(fakeArticleCreatedDTO);

      jest.spyOn(articleRepo, 'save').mockReturnValue({
        content: 'test',
        title: 'test',
        id: 3,
        user: fakeUser,
        comments: [],
        votes: []
      });

      jest.spyOn(articleRepo, 'findOne').mockReturnValue({
        content: 'test',
        title: 'test',
        id: 3,
        user: fakeUser,
        comments: [],
        votes: []
      });

      jest
        .spyOn(service, 'convertToShowDTO')
        .mockReturnValue(fakeReturnArticle);

      const result = await service.create(fakeArticleCreatedDTO, fakeUser);

      expect(result).toEqual(fakeReturnArticle);
    });

    it('should call articleRepo find() once with correct parameters', async () => {
      const spy = jest.spyOn(articleRepo, 'find').mockReturnValue(undefined);
      jest.spyOn(articleRepo, 'create').mockReturnValue(fakeArticleCreatedDTO);

      jest.spyOn(articleRepo, 'save').mockReturnValue({
        content: 'test',
        title: 'test',
        id: 3,
        user: fakeUser,
        comments: [],
        votes: []
      });

      jest.spyOn(articleRepo, 'findOne').mockReturnValue({
        content: 'test',
        title: 'test',
        id: 3,
        user: fakeUser,
        comments: [],
        votes: []
      });
      jest
        .spyOn(service, 'convertToShowDTO')
        .mockReturnValue(fakeReturnArticle);

      await service.create(fakeArticleCreatedDTO, fakeUser);

      expect(spy).toBeCalledWith({
        where: { title: 'test' }
      });
      expect(spy).toBeCalledTimes(1);
    });

    it('should call articleRepo find() once with correct parameters', async () => {
      jest.spyOn(articleRepo, 'find').mockImplementation(() => {
        throw new MySystemError(`test message`, 408);
      });
      jest.spyOn(articleRepo, 'create').mockReturnValue(fakeArticleCreatedDTO);

      jest.spyOn(articleRepo, 'save').mockReturnValue({
        content: 'test',
        title: 'test',
        id: 3,
        user: fakeUser,
        comments: [],
        votes: []
      });

      jest.spyOn(articleRepo, 'findOne').mockReturnValue({
        content: 'test',
        title: 'test',
        id: 3,
        user: fakeUser,
        comments: [],
        votes: []
      });

      jest
        .spyOn(service, 'convertToShowDTO')
        .mockReturnValue(fakeReturnArticle);
      let test: string;

      try {
        await service.create(fakeArticleCreatedDTO, fakeUser);
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe(`test message`);
    });

    it('should call articleRepo create() once with correct parameters', async () => {
      jest.spyOn(articleRepo, 'find').mockReturnValue(undefined);
      const spy = jest
        .spyOn(articleRepo, 'create')
        .mockReturnValue(fakeArticleCreatedDTO);

      jest.spyOn(articleRepo, 'save').mockReturnValue({
        content: 'test',
        title: 'test',
        id: 3,
        user: fakeUser,
        comments: [],
        votes: []
      });

      jest.spyOn(articleRepo, 'findOne').mockReturnValue({
        content: 'test',
        title: 'test',
        id: 3,
        user: fakeUser,
        comments: [],
        votes: []
      });

      jest
        .spyOn(service, 'convertToShowDTO')
        .mockReturnValue(fakeReturnArticle);

      await service.create(fakeArticleCreatedDTO, fakeUser);

      expect(spy).toBeCalledWith(fakeArticleCreatedDTO);
      expect(spy).toBeCalledTimes(1);
    });

    it('should call articleRepo save() once with correct parameters', async () => {
      jest.spyOn(articleRepo, 'find').mockReturnValue(undefined);
      jest.spyOn(articleRepo, 'create').mockReturnValue(fakeArticleCreatedDTO);

      const spy = jest.spyOn(articleRepo, 'save').mockReturnValue({
        content: 'test',
        title: 'test',
        id: 3,
        user: fakeUser,
        comments: [],
        votes: []
      });

      jest.spyOn(articleRepo, 'findOne').mockReturnValue({
        content: 'test',
        title: 'test',
        id: 3,
        user: fakeUser,
        comments: [],
        votes: []
      });

      jest
        .spyOn(service, 'convertToShowDTO')
        .mockReturnValue(fakeReturnArticle);

      await service.create(fakeArticleCreatedDTO, fakeUser);

      expect(spy).toBeCalledWith({
        content: 'test',
        title: 'test',
        user: fakeUser,
        comments: [],
        votes: []
      });
      expect(spy).toBeCalledTimes(1);
    });

    it('should call articleRepo findOne() once with correct parameters', async () => {
      jest.spyOn(articleRepo, 'find').mockReturnValue(undefined);
      jest.spyOn(articleRepo, 'create').mockReturnValue(fakeArticleCreatedDTO);

      jest.spyOn(articleRepo, 'save').mockReturnValue({
        content: 'test',
        title: 'test',
        id: 3,
        user: fakeUser,
        comments: [],
        votes: []
      });

      const spy = jest.spyOn(articleRepo, 'findOne').mockReturnValue({
        content: 'test',
        title: 'test',
        id: 3,
        user: fakeUser,
        comments: [],
        votes: []
      });

      jest
        .spyOn(service, 'convertToShowDTO')
        .mockReturnValue(fakeReturnArticle);

      await service.create(fakeArticleCreatedDTO, fakeUser);

      expect(spy).toBeCalledWith(3);
      expect(spy).toBeCalledTimes(1);
    });
  });

  describe('Test metod editQuery()', () => {
    it('should not call articleVoteRepo.createQueryBuilder metod when no query', async () => {
      const spy = articleVoteRepo.createQueryBuilder.mockImplementation(() => ({
        where: jest.fn().mockReturnThis(),
        andWhere: jest.fn().mockReturnThis(),
        getOne: jest.fn().mockReturnValue(fakePositiveVote)
      }));

      service.editQuery(fakeArticle, fakeUser);

      expect(spy).toBeCalledTimes(0);
    });

    it('should call articleVoteRepo.save metod with correct data when Vote is positive', async () => {
      articleVoteRepo.createQueryBuilder.mockImplementation(() => ({
        where: jest.fn().mockReturnThis(),
        andWhere: jest.fn().mockReturnThis(),
        getOne: jest.fn().mockReturnValue('')
      }));

      await service.editQuery(fakeArticle, fakeUser, true);

      expect(articleVoteRepo.save).toBeCalledTimes(1);
      expect(articleVoteRepo.save).toBeCalledWith({
        userId: fakeUser.id,
        articleId: fakeArticle.id,
        positive: true
      });
    });

    it('should call articleVoteRepo.createQueryBuilder metod twice with correct data when Vote is positive', async () => {
      let spy: string;
      articleVoteRepo.createQueryBuilder.mockImplementation(() => ({
        update: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        andWhere: jest.fn().mockReturnThis(),
        set: jest.fn().mockReturnThis(),
        getOne: jest.fn().mockReturnValue(fakeNegativeVote),
        execute: () => {
          spy = 'test';
        }
      }));

      await service.editQuery(fakeArticle, fakeUser, true);

      expect(articleVoteRepo.createQueryBuilder).toBeCalledTimes(2);
      expect(spy).toBe('test');
    });

    it('should call articleVoteRepo.createQueryBuilder metod once and then does nothing when Vote is Positive and found vote is Positive', async () => {
      let spy: string;
      articleVoteRepo.createQueryBuilder.mockImplementation(() => ({
        update: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        andWhere: jest.fn().mockReturnThis(),
        set: jest.fn().mockReturnThis(),
        getOne: jest.fn().mockReturnValue(fakePositiveVote),
        execute: () => {
          spy = 'test';
        }
      }));

      await service.editQuery(fakeArticle, fakeUser, true);

      expect(articleVoteRepo.createQueryBuilder).toBeCalledTimes(1);
      expect(spy).toBe(undefined);
    });

    it('should call articleVoteRepo.save metod with correct data when Vote is negative', async () => {
      articleVoteRepo.createQueryBuilder.mockImplementation(() => ({
        where: jest.fn().mockReturnThis(),
        andWhere: jest.fn().mockReturnThis(),
        getOne: jest.fn().mockReturnValue('')
      }));

      await service.editQuery(fakeArticle, fakeUser, false);

      expect(articleVoteRepo.save).toBeCalledTimes(1);
      expect(articleVoteRepo.save).toBeCalledWith({
        userId: fakeUser.id,
        articleId: fakeArticle.id,
        positive: false
      });
    });

    it('should call articleVoteRepo.createQueryBuilder metod twice with correct data when Vote is negative', async () => {
      let spy: string;
      articleVoteRepo.createQueryBuilder.mockImplementation(() => ({
        update: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        andWhere: jest.fn().mockReturnThis(),
        set: jest.fn().mockReturnThis(),
        getOne: jest.fn().mockReturnValue(fakePositiveVote),
        execute: () => {
          spy = 'test';
        }
      }));

      await service.editQuery(fakeArticle, fakeUser, false);

      expect(articleVoteRepo.createQueryBuilder).toBeCalledTimes(2);
      expect(spy).toBe('test');
    });

    it('should call articleVoteRepo.createQueryBuilder metod once and then does nothing when Vote is Negative and found vote is Negative', async () => {
      let spy: string;
      articleVoteRepo.createQueryBuilder.mockImplementation(() => ({
        update: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        andWhere: jest.fn().mockReturnThis(),
        set: jest.fn().mockReturnThis(),
        getOne: jest.fn().mockReturnValue(fakeNegativeVote),
        execute: () => {
          spy = 'test';
        }
      }));

      await service.editQuery(fakeArticle, fakeUser, false);

      expect(articleVoteRepo.createQueryBuilder).toBeCalledTimes(1);
      expect(spy).toBe(undefined);
    });

    it('should call articleVoteRepo.createQueryBuilder metod with correct data when Flag is positive', async () => {
      let spy: string;
      articleRepo.createQueryBuilder.mockImplementation(() => ({
        update: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        andWhere: jest.fn().mockReturnThis(),
        set: jest.fn().mockReturnThis(),
        getOne: jest.fn().mockReturnThis(),
        execute: () => {
          spy = 'test';
        }
      }));

      await service.editQuery(fakeArticle, fakeUser, undefined, true);

      expect(articleRepo.createQueryBuilder).toBeCalledTimes(1);
      expect(spy).toBe('test');
    });

    it('should throw an error with correct message from error', async () => {
      articleRepo.createQueryBuilder.mockImplementation(() => {
        throw new MySystemError(`test message`, 408);
      });
      let test: string;

      try {
        await service.editQuery(fakeArticle, fakeUser, undefined, true);
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe(`test message`);
    });

    it('should throw an error with correct message when no Error message provided', async () => {
      articleRepo.createQueryBuilder.mockImplementation(() => {
        throw new MySystemError();
      });
      fakeArticle.isLocked = true;
      let test: string;

      try {
        await service.editQuery(fakeArticle, fakeUser, undefined, true);
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe(`This article is locked!`);
    });

    it('should throw an error with correct message when no Error message provided', async () => {
      articleRepo.createQueryBuilder.mockImplementation(() => {
        throw new MySystemError();
      });
      let test: string;

      try {
        await service.editQuery(fakeArticle, fakeUser, undefined, true);
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe(
        `Database took too much to respond, please try again later`
      );
    });
  });

  describe('Test metod editContent()', () => {
    it('should call service.editContent metod when content is provided', async () => {
      const spy = jest.spyOn(service, 'editContent');
      articleRepo.createQueryBuilder.mockImplementation(() => ({
        update: jest.fn().mockReturnThis(),
        set: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        execute: jest.fn().mockReturnThis()
      }));
      await service.editContent(fakeArticle, fakeUser, 'TestNewContent');

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(fakeArticle, fakeUser, 'TestNewContent');
    });

    it('should call service.editContent metod when no content is provided', async () => {
      const spy = jest.spyOn(service, 'editContent');
      articleRepo.createQueryBuilder.mockImplementation(() => ({
        update: jest.fn().mockReturnThis(),
        set: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        execute: jest.fn().mockReturnThis()
      }));

      await service.editContent(fakeArticle, fakeUser, undefined);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(fakeArticle, fakeUser, undefined);
    });

    it('should call articleRepo.createQueryBuilder() metod ', async () => {
      const spy = articleRepo.createQueryBuilder.mockImplementation(() => ({
        update: jest.fn().mockReturnThis(),
        set: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        execute: jest.fn().mockReturnThis()
      }));

      await service.editContent(fakeArticle, fakeUser, undefined);

      expect(spy).toBeCalledTimes(1);
    });

    it("should throw an Error when article.user and user doesn't match", async () => {
      const fakeUser2 = {
        id: 6,
        username: 'Test',
        password: 'testP@ss',
        avatarUrl: 'http://this.jpg',
        role: { name: 'Guest' }
      } as User;
      articleRepo.createQueryBuilder.mockImplementation(() => ({
        update: jest.fn().mockReturnThis(),
        set: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        execute: jest.fn().mockReturnThis()
      }));

      let test: string;

      try {
        await service.editContent(fakeArticle, fakeUser2, 'TestNewContent');
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe(`This is not your article so you can't edit it!`);
    });
  });

  describe('Test metod editContentByAdmin()', () => {
    it('should call articleRepository.createQueryBuilder metod when content is provided', async () => {
      let spy: string;
      articleRepo.createQueryBuilder.mockImplementation(() => ({
        update: jest.fn().mockReturnThis(),
        set: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        execute: () => {
          spy = 'test';
        }
      }));
      await service.editContentByAdmin(fakeArticle, fakeUser, 'TestNewContent');

      expect(articleRepo.createQueryBuilder).toBeCalledTimes(1);
      expect(spy).toBe('test');
    });

    it('should call articleRepository.createQueryBuilder metod when no content is provided', async () => {
      let spy: string;
      articleRepo.createQueryBuilder.mockImplementation(() => ({
        update: jest.fn().mockReturnThis(),
        set: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        execute: () => {
          spy = 'test';
        }
      }));
      await service.editContentByAdmin(fakeArticle, fakeUser, undefined);

      expect(articleRepo.createQueryBuilder).toBeCalledTimes(1);
      expect(spy).toBe('test');
    });

    it('should throw an Error with correct message when content is provided', async () => {
      articleRepo.createQueryBuilder.mockImplementation(() => {
        throw new MySystemError(`Test error`, 408);
      });

      let test: string;

      try {
        await service.editContentByAdmin(fakeArticle, fakeUser, 'TestNewContent');
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe(`Test error`);
    });

    it('should throw an Error with correct message when content is provided and no error message', async () => {
      articleRepo.createQueryBuilder.mockImplementation(() => {
        throw new MySystemError();
      });

      let test: string;

      try {
        await service.editContentByAdmin(fakeArticle, fakeUser, 'TestNewContent');
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe(
        `Database took too much to respond, please try again later`
      );
    });

    it('should throw an Error with correct message when no content is provided', async () => {
      articleRepo.createQueryBuilder.mockImplementation(() => {
        throw new MySystemError(
          `Database took too much to respond, please try again later`,
          408
        );
      });

      let test: string;

      try {
        await service.editContentByAdmin(fakeArticle, fakeUser, undefined);
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe(
        `Database took too much to respond, please try again later`
      );
    });

    it('should throw an Error with correct message when no content is provided and no error message', async () => {
      articleRepo.createQueryBuilder.mockImplementation(() => {
        throw new MySystemError();
      });

      let test: string;

      try {
        await service.editContentByAdmin(fakeArticle, fakeUser, undefined);
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe(
        `Database took too much to respond, please try again later`
      );
    });
  });

  describe('Test metod toggleFlagByAdmin()', () => {
    it('should call metod createQueryBuilder()', async () => {
      let spy: string;
      const spy2 = articleRepo.createQueryBuilder.mockImplementation(() => ({
        update: jest.fn().mockReturnThis(),
        set: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        execute: () => {
          spy = 'test';
        }
      }));
      await service.toggleFlagByAdmin(fakeArticle);

      expect(spy2).toBeCalledTimes(1);
      expect(spy).toBe('test');
    });

    it("should throw an Error with passed message when database doesn't respond", async () => {
      articleRepo.createQueryBuilder.mockImplementation(() => {
        throw new MySystemError(`Test message`, 408);
      });
      let test: string;

      try {
        await service.toggleFlagByAdmin(fakeArticle);
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe(`Test message`);
    });

    it("should throw an Error with generic message when database doesn't respond", async () => {
      articleRepo.createQueryBuilder.mockImplementation(() => {
        throw new MySystemError();
      });
      let test: string;

      try {
        await service.toggleFlagByAdmin(fakeArticle);
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe(
        `Database took too much to respond, please try again later`
      );
    });
  });

  describe('Test metod toggleLockByAdmin()', () => {
    it('should call metod createQueryBuilder()', async () => {
      let spy: string;
      const spy2 = articleRepo.createQueryBuilder.mockImplementation(() => ({
        update: jest.fn().mockReturnThis(),
        set: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        execute: () => {
          spy = 'test';
        }
      }));
      await service.toggleLockByAdmin(fakeArticle);

      expect(spy2).toBeCalledTimes(1);
      expect(spy).toBe('test');
    });

    it("should throw an Error with passed message when database doesn't respond", async () => {
      articleRepo.createQueryBuilder.mockImplementation(() => {
        throw new MySystemError(`Test message`, 408);
      });
      let test: string;

      try {
        await service.toggleLockByAdmin(fakeArticle);
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe(`Test message`);
    });

    it("should throw an Error with generic message when database doesn't respond", async () => {
      articleRepo.createQueryBuilder.mockImplementation(() => {
        throw new MySystemError();
      });
      let test: string;

      try {
        await service.toggleLockByAdmin(fakeArticle);
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe(
        `Database took too much to respond, please try again later`
      );
    });
  });

  describe('Test metod delete()', () => {
    it('should call metod delete()', async () => {
      let spy: string;
      const spy2 = articleRepo.createQueryBuilder.mockImplementation(() => ({
        update: jest.fn().mockReturnThis(),
        set: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        execute: () => {
          spy = 'test';
        }
      }));
      await service.delete(fakeArticle);

      expect(spy2).toBeCalledTimes(1);
      expect(spy).toBe('test');
    });

    it("should throw an Error with passed message when database doesn't respond", async () => {
      articleRepo.createQueryBuilder.mockImplementation(() => {
        throw new MySystemError(`Test message`, 408);
      });
      let test: string;

      try {
        await service.delete(fakeArticle);
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe(`Test message`);
    });

    it("should throw an Error with generic message when database doesn't respond", async () => {
      articleRepo.createQueryBuilder.mockImplementation(() => {
        throw new MySystemError();
      });
      let test: string;

      try {
        await service.delete(fakeArticle);
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe(
        `Database took too much to respond, please try again later`
      );
    });
  });

  describe('Test metod convertToShowDTO()', () => {
    it('should return correct result when article is not flagged', () => {
      jest
        .spyOn(service, 'convertUnmaskedToShowDTO')
        .mockReturnValue(fakeFlaggedArticle);

      const result = service.convertToShowDTO(fakeReturnArticle);

      expect(result).toEqual(fakeReturnArticle);
    });
    it('should return correct result when article is flagged', () => {
      jest.spyOn(service, 'convertUnmaskedToShowDTO').mockReturnValue(
        (() => {
          fakeFlaggedArticle.isFlagged = true;
          return fakeFlaggedArticle;
        })()
      );

      const result = service.convertToShowDTO(fakeArticle);

      expect(result.content).toBe(
        `This Article is flagged, please edit it's content or it will be deleted!`
      );
    });

    it('should call metod convertUnmaskedToShowDTO()', () => {
      const spy = jest
        .spyOn(service, 'convertUnmaskedToShowDTO')
        .mockReturnValue(fakeFlaggedArticle);

      service.convertToShowDTO(fakeReturnArticle);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(fakeReturnArticle);
    });

    it('should call metod convertToShowDTO() with correct result', () => {
      jest
        .spyOn(service, 'convertUnmaskedToShowDTO')
        .mockReturnValue(fakeFlaggedArticle);

      const result = service.convertToShowDTO(fakeFlaggedArticle);

      expect(result).toEqual(fakeReturnArticle);
    });
  });
});
