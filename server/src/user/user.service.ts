/* eslint-disable @typescript-eslint/camelcase */
import { Injectable } from '@nestjs/common';
import { UserUpdateDTO } from './../models/user/user-update-dto';
import { UserCreateDTO } from './../models/user/user-create-dto';
import { User } from '../data/entities/user.entity';
import { UserReturnDTO } from '../models/user/user-return-dto';
import { UserQueryDTO } from '../models/user/user-query-dto';
import { InjectRepository } from '@nestjs/typeorm';
import {
  Repository,
  In,
  getRepository,
  Transaction,
  TransactionRepository
} from 'typeorm';
import { Role } from '../data/entities/role.entity';
import * as bcrypt from 'bcrypt';
import { plainToClass } from 'class-transformer';
import { MySystemError } from '../common/exceptions/my-system.error';
import { UpdateUserRolesDTO } from '../models/user/user-update-role-dto';
import { Friends } from '../data/entities/friends.entity';
import { UserActivityDTO } from '../models/user/user-activity-dto';
import { Article } from '../data/entities/article.entity';
import { Comment } from '../data/entities/comment.entity';
import { ArticleVote } from '../data/entities/article-vote.entity';
import { CommentVote } from '../data/entities/comment-vote.entity';
import { FriendsReturnDTO } from '../models/friends/friends-return-dto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    @InjectRepository(Role) private readonly rolesRepository: Repository<Role>,
    @InjectRepository(Friends)
    private readonly friendsRepository: Repository<Friends>
  ) {}

  async getAll(query: UserQueryDTO): Promise<UserReturnDTO[]> {
    let repoQuery = this.usersRepository
      .createQueryBuilder()
      .where('user.isDeleted = :del', { del: false });

    if (query.username)
      repoQuery = repoQuery.andWhere('user.username like :search', {
        search: `%${query.username}%`
      });
    if (query.email)
      repoQuery = repoQuery.andWhere('article.title = :email', {
        email: `%${query.email}%`
      });

    let userIds: any = await repoQuery.getMany();

    userIds = userIds.map((user: User) => user.id);

    const users: User[] = await this.usersRepository.find({
      id: In(userIds)
    });

    return users.map((user: User) => this.convertToShowDTO(user));
  }

  async getUserById(id: number): Promise<UserReturnDTO> {
    const userFound = await this.usersRepository.findOne({
      id,
      isDeleted: false
    });

    if (!userFound) {
      throw new MySystemError('User not found!', 404);
    }

    return this.convertToShowDTO(userFound);
  }

  async createUser(user: UserCreateDTO): Promise<UserReturnDTO> {
    const { username } = user;

    const foundUser: User = await this.usersRepository.findOne({
      where: {
        username,
        isDeleted: false
      }
    });
    if (foundUser) {
      throw new MySystemError('User with such username already exists!', 400);
    }

    const guestRole = await this.rolesRepository.findOne({
      where: {
        name: 'GUEST'
      }
    });

    const newUserEntity = this.usersRepository.create(user);

    newUserEntity.role = guestRole;
    newUserEntity.password = await bcrypt.hash(newUserEntity.password, 10);

    const savedUser: User = await this.usersRepository.save(newUserEntity);

    return this.convertToShowDTO(savedUser);
  }

  async updateUser(id: number, user: UserUpdateDTO): Promise<UserReturnDTO> {
    
    const foundUser: User = await this.usersRepository.findOne({ id });
    if (foundUser === undefined || foundUser.isDeleted) {
      throw new MySystemError('No such user found', 404);
    }

    const entityToUpdate: User = { ...foundUser, ...user };
    const savedUser: User = await this.usersRepository.save(entityToUpdate);
    return this.convertToShowDTO(savedUser);
  }

  public async updateUserRoles(
    id: number,
    updateUserRoles: UpdateUserRolesDTO
  ): Promise<UserReturnDTO> {
    const foundUser: User = await this.usersRepository.findOne({ id });
    if (foundUser === undefined || foundUser.isDeleted) {
      throw new MySystemError('No such user found', 404);
    }

    const foundRole: Role = await this.rolesRepository.findOne({
      where: {
        name: updateUserRoles.role
      }
    });

    const entityToUpdate: User = { ...foundUser, role: foundRole };

    const savedUser: User = await this.usersRepository.save(entityToUpdate);

    return this.convertToShowDTO(savedUser);
  }

  async deleteUser(id: number): Promise<void> {
    const foundUser = await this.usersRepository.findOne({
      id,
      isDeleted: false
    });

    if (!foundUser) {
      throw new MySystemError(`User with such id ${id} doesn\'t exists!`, 400);
    }

    foundUser.isDeleted = true;

    await this.usersRepository.save(foundUser);
  }

  async getFriends(user: User): Promise<FriendsReturnDTO[]> {
    const friends: any = await this.friendsRepository.find({
      where: { userId_1: user.id }
    });

    return friends.map((friendship: any) => {
      friendship.user2 = this.convertToShowDTO(friendship.user2);
      return plainToClass(FriendsReturnDTO, friendship, {
        excludeExtraneousValues: true
      });
    });
  }

  async addFriendToUser(user: User, friendToAdd: User): Promise<void> {
    const bond1: Friends = this.friendsRepository.create({
      user1: user,
      user2: friendToAdd,
      pending: false
    });
    const bond2: Friends = this.friendsRepository.create({
      user1: friendToAdd,
      user2: user
    });
    try {
      await this.friendsRepository.save([bond1, bond2]);
    } catch (err) {
      throw new MySystemError(
        err.message ||
          `Database took too much to respond, please try again later`,
        408
      );
    }
  }

  async acceptFriendToUser(user: User, friendToAdd: User): Promise<void> {
    const bond: Friends = await this.friendsRepository.findOne({
      where: {
        user1: user,
        user2: friendToAdd,
        pending: true
      }
    });
    bond.pending = false;
    try {
      await this.friendsRepository.save(bond);
    } catch (err) {
      throw new MySystemError(
        err.message ||
          `Database took too much to respond, please try again later`,
        408
      );
    }
  }

  @Transaction()
  async removeFriendFromUser(
    user: User,
    friendToDelete: User,
    @TransactionRepository(Friends) repo?: Repository<Friends>
  ): Promise<void> {
    try {
      await repo.delete({
        userId_1: user.id,
        userId_2: friendToDelete.id
      });
      await repo.delete({
        userId_1: friendToDelete.id,
        userId_2: user.id
      });
    } catch (err) {
      throw new MySystemError(
        err.message ||
          `Database took too much to respond, please try again later`,
        408
      );
    }
  }

  async toggleBanUser(userId: number): Promise<void> {
    const userEntity: User = await this.usersRepository.findOne({
      id: userId,
      isDeleted: false
    });

    if (userEntity === undefined) {
      throw new MySystemError(`User with such id ${userId} doesn\'t exists!`, 400);
    }
    userEntity.isBanned = !userEntity.isBanned;
    await this.usersRepository.save(userEntity);
  }

  async userActivity(user: User, requestUser: User): Promise<UserActivityDTO> {
    const friends: Friends = await this.friendsRepository.findOne({
      where: { userId_1: user.id, userId_2: requestUser.id, pending: false }
    });

    if (user.id !== requestUser.id && !friends) {
      throw new MySystemError(
        `This is not your friend or friendship is not accepted, so u can't see that user's activity!`,
        401
      );
    }

    const userActivity = {
      userId: user.id,
      articles: 0,
      comments: 0,
      upvoteArticles: 0,
      upvoteComments: 0,
      downvoteArticles: 0,
      downvoteComments: 0
    };

    let _: any;
    try {
      [_, userActivity.articles] = await getRepository(Article).findAndCount({
        where: { user: user, isDeleted: false }
      });
      [_, userActivity.comments] = await getRepository(Comment).findAndCount({
        where: { user: user, isDeleted: false }
      });
      [_, userActivity.upvoteArticles] = await getRepository(
        ArticleVote
      ).findAndCount({ where: { userId: user.id, positive: true } });
      [_, userActivity.upvoteComments] = await getRepository(
        CommentVote
      ).findAndCount({ where: { userId: user.id, positive: true } });
      [_, userActivity.downvoteArticles] = await getRepository(
        ArticleVote
      ).findAndCount({ where: { userId: user.id, positive: false } });
      [_, userActivity.downvoteComments] = await getRepository(
        CommentVote
      ).findAndCount({ where: { userId: user.id, positive: false } });
    } catch (err) {
      throw new MySystemError(
        err.message ||
          `Database took too much to respond, please try again later`,
        408
      );
    }

    return userActivity;
  }

  public convertToShowDTO(user: User): UserReturnDTO {
    user.role = user.role.name as any;
    return plainToClass(UserReturnDTO, user, {
      excludeExtraneousValues: true
    });
  }
}
