import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from './user.service';
import { Repository, In } from 'typeorm';
import { User } from '../data/entities/user.entity';
import { Role } from '../data/entities/role.entity';
import { Friends } from '../data/entities/friends.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UserReturnDTO } from '../models/user/user-return-dto';
import { UserQueryDTO } from '../models/user/user-query-dto';
import { MockType, repositoryMockFactory } from '../common/types/mock-type';
import { UserCreateDTO } from '../models/user/user-create-dto';
import * as bcrypt from 'bcrypt';
import { UserUpdateDTO } from '../models/user/user-update-dto';
import { UpdateUserRolesDTO } from '../models/user/user-update-role-dto';
import { MySystemError } from '../common/exceptions/my-system.error';
import typeorm = require('typeorm');
import { Article } from '../data/entities/article.entity';
import { Comment } from '../data/entities/comment.entity';
import { ArticleVote } from '../data/entities/article-vote.entity';
import { CommentVote } from '../data/entities/comment-vote.entity';

describe('UserService', () => {
  let service: UserService;
  let userRepositoryMock: MockType<Repository<User>>;
  let roleRepositoryMock: MockType<Repository<Role>>;
  let friendsRepositoryMock: MockType<Repository<Friends>>;

  const mockUserQuery = {
    username: 'Petar',
    email: 'Petar@the.best'
  } as UserQueryDTO;

  let mockUser: User;

  let mockUser2: User;

  const mockReturnUser = {
    id: 2,
    username: 'Test',
    avatarUrl: 'http://this.jpg',
    role: 'GUEST'
  } as UserReturnDTO;

  const mockCreateUser = {
    username: 'Petar',
    password: 'TestP@ss',
    email: 'Petar@the.best'
  } as UserCreateDTO;

  let mockSaveUser: Partial<User>;

  const mockReturnUpdatedUser = {
    id: 2,
    username: 'Test',
    avatarUrl: 'http://testUpdate.jpg',
    role: 'GUEST'
  } as UserReturnDTO;

  const mockReturnUpdatedRoleUser = {
    id: 2,
    username: 'Test',
    avatarUrl: 'http://this.jpg',
    role: 'ADMIN'
  } as UserReturnDTO;

  const mockUserUpdate: UserUpdateDTO = {
    avatarUrl: 'http://testUpdate.jpg'
  };

  let mockUserUpdateRole: UpdateUserRolesDTO;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: getRepositoryToken(User),
          useFactory: repositoryMockFactory
        },
        {
          provide: getRepositoryToken(Role),
          useFactory: repositoryMockFactory
        },
        {
          provide: getRepositoryToken(Friends),
          useFactory: repositoryMockFactory
        }
      ]
    }).compile();

    service = module.get<UserService>(UserService);
    userRepositoryMock = module.get(getRepositoryToken(User));
    roleRepositoryMock = module.get(getRepositoryToken(Role));
    friendsRepositoryMock = module.get(getRepositoryToken(Friends));

    mockUser = {
      id: 2,
      username: 'Test',
      password: 'testP@ss',
      avatarUrl: 'http://this.jpg',
      role: { name: 'GUEST' }
    } as User;

    mockUser2 = {
      id: 3,
      username: 'Test',
      password: 'testP@ss',
      avatarUrl: 'http://this.jpg',
      role: { name: 'GUEST' }
    } as User;

    mockUserUpdateRole = {
      role: 'ADMIN'
    } as UpdateUserRolesDTO;

    mockSaveUser = {
      username: 'Petar',
      password: 'bcryptMock',
      email: 'Petar@the.best',
      role: { id: 3, name: 'GUEST' }
    };

    typeorm.getRepository = jest.fn().mockReturnValue({
      findAndCount: jest
        .fn()
        .mockReturnValueOnce([[], 1])
        .mockReturnValueOnce([[], 2])
        .mockReturnValueOnce([[], 3])
        .mockReturnValueOnce([[], 4])
        .mockReturnValueOnce([[], 5])
        .mockReturnValueOnce([[], 6])
    });

    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(userRepositoryMock).toBeDefined();
    expect(roleRepositoryMock).toBeDefined();
    expect(friendsRepositoryMock).toBeDefined();
  });

  describe('Test metod getAll()', () => {
    it('should return correct result with Query', async () => {
      userRepositoryMock.createQueryBuilder.mockImplementation(() => ({
        where: jest.fn().mockReturnThis(),
        andWhere: jest.fn().mockReturnThis(),
        getMany: jest.fn().mockReturnValue([mockUser])
      }));
      userRepositoryMock.find.mockReturnValue([mockUser]);

      const result = await service.getAll(mockUserQuery);

      expect(result).toEqual([mockReturnUser]);
    });

    it('should return correct result without Query', async () => {
      userRepositoryMock.createQueryBuilder.mockImplementation(() => ({
        where: jest.fn().mockReturnThis(),
        andWhere: jest.fn().mockReturnThis(),
        getMany: jest.fn().mockReturnValue([mockUser])
      }));
      userRepositoryMock.find.mockReturnValue([mockUser]);

      const result = await service.getAll({} as UserQueryDTO);

      expect(result).toEqual([mockReturnUser]);
    });

    it('should call mockRepository.queryBuilder metod', async () => {
      const spy = jest.spyOn(userRepositoryMock, 'createQueryBuilder');
      userRepositoryMock.createQueryBuilder.mockImplementation(() => ({
        where: jest.fn().mockReturnThis(),
        andWhere: jest.fn().mockReturnThis(),
        getMany: jest.fn().mockReturnValue([mockUser])
      }));
      userRepositoryMock.find.mockReturnValue([mockUser]);

      await service.getAll(mockUserQuery);

      expect(spy).toBeCalledTimes(1);
    });

    it('should call mockRepository.find metod', async () => {
      userRepositoryMock.createQueryBuilder.mockImplementation(() => ({
        where: jest.fn().mockReturnThis(),
        andWhere: jest.fn().mockReturnThis(),
        getMany: jest.fn().mockReturnValue([mockUser])
      }));
      userRepositoryMock.find.mockReturnValue([mockUser]);
      const spy = jest.spyOn(userRepositoryMock, 'find');

      await service.getAll(mockUserQuery);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith({
        id: In([2])
      });
    });
  });

  describe('Test metod getUserById()', () => {
    it('should return correct result', async () => {
      userRepositoryMock.findOne.mockReturnValue(mockUser);
      const result = await service.getUserById(2);

      expect(result).toEqual(mockReturnUser);
    });

    it('should call mockRepository.findOne metod', async () => {
      userRepositoryMock.findOne.mockReturnValue(mockUser);
      const spy = jest.spyOn(userRepositoryMock, 'findOne');

      await service.getUserById(7);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith({
        id: 7,
        isDeleted: false
      });
    });

    it('should throw an Error with specified message', async () => {
      userRepositoryMock.findOne.mockReturnValue(null);
      let test: string;
      try {
        await service.getUserById(4);
      } catch (error) {
        test = error.message;
      }
      expect(test).toBe('User not found!');
    });
  });

  describe('Test metod getFriends()', () => {
    it('should return correct result', async () => {
      jest
        .spyOn(friendsRepositoryMock, 'find')
        .mockImplementation(jest.fn().mockReturnThis())
        .mockReturnValue([
          { user1: mockUser2, user2: mockUser, pending: false }
        ]);

      const result = await service.getFriends(mockUser2);

      expect(result).toEqual([{ user2: mockReturnUser, pending: false }]);
    });

    it('should call mockRepository.find metod', async () => {
      const spy = jest
        .spyOn(friendsRepositoryMock, 'find')
        .mockImplementation(jest.fn().mockReturnThis())
        .mockReturnValue([
          { user1: mockUser2, user2: mockUser, pending: false }
        ]);

      await service.getFriends(mockUser2);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith({
        where: { userId_1: 3 }
      });
    });
  });

  describe('Test metod createUser()', () => {
    it('should return correct result', async () => {
      userRepositoryMock.findOne.mockReturnValue('');
      userRepositoryMock.create.mockReturnValue(mockCreateUser);
      userRepositoryMock.save.mockReturnValue(mockUser);
      roleRepositoryMock.findOne.mockReturnValue({ id: 3, name: 'GUEST' });

      const result = await service.createUser(mockCreateUser);

      expect(result).toEqual(mockReturnUser);
    });

    it('should call mockRepository.findOne metod', async () => {
      userRepositoryMock.findOne.mockReturnValue('');
      userRepositoryMock.create.mockReturnValue(mockCreateUser);
      userRepositoryMock.save.mockReturnValue(mockUser);
      roleRepositoryMock.findOne.mockReturnValue({ id: 3, name: 'GUEST' });
      const spy = jest.spyOn(userRepositoryMock, 'findOne');

      await service.createUser(mockCreateUser);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith({
        where: {
          username: mockCreateUser.username,
          isDeleted: false
        }
      });
    });

    it('should call mockRepository.create metod', async () => {
      userRepositoryMock.findOne.mockReturnValue('');
      userRepositoryMock.create.mockReturnValue(mockCreateUser);
      userRepositoryMock.save.mockReturnValue(mockUser);
      roleRepositoryMock.findOne.mockReturnValue({ id: 3, name: 'GUEST' });
      const spy = jest.spyOn(userRepositoryMock, 'create');

      await service.createUser(mockCreateUser);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(mockCreateUser);
    });

    it('should call mockRepository.save metod', async () => {
      userRepositoryMock.findOne.mockReturnValue('');
      userRepositoryMock.create.mockReturnValue(mockCreateUser);
      userRepositoryMock.save.mockReturnValue(mockUser);
      roleRepositoryMock.findOne.mockReturnValue({ id: 3, name: 'GUEST' });
      jest.spyOn(bcrypt, 'hash').mockReturnValue(Promise.resolve('bcryptMock'));
      const spy = jest.spyOn(userRepositoryMock, 'save');

      await service.createUser(mockCreateUser);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(mockSaveUser);
    });

    it('should throw an Error with specified message', async () => {
      userRepositoryMock.findOne.mockReturnValue('user');
      let test: string;
      try {
        await service.createUser(mockCreateUser);
      } catch (error) {
        test = error.message;
      }
      expect(test).toBe('User with such username already exists!');
    });
  });

  describe('Test metod updateUser()', () => {
    it('should return correct result', async () => {
      userRepositoryMock.findOne.mockReturnValue(mockUser);

      const result = await service.updateUser(2, mockUserUpdate);

      expect(result).toEqual(mockReturnUpdatedUser);
    });

    it('should call mockRepository.findOne metod', async () => {
      userRepositoryMock.findOne.mockReturnValue(mockUser);
      const spy = jest.spyOn(userRepositoryMock, 'findOne');

      await service.updateUser(2, mockUserUpdate);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith({ id: 2 });
    });

    it('should throw na Error when findOne metod returns undefined', async () => {
      userRepositoryMock.findOne.mockReturnValue(undefined);
      let test: string;

      try {
        await service.updateUser(2, mockUserUpdate);
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe('No such user found');
    });

    it('should call mockRepository.save metod with correct data', async () => {
      userRepositoryMock.findOne.mockReturnValue(mockUser);
      const spy = jest.spyOn(userRepositoryMock, 'save');
      const rawUpdatedUser = {
        id: 2,
        username: 'Test',
        avatarUrl: 'http://testUpdate.jpg',
        password: 'testP@ss',
        role: 'GUEST'
      };

      await service.updateUser(2, mockUserUpdate);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(rawUpdatedUser);
    });
  });

  describe('Test metod updateUserRoles()', () => {
    it('should return correct result', async () => {
      userRepositoryMock.findOne.mockReturnValue(mockUser);
      roleRepositoryMock.findOne.mockReturnValue({ id: 1, name: 'ADMIN' });

      const result = await service.updateUserRoles(2, mockUserUpdateRole);

      expect(result).toEqual(mockReturnUpdatedRoleUser);
    });

    it('should call mockRepository.findOne metod', async () => {
      userRepositoryMock.findOne.mockReturnValue(mockUser);
      roleRepositoryMock.find.mockReturnValue({ id: 1, name: 'ADMIN' });
      const spy = jest.spyOn(userRepositoryMock, 'findOne');

      await service.updateUserRoles(2, mockUserUpdateRole);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith({ id: 2 });
    });

    it('should throw na Error when findOne metod returns undefined', async () => {
      userRepositoryMock.findOne.mockReturnValue(undefined);
      roleRepositoryMock.find.mockReturnValue({ id: 1, name: 'ADMIN' });
      let test: string;

      try {
        await service.updateUserRoles(2, mockUserUpdateRole);
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe('No such user found');
    });

    it('should call mockRepository.save metod with correct data', async () => {
      userRepositoryMock.findOne.mockReturnValue(mockUser);
      roleRepositoryMock.findOne.mockReturnValue({ id: 1, name: 'ADMIN' });
      const spy = jest.spyOn(userRepositoryMock, 'save');
      const rawUpdatedUser = {
        id: 2,
        username: 'Test',
        avatarUrl: 'http://this.jpg',
        password: 'testP@ss',
        role: 'ADMIN'
      };

      await service.updateUserRoles(2, mockUserUpdateRole);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(rawUpdatedUser);
    });
  });

  describe('Test metod deleteUser()', () => {
    it('should call mockRepository.findOne metod with correct data', async () => {
      const spy = jest
        .spyOn(userRepositoryMock, 'findOne')
        .mockReturnValue(mockUser);

      await service.deleteUser(2);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith({
        id: 2,
        isDeleted: false
      });
    });

    it('should call mockRepository.save metod with correct data', async () => {
      userRepositoryMock.findOne.mockReturnValue(mockUser);
      const spy = jest.spyOn(userRepositoryMock, 'save');

      await service.deleteUser(2);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(mockUser);
    });

    it('should throw na Error when findOne metod returns undefined', async () => {
      userRepositoryMock.findOne.mockReturnValue(undefined);
      let test: string;

      try {
        await service.deleteUser(2);
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe(`User with such id 2 doesn\'t exists!`);
    });
  });

  describe('Test metod addFriendToUser()', () => {
    it('should call mockRepository.create metod correct times with correct data', async () => {
      const spy = jest
        .spyOn(friendsRepositoryMock, 'create')
        .mockReturnValueOnce({
          user1: mockUser,
          user2: mockUser2,
          pending: false
        })
        .mockReturnValueOnce({ user1: mockUser2, user2: mockUser });
      friendsRepositoryMock.save.mockReturnThis();

      await service.addFriendToUser(mockUser, mockUser2);

      expect(spy).toBeCalledTimes(2);
      expect(spy).toHaveBeenNthCalledWith(1, {
        user1: mockUser,
        user2: mockUser2,
        pending: false
      });
      expect(spy).toHaveBeenNthCalledWith(2, {
        user1: mockUser2,
        user2: mockUser
      });
    });

    it('should call mockRepository.save metod with correct data', async () => {
      friendsRepositoryMock.create
        .mockReturnValueOnce({ user1: mockUser, user2: mockUser2 })
        .mockReturnValueOnce({ user1: mockUser2, user2: mockUser });
      const spy = jest.spyOn(friendsRepositoryMock, 'save').mockReturnThis();

      await service.addFriendToUser(mockUser, mockUser2);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toHaveBeenNthCalledWith(1, [
        {
          user1: mockUser,
          user2: mockUser2
        },
        {
          user1: mockUser2,
          user2: mockUser
        }
      ]);
    });

    it('should throw na Error when save metod fails', async () => {
      friendsRepositoryMock.create
        .mockReturnValueOnce({ user1: mockUser, user2: mockUser2 })
        .mockReturnValueOnce({ user1: mockUser2, user2: mockUser });
      friendsRepositoryMock.save.mockImplementation(() => {
        throw new MySystemError(`TestErrorMessage`, 408);
      });
      let test: string;

      try {
        await service.addFriendToUser(mockUser, mockUser2);
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe(`TestErrorMessage`);
    });

    it('should throw an Error with generic message when save metod fails', async () => {
      friendsRepositoryMock.create
        .mockReturnValueOnce({ user1: mockUser, user2: mockUser2 })
        .mockReturnValueOnce({ user1: mockUser2, user2: mockUser });
      friendsRepositoryMock.save.mockImplementation(() => {
        throw new MySystemError();
      });
      let test: string;

      try {
        await service.addFriendToUser(mockUser, mockUser2);
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe(
        `Database took too much to respond, please try again later`
      );
    });
  });

  describe('Test metod acceptFriendToUser()', () => {
    it('should call mockRepository.create metod correct times with correct data', async () => {
      const spy = jest.spyOn(friendsRepositoryMock, 'findOne').mockReturnValue({
        user1: mockUser,
        user2: mockUser2,
        pending: true
      });
      // friendsRepositoryMock.save.mockReturnThis();

      await service.acceptFriendToUser(mockUser, mockUser2);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toHaveBeenCalledWith({
        where: {
          user1: mockUser,
          user2: mockUser2,
          pending: true
        }
      });
    });

    it('should call mockRepository.save metod with correct data', async () => {
      friendsRepositoryMock.findOne.mockReturnValue({
        user1: mockUser,
        user2: mockUser2,
        pending: true
      });
      const spy = jest.spyOn(friendsRepositoryMock, 'save').mockReturnThis();

      await service.acceptFriendToUser(mockUser, mockUser2);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toHaveBeenCalledWith({
        user1: mockUser,
        user2: mockUser2,
        pending: false
      });
    });

    it('should throw na Error when save metod fails', async () => {
      friendsRepositoryMock.findOne.mockReturnValue({
        user1: mockUser,
        user2: mockUser2,
        pending: true
      });
      friendsRepositoryMock.save.mockImplementation(() => {
        throw new MySystemError(`TestErrorMessage`, 408);
      });
      let test: string;

      try {
        await service.acceptFriendToUser(mockUser, mockUser2);
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe(`TestErrorMessage`);
    });

    it('should throw an Error with generic message when save metod fails', async () => {
      friendsRepositoryMock.findOne.mockReturnValue({
        user1: mockUser,
        user2: mockUser2,
        pending: true
      });
      friendsRepositoryMock.save.mockImplementation(() => {
        throw new MySystemError();
      });
      let test: string;

      try {
        await service.acceptFriendToUser(mockUser, mockUser2);
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe(
        `Database took too much to respond, please try again later`
      );
    });
  });

  // @Transaction needs sandbox to mock, try sinon ot test db
  // describe('Test metod removeFriendFromUser()', () => {
  //   it('should call mockRepository.delete metod correct times with correct data', async () => {
  //     const spy = jest.spyOn(friendsRepositoryMock, 'delete');

  //     await service.removeFriendFromUser(mockUser, mockUser2, friendsRepositoryMock as any);

  //     expect(spy).toBeCalledTimes(2);
  //     expect(spy).toHaveBeenNthCalledWith(1, {
  //       userId_1: mockUser.id,
  //       userId_2: mockUser2.id
  //     });
  //     expect(spy).toHaveBeenNthCalledWith(2, {
  //       userId_1: mockUser2.id,
  //       userId_2: mockUser.id
  //     });
  //   });

  //   it('should throw an Error with passed message, when delete metod fails', async () => {
  //     friendsRepositoryMock.delete.mockReturnValueOnce('error');
  //     friendsRepositoryMock.delete.mockImplementation(() => {
  //       throw new MySystemError(`TestErrorMessage`, 408);
  //     });
  //     let test: string;

  //     try {
  //       await service.removeFriendFromUser(mockUser, mockUser2, friendsRepositoryMock as any);
  //     } catch (error) {
  //       test = error.message;
  //     }

  //     expect(test).toBe(`TestErrorMessage`);
  //   });

  //   it('should throw an Error with generic message when delete metod fails', async () => {
  //     friendsRepositoryMock.delete.mockReturnValueOnce('error');
  //     friendsRepositoryMock.delete.mockImplementation(() => {
  //       throw new MySystemError();
  //     });
  //     let test: string;

  //     try {
  //       await service.removeFriendFromUser(mockUser, mockUser2, friendsRepositoryMock as any);
  //     } catch (error) {
  //       test = error.message;
  //     }

  //     expect(test).toBe(
  //       `Database took too much to respond, please try again later`
  //     );
  //   });
  // });

  describe('Test metod toggleBanUser()', () => {
    it('should call mockRepository.findOne metod with correct data', async () => {
      const spy = jest
        .spyOn(userRepositoryMock, 'findOne')
        .mockReturnValue(mockUser);

      await service.toggleBanUser(2);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith({
        id: 2,
        isDeleted: false
      });
    });

    it('should call mockRepository.save metod with correct data', async () => {
      userRepositoryMock.findOne.mockReturnValue(mockUser);
      const spy = jest.spyOn(userRepositoryMock, 'save');

      await service.toggleBanUser(2);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(mockUser);
    });

    it('should throw na Error when findOne metod returns undefined', async () => {
      userRepositoryMock.findOne.mockReturnValue(undefined);
      let test: string;

      try {
        await service.toggleBanUser(2);
      } catch (error) {
        test = error.message;
      }

      expect(test).toBe(`User with such id 2 doesn\'t exists!`);
    });
  });

  describe('Test metod userActivity()', () => {
    it('should return correct data', async () => {
      jest
        .spyOn(friendsRepositoryMock, 'findOne')
        .mockImplementation(jest.fn().mockReturnThis())
        .mockReturnValue({ user1: mockUser, user2: mockUser2, pending: false });
      const mockResult = {
        userId: 2,
        articles: 1,
        comments: 2,
        upvoteArticles: 3,
        upvoteComments: 4,
        downvoteArticles: 5,
        downvoteComments: 6
      };
      const result = await service.userActivity(mockUser, mockUser2);

      expect(result).toEqual(mockResult);
    });

    it('should call mockRepository.findOne metod correct times with correct data', async () => {
      const spy = jest
        .spyOn(friendsRepositoryMock, 'findOne')
        .mockImplementation(jest.fn().mockReturnThis())
        .mockReturnValue({ user1: mockUser, user2: mockUser2, pending: false });

      await service.userActivity(mockUser, mockUser2);

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith({
        where: { userId_1: 2, userId_2: 3, pending: false }
      });
    });

    it('should call typeorm.getRepository metod correct times with correct data', async () => {
      jest
        .spyOn(friendsRepositoryMock, 'findOne')
        .mockImplementation(jest.fn().mockReturnThis())
        .mockReturnValue(mockUser);

      await service.userActivity(mockUser, mockUser2);

      expect(typeorm.getRepository).toHaveBeenCalledTimes(6);
      expect(typeorm.getRepository).toHaveBeenNthCalledWith(1, Article);
      expect(typeorm.getRepository).toHaveBeenNthCalledWith(2, Comment);
      expect(typeorm.getRepository).toHaveBeenNthCalledWith(3, ArticleVote);
      expect(typeorm.getRepository).toHaveBeenNthCalledWith(4, CommentVote);
      expect(typeorm.getRepository).toHaveBeenNthCalledWith(5, ArticleVote);
      expect(typeorm.getRepository).toHaveBeenNthCalledWith(6, CommentVote);
    });

    it('should throw an Error when friend is not found', async () => {
      jest
        .spyOn(friendsRepositoryMock, 'findOne')
        .mockImplementation(jest.fn().mockReturnThis())
        .mockReturnValue(undefined);
      let test: string;

      try {
        await service.userActivity(mockUser, mockUser2);
      } catch (err) {
        test = err.message;
      }

      expect(test).toBe(
        `This is not your friend or friendship is not accepted, so u can't see that user's activity!`
      );
    });

    it('should throw an Error with passed message when repository is unavailable', async () => {
      jest
        .spyOn(friendsRepositoryMock, 'findOne')
        .mockImplementation(jest.fn().mockReturnThis())
        .mockReturnValue(mockUser);
      typeorm.getRepository = jest.fn().mockReturnValue({
        findAndCount: () => {
          throw new MySystemError(`TestErrorMessage`, 408);
        }
      });

      let test: string;

      try {
        await service.userActivity(mockUser, mockUser2);
      } catch (err) {
        test = err.message;
      }

      expect(test).toBe(`TestErrorMessage`);
    });

    it('should throw an Error with generic message when none is passed, when repository is unavailable', async () => {
      jest
        .spyOn(friendsRepositoryMock, 'findOne')
        .mockImplementation(jest.fn().mockReturnThis())
        .mockReturnValue(mockUser);
      typeorm.getRepository = jest.fn().mockReturnValue({
        findAndCount: () => {
          throw new MySystemError();
        }
      });

      let test: string;

      try {
        await service.userActivity(mockUser, mockUser2);
      } catch (err) {
        test = err.message;
      }

      expect(test).toBe(
        `Database took too much to respond, please try again later`
      );
    });
  });
});
