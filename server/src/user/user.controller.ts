import {
  Controller,
  Body,
  Post,
  Get,
  Put,
  Delete,
  Param,
  Query,
  ParseIntPipe,
  HttpStatus,
  HttpCode,
  UseGuards,
  UseInterceptors
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { UserService } from './user.service';
import { UserUpdateDTO } from './../models/user/user-update-dto';
import { UserCreateDTO } from './../models/user/user-create-dto';
import { UserReturnDTO } from '../models/user/user-return-dto';
import { UserQueryDTO } from '../models/user/user-query-dto';
import { UpdateUserRolesDTO } from '../models/user/user-update-role-dto';
import { AdminGuard } from '../common/guards/admin.guard';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklist.guard';
import { UserDecorator } from '../common/decorators/user.decorator';
import { User } from '../data/entities/user.entity';
import { UserByIdPipe } from '../common/pipes/user-by-id.pipe';
import { UserActivityDTO } from '../models/user/user-activity-dto';
import { FriendsReturnDTO } from '../models/friends/friends-return-dto';

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  @UseGuards(AuthGuardWithBlacklisting, AdminGuard)
  async getAll(@Query() query: UserQueryDTO): Promise<UserReturnDTO[]> {
    return await this.userService.getAll(query);
  }

  @Get('/friends')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuardWithBlacklisting)
  public async getFriends(
    @UserDecorator() user: User
  ): Promise<FriendsReturnDTO[]> {
    return await this.userService.getFriends(user);
  }

  @Get(':id')
  @UseGuards(AuthGuardWithBlacklisting)
  async getUserById(@Param('id') id: number): Promise<UserReturnDTO> {
    return await this.userService.getUserById(id);
  }

  @Get('/:id/activity')
  @UseGuards(AuthGuardWithBlacklisting)
  async getUserActivity(
    @Param('id', UserByIdPipe) user: Promise<User>,
    @UserDecorator() requestUser: User
  ): Promise<UserActivityDTO> {
    return await this.userService.userActivity(await user, requestUser);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  async createUser(@Body() user: UserCreateDTO): Promise<UserReturnDTO> {
    return await this.userService.createUser(user);
  }

  @Put(':id')
  @UseGuards(AuthGuardWithBlacklisting, AdminGuard)
  async updateOther(
    @Param('id', ParseIntPipe) id: number,
    @Body() user: UserUpdateDTO
  ): Promise<UserReturnDTO> {
    return await this.userService.updateUser(id, user);
  }

  @Put()
  @UseGuards(AuthGuardWithBlacklisting)
  @HttpCode(HttpStatus.CREATED)
  @UseInterceptors(FileInterceptor('file'))
  async updateSelf(
    @Body() body: UserUpdateDTO,
    // @UploadedFile() file: any,
    @UserDecorator() user: User
  ): Promise<UserReturnDTO> {
    // if (file.filename) {
    //   body.avatarUrl = `http://${process.env.DB_HOST}:${process.env.PORT}/avatars/${file.filename}`;
    // }
    return await this.userService.updateUser(user.id, body);
  }

  @Delete(':id')
  @UseGuards(AuthGuardWithBlacklisting, AdminGuard)
  async deleteUser(
    @Param('id', ParseIntPipe) id: number
  ): Promise<{ message: string }> {
    await this.userService.deleteUser(id);

    return {
      message: `User was deleted!`
    };
  }

  @Put('/:id/roles')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuardWithBlacklisting, AdminGuard)
  public async updateUserRoles(
    @Param('id') userId: number,
    @Body() updateUserRoles: UpdateUserRolesDTO
  ): Promise<UserReturnDTO> {
    return await this.userService.updateUserRoles(userId, updateUserRoles);
  }

  @Delete('/:id/ban')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuardWithBlacklisting, AdminGuard)
  public async toggleBanUser(
    @Param('id') userId: number
  ): Promise<{ message: string }> {
    await this.userService.toggleBanUser(userId);

    return {
      message: `BANNED`
    };
  }

  @Post('/:id/friends')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuardWithBlacklisting)
  public async addFriendToUser(
    @Param('id', UserByIdPipe) friendToAdd: Promise<User>,
    @UserDecorator() user: User
  ): Promise<{ message: string }> {
    await this.userService.addFriendToUser(user, await friendToAdd);

    return {
      message: `Friendship requested!`
    };
  }

  @Put('/:id/friends')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuardWithBlacklisting)
  public async acceptFriendToUser(
    @Param('id', UserByIdPipe) friendToAdd: Promise<User>,
    @UserDecorator() user: User
  ): Promise<{ message: string }> {
    await this.userService.acceptFriendToUser(user, await friendToAdd);

    return {
      message: `Friendship created!`
    };
  }

  @Delete('/:id/friends')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuardWithBlacklisting)
  public async removeFriendFromUser(
    @Param('id', UserByIdPipe) friendToDelete: Promise<User>,
    @UserDecorator() user: User
  ): Promise<{ message: string }> {
    await this.userService.removeFriendFromUser(user, await friendToDelete);

    return {
      message: `Friendship is over!`
    };
  }
}
