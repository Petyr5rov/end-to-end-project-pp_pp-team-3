import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../data/entities/user.entity';
import { Role } from 'src/data/entities/role.entity';
import { MulterModule } from '@nestjs/platform-express';
import { MySystemError } from '../common/exceptions/my-system.error';
import { extname } from 'path';
import { diskStorage } from 'multer';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Friends } from '../data/entities/friends.entity';


@Module({
      imports: [
        TypeOrmModule.forFeature([User, Role, Friends]),
        MulterModule.register({
          fileFilter(_, file, cb) {
            const ext = extname(file.originalname);
            const allowedExtensions = ['.png', '.jpg', '.gif', '.jpeg'];

            if (!allowedExtensions.includes(ext)) {
              return cb(new MySystemError('Only images are allowed', 400), false);
            }

            cb(null, true);
          },
          storage: diskStorage({
            destination: './avatars',
            filename: (_, file, cb) => {
              const randomName = Array.from({ length: 32 })
                .map(() => Math.round(Math.random() * 10))
                .join('');

              return cb(null, `${randomName}${extname(file.originalname)}`);
            },
          }),
        }),
        ConfigModule,
      ],
  controllers: [UserController],
  providers: [UserService, ConfigService],
  exports: [UserService],
})
export class UserModule {}
