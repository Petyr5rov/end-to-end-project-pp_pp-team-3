import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { UserQueryDTO } from '../models/user/user-query-dto';
import { UserCreateDTO } from '../models/user/user-create-dto';
import { UserUpdateDTO } from '../models/user/user-update-dto';
import { User } from '../data/entities/user.entity';
import { UpdateUserRolesDTO } from '../models/user/user-update-role-dto';

describe('User Controller', () => {
  let controller: UserController;
  const userService: Partial<UserService> = {
    getAll() {
      return null;
    },
    getUserById() {
      return null;
    },
    getFriends() {
      return null;
    },
    createUser() {
      return null;
    },
    updateUser() {
      return null;
    },
    deleteUser() {
      return null;
    },
    toggleBanUser() {
      return null;
    },
    updateUserRoles() {
      return null;
    },
    addFriendToUser() {
      return null;
    },
    acceptFriendToUser() {
      return null;
    },
    removeFriendFromUser() {
      return null;
    },
    userActivity() {
      return null;
    }
  };
  const mockQuery = new UserQueryDTO();
  const mockCreate = new UserCreateDTO();
  const mockUserUpdate = new UserUpdateDTO();
  const mockUser = { id: 3 } as User;
  const mockUserUpdateRoles = new UpdateUserRolesDTO();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [
        {
          provide: UserService,
          useValue: userService
        }
      ]
    }).compile();

    controller = module.get<UserController>(UserController);

    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('Test metod getAll()', () => {
    it('should return correct result', async () => {
      jest
        .spyOn(userService, 'getAll')
        .mockImplementation(() => Promise.resolve('test') as any);
      const result = await controller.getAll(mockQuery);

      expect(result).toBe('test');
    });

    it('should call getAll() metod on userService correctly ', async () => {
      const spy = jest.spyOn(userService, 'getAll');
      await controller.getAll(mockQuery);
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(mockQuery);
    });
  });

  describe('Test metod getUserById()', () => {
    it('should return correct result', async () => {
      jest
        .spyOn(userService, 'getUserById')
        .mockImplementation(() => Promise.resolve('test') as any);
      const result = await controller.getUserById(3);

      expect(result).toBe('test');
    });

    it('should call getUserById() method on userService', async () => {
      const spy = jest.spyOn(userService, 'getUserById');
      await controller.getUserById(3);
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(3);
    });
  });

  describe('Test metod getUserActivity()', () => {
    it('should return correct result', async () => {
      jest
        .spyOn(userService, 'userActivity')
        .mockImplementation(() => Promise.resolve('test') as any);
      const result = await controller.getUserActivity(
        Promise.resolve(mockUser),
        mockUser
      );

      expect(result).toBe('test');
    });

    it('should call userActivity() method on userService', async () => {
      const spy = jest.spyOn(userService, 'userActivity');
      await controller.getUserActivity(Promise.resolve(mockUser), mockUser);
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(mockUser, mockUser);
    });
  });

  describe('Test metod getFriends()', () => {
    it('should return correct result', async () => {
      jest
        .spyOn(userService, 'getFriends')
        .mockImplementation(() => Promise.resolve('test') as any);

      const result = await controller.getFriends(mockUser);

      expect(result).toBe('test');
    });

    it('should call getFriends() method on userService', async () => {
      const spy = jest.spyOn(userService, 'getFriends');
      await controller.getFriends(mockUser);
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(mockUser);
    });
  });

  describe('Test metod createUser()', () => {
    it('should return correct result', async () => {
      jest
        .spyOn(userService, 'createUser')
        .mockImplementation(() => Promise.resolve('test') as any);
      const result = await controller.createUser(mockCreate);

      expect(result).toBe('test');
    });

    it('should call createUser() method on userService', async () => {
      const spy = jest.spyOn(userService, 'createUser');
      await controller.createUser(mockCreate);
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(mockCreate);
    });
  });

  describe('Test metod updateOther()', () => {
    it('should return correct result', async () => {
      jest
        .spyOn(userService, 'updateUser')
        .mockImplementation(() => Promise.resolve('test') as any);
      const result = await controller.updateOther(2, mockUserUpdate);

      expect(result).toBe('test');
    });

    it('should call updateUser() method on userService', async () => {
      const spy = jest.spyOn(userService, 'updateUser');
      await controller.updateOther(2, mockUserUpdate);
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(2, mockUserUpdate);
    });
  });

  describe('Test metod updateSelf()', () => {
    it('should return correct result', async () => {
      jest
        .spyOn(userService, 'updateUser')
        .mockImplementation(() => Promise.resolve('test') as any);
      const result = await controller.updateSelf(mockUserUpdate, mockUser);

      expect(result).toBe('test');
    });

    it('should call updateUser() method on userService', async () => {
      const spy = jest.spyOn(userService, 'updateUser');
      await controller.updateSelf(mockUserUpdate, mockUser);
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(3, mockUserUpdate);
    });

    // it('should transform mockUserUpdate with correct data', async () => {
    //   const spy = jest.spyOn(userService, 'updateUser');
    //   await controller.updateSelf(3, mockUserUpdate );

    //   expect(spy).toHaveBeenCalledWith(3, {
    //     avatarUrl: 'http://undefined:undefined/avatars/testFieName'
    //   } as UserUpdateDTO);
    // });
  });

  describe('Test metod deleteUser()', () => {
    it('should return correct result', async () => {
      const result = await controller.deleteUser(7);

      expect(result).toEqual({ message: `User was deleted!` });
    });

    it('should call deleteUser() method on userService', async () => {
      const spy = jest.spyOn(userService, 'deleteUser');
      await controller.deleteUser(7);

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(7);
    });
  });

  describe('Test metod updateUserRoles()', () => {
    it('should return correct result', async () => {
      jest
        .spyOn(userService, 'updateUserRoles')
        .mockImplementation(() => Promise.resolve('test') as any);
      const result = await controller.updateUserRoles(3, mockUserUpdateRoles);

      expect(result).toBe('test');
    });

    it('should call updateUserRoles() method on userService', async () => {
      const spy = jest.spyOn(userService, 'updateUserRoles');
      await controller.updateUserRoles(3, mockUserUpdateRoles);

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(3, mockUserUpdateRoles);
    });
  });

  describe('Test metod toggleBanUser()', () => {
    it('should return correct result', async () => {
      const result = await controller.toggleBanUser(3);

      expect(result).toEqual({
        message: `BANNED`
      });
    });

    it('should call toggleBanUser() method on userService', async () => {
      const spy = jest.spyOn(userService, 'toggleBanUser');
      await controller.toggleBanUser(3);

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(3);
    });
  });

  describe('Test metod addFriendToUser()', () => {
    it('should return correct result', async () => {
      const result = await controller.addFriendToUser(
        Promise.resolve(mockUser),
        mockUser
      );

      expect(result).toEqual({ message: `Friendship requested!` });
    });

    it('should call addFriendToUser() method on userService', async () => {
      const spy = jest.spyOn(userService, 'addFriendToUser');
      await controller.addFriendToUser(Promise.resolve(mockUser), mockUser);

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(mockUser, mockUser);
    });
  });

  describe('Test metod acceptFriendToUser()', () => {
    it('should return correct result', async () => {
      const result = await controller.acceptFriendToUser(
        Promise.resolve(mockUser),
        mockUser
      );

      expect(result).toEqual({ message: `Friendship created!` });
    });

    it('should call addFriendToUser() method on userService', async () => {
      const spy = jest.spyOn(userService, 'acceptFriendToUser');
      await controller.acceptFriendToUser(Promise.resolve(mockUser), mockUser);

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(mockUser, mockUser);
    });
  });

  describe('Test metod removeFriendFromUser()', () => {
    it('should return correct result', async () => {
      const result = await controller.removeFriendFromUser(
        Promise.resolve(mockUser),
        mockUser
      );

      expect(result).toEqual({ message: `Friendship is over!` });
    });

    it('should call removeFriendFromUser() method on userService', async () => {
      const spy = jest.spyOn(userService, 'removeFriendFromUser');
      await controller.removeFriendFromUser(
        Promise.resolve(mockUser),
        mockUser
      );

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(mockUser, mockUser);
    });
  });
});
