import { UserReturnDTO } from '../user/user-return-dto';

export interface FriendsReturnDTO {
  user2: UserReturnDTO;
  pending: boolean;
}
