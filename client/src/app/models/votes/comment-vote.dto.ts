import { UserReturnDTO } from '../user/user-return-dto';

export interface CommentVoteDTO {
  positive: boolean;
  user: UserReturnDTO;
}
