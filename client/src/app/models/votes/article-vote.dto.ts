import { UserReturnDTO } from '../user/user-return-dto';

export class ArticleVoteDTO {
  positive: boolean;
  user: UserReturnDTO;
}
