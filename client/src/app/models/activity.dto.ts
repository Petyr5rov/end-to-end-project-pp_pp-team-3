export class ActivityDTO{
  userId: number;
  articles: number;
  comments: number;
  upvoteArticles: number;
  upvoteComments: number;
  downvoteArticles: number;
  downvoteComments: number;
}
