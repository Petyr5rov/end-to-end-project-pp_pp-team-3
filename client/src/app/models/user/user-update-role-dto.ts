import { UserRole } from './user-role';


export interface UpdateUserRolesDTO {
  role: UserRole;
}
