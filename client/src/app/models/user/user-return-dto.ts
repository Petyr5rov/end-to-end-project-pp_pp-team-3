export interface UserReturnDTO {
  id: number;
  username: string;
  avatarUrl: string;
  role: string;
  isBanned: boolean;
  isDeleted: boolean;
}
