

export interface UserQueryDTO {
  username: string;
  email: string;
}
