export interface UserUpdateDTO {
  username?: string;
  avatarUrl?: string;
}
