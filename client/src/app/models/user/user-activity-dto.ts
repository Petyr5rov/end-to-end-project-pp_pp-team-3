export interface UserActivityDTO {
  userId: number;
  articles: number;
  comments: number;
  upvoteArticles: number;
  downvoteArticles: number;
  upvoteComments: number;
  downvoteComments: number;
}
