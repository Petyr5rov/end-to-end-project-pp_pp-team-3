export interface CommentCreatedDTO {
  content: string;
  articleId: number;
}
