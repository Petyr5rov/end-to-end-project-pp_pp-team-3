import { UserReturnDTO } from '../user/user-return-dto';

export interface CommentWithFlagShowDTO {
  id: number;
  content: string;
  createTime: string;
  updateTime: string;
  isFlagged: boolean;
  user: UserReturnDTO;
}
