export interface CommentPutQueryDTO {
  vote: string;
  flag: string;
}
