import { UserReturnDTO } from '../user/user-return-dto';
import { CommentVoteDTO } from '../votes/comment-vote.dto';

export interface CommentShowDTO {
  id: number;
  content: string;
  createTime: string;
  updateTime: string;
  user: UserReturnDTO;
  votes: CommentVoteDTO[];
}
