import { UserReturnDTO } from '../user/user-return-dto';
import { ArticleVoteDTO } from '../votes/article-vote.dto';
import { CommentShowDTO } from '../comment/comment-show-dto';

export interface ArticleShowDTO {
  id: number;
  title: string;
  content: string;
  createTime: string;
  updateTime: string;
  isLocked: boolean;
  user: UserReturnDTO;
  comments: CommentShowDTO[];
  votes: ArticleVoteDTO[];
}
