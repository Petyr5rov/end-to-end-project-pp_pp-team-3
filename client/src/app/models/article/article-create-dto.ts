export interface ArticleCreatedDTO {
  title: string;
  content: string;
}
