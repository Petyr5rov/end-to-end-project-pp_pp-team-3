export interface ArticleGetAllQueryDTO {
  items: number;
  page: number;
  searchContent: string;
  searchTitle: string;
  userId: number;
}
