export interface ArticlePutQueryDTO {
  vote?: string;
  flag?: string;
}
