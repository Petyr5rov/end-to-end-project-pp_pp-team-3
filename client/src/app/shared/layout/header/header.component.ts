import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserReturnDTO } from '../../../models/user/user-return-dto';
import { Router } from '@angular/router';
import { ThemeService } from '../../theme/theme.service';
import { ArticlesService } from '../../../core/services/articles.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  @Input() loggedIn: boolean;
  @Input() user: UserReturnDTO;
  @Output() logout = new EventEmitter<void>();
  constructor(
    private readonly articleService: ArticlesService,
    private readonly router: Router,
    private themeService: ThemeService
  ) {}

  public theme = false;
  public inTitle = '';
  public searchString = '';

  public search() {
    if (!this.searchString || this.searchString.length < 3) {
      this.router.navigate(['not-found']);
    } else {
      const appendParam = !!this.inTitle
        ? { searchTitle: this.searchString }
        : { searchContent: this.searchString };
      this.router.navigate(['/home'], {
        queryParams: appendParam,
      });
      this.searchString = '';
    }
  }

  public goHome(): void {
    this.articleService.all();
    this.router.navigate(['/home']);
  }

  public create(): void {
    this.articleService.all();
    this.router.navigate(['/home'], {
      queryParams: { create: 'true' },
    });
  }

  ngOnInit(): void {}

  public triggerLogout() {
    this.logout.emit();
  }

  public gotoLogin() {
    this.router.navigate(['login'], {
      queryParams: { returnUrl: this.router.routerState.snapshot.url },
    });
  }

  public gotoRegister() {
    this.router.navigate(['register'], {
      queryParams: { returnUrl: this.router.routerState.snapshot.url },
    });
  }

  public friends() {
    this.router.navigate(['/users', this.user.id, 'friends']);
  }

  public profilsAll() {
    this.router.navigate(['/users', 'allUsers']);
  }

  public getFlagged() {
    this.router.navigate(['/home'], {
      queryParams: { getFlagged: 'true' },
    });
  }

  public edit() {
    this.router.navigate(['/users', this.user.id, 'edit'], {
      queryParams: { returnUrl: this.router.routerState.snapshot.url },
    });
  }

  toggle() {
    const active = this.themeService.getActiveTheme();
    if (active.name === 'light') {
      this.themeService.setTheme('dark');
      this.theme = !this.theme;
    } else {
      this.themeService.setTheme('light');
      this.theme = !this.theme;
    }
  }
}
