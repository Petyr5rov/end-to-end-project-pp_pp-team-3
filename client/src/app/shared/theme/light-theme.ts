import { Theme } from './symbols';

export const lightTheme: Theme = {
  name: 'light',
  properties: {
    '--background': '#f6f7f9',
    '--on-background': '#646464',
    '--primary': '#f6f7f9',
    '--on-primary': '#646464'
  }
};
