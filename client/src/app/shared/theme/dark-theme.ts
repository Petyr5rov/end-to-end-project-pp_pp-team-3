import { Theme } from './symbols';

export const darkTheme: Theme = {
  name: 'dark',
  properties: {
    '--background': '#5a5a5a',
    '--on-background': '#C8C8C8',
    '--primary': '4a4a4a',
    '--on-primary': '#C8C8C8',
  }
}
