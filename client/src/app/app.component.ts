import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserReturnDTO } from './models/user/user-return-dto';
import { AuthService } from './core/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {
  private loggedInSubscription: Subscription;
  private userSubscription: Subscription;

  public loggedIn: boolean;
  public user: UserReturnDTO;

  constructor(
    private readonly authService: AuthService,
    ) {}

  ngOnInit() {
    this.loggedInSubscription = this.authService.isLoggedIn$.subscribe(
      (loggedIn) => (this.loggedIn = loggedIn)
    );
    this.userSubscription = this.authService.loggedUser$.subscribe(
      (user) => (this.user = user)
    );

  }

  ngOnDestroy() {
    this.loggedInSubscription.unsubscribe();
    this.userSubscription.unsubscribe();
  }

  public logout() {
    this.authService.logout();
  }
}
