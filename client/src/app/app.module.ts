import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { ThemeModule } from './shared/theme/theme.module';
import { lightTheme } from './shared/theme/light-theme';
import { darkTheme } from './shared/theme/dark-theme';
import { CoreModule } from './core/core.module';
import { TokenInterceptorService } from './core/interceptors/token-interceptor.service';
import { SpinnerIntercerptorService } from './core/interceptors/spinner-intercerptor.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserModule } from './components/user/user.module';
import { ArticlesModule } from './components/articles/articles.module';
import { ArticleCommentsModule } from './components/article-comments/article-comments.module';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
  ],
  imports: [
    CoreModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    ArticlesModule,
    ArticleCommentsModule,
    UserModule,
    ThemeModule.forRoot({
      themes: [lightTheme, darkTheme],
      active: 'light',
    }),
  ],

  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SpinnerIntercerptorService,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
