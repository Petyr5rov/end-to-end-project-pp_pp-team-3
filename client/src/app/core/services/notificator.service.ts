import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class NotificatorService {

  constructor(
    private readonly toastr: ToastrService,
  ) { }

  public success(message: string) {
    this.toastr.success(message);
  }

  public warn(message: string) {
    this.toastr.warning(message);
  }

  public error(message: string) {
    this.toastr.error(message);
  }

}
