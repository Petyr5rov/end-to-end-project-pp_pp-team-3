import { Router } from '@angular/router';
import { StorageService } from './storage.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserLoginDTO } from '../../models/user/user-login-dto';
import { UserReturnDTO } from '../../models/user/user-return-dto';
import { UserRegisterDTO } from '../../models/user/user-register-dto';


@Injectable()
export class AuthService {
  private readonly helper = new JwtHelperService();
  private readonly isLoggedInSubject$ = new BehaviorSubject<boolean>(this.isUserLoggedIn());
  private readonly loggedUserSubject$ = new BehaviorSubject<UserReturnDTO>(this.loggedUser());

  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
    private readonly router: Router,
  ) { }

  public get isLoggedIn$(): Observable<boolean> {
    return this.isLoggedInSubject$.asObservable();
  }

  public get loggedUser$(): Observable<UserReturnDTO> {
    return this.loggedUserSubject$.asObservable();
  }

  public login(user: UserLoginDTO) {
    return this.http.post<{ token: string }>(`http://localhost:3000/session`, user)
      .pipe(
        tap(({ token }) => {
          try {
            const loggedUser = this.helper.decodeToken(token);
            this.storage.save('token', token);

            this.isLoggedInSubject$.next(true);
            this.loggedUserSubject$.next(loggedUser);
          } catch (error) {
            // error handling on the consumer side
          }
        }),
      );
  }

  public logout() {
    this.storage.save('token', '');
    this.isLoggedInSubject$.next(false);
    this.loggedUserSubject$.next(null);

    this.router.navigate(['home']);
  }

  public register(user: UserRegisterDTO) {
    return this.http.post(`http://localhost:3000/users`, user);
  }

  private isUserLoggedIn(): boolean {
    return !!this.storage.read('token');
  }

  private loggedUser(): UserReturnDTO {
    try {
      return this.helper.decodeToken(this.storage.read('token'));
    } catch (error) {
      // in case of storage tampering
      this.isLoggedInSubject$.next(false);

      return null;
    }
  }

}
