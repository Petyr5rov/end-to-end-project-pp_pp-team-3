import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserReturnDTO } from '../../models/user/user-return-dto';
import { ActivityDTO } from '../../models/activity.dto';
import { FriendsReturnDTO } from '../../models/friends/friends-return-dto';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private readonly http: HttpClient) {}

  public friends: UserReturnDTO;

  loadFriends(): Observable<FriendsReturnDTO[]> {
    return this.http.get<FriendsReturnDTO[]>(
      `http://localhost:3000/users/friends`
    );
  }

  getAll(): Observable<UserReturnDTO[]> {
    return this.http.get<UserReturnDTO[]>(`http://localhost:3000/users`);
  }

  getUserById(userId: number): Observable<UserReturnDTO> {
    return this.http.get<UserReturnDTO>(
      `http://localhost:3000/users/${userId}`
    );
  }

  edit(userForm: any): Observable<UserReturnDTO> {
    return this.http.put<any>(`http://localhost:3000/users`, userForm);
  }

  delateUser(id: number) {
    return this.http.delete<{ msg: string }>(
      `http://localhost:3000/users/${id}`
    );
  }

  banUser(id: number) {
    return this.http.delete<{ msg: string }>(
      `http://localhost:3000/users/${id}/ban`
    );
  }
  editUserRole(role: string, id: number): Observable<UserReturnDTO> {
    return this.http.put<any>(`http://localhost:3000/users/${id}/roles`, {
      role,
    });
  }
  getUserActivity(id: number): Observable<ActivityDTO> {
    return this.http.get<ActivityDTO>(
      `http://localhost:3000/users/${id}/activity`
    );
  }

  deleteFriend(id: number) {
    return this.http.delete<{ msg: string }>(
      `http://localhost:3000/users/${id}/friends`
    );
  }

  addFriend(id: number) {
    return this.http.post<{ msg: string }>(
      `http://localhost:3000/users/${id}/friends`,
      {}
    );
  }

  acceptFriend(id: number) {
    return this.http.put<{ msg: string }>(
      `http://localhost:3000/users/${id}/friends`,
      {}
    );
  }
}
