import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { ArticleShowDTO } from '../../models/article/article-show-dto';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class ArticlesService {
  constructor(
    private readonly http: HttpClient,
    private readonly router: Router
  ) {}

  private articlesStream$: Subject<[ArticleShowDTO[], number]> = new Subject();
  private singleArticleStream$: Subject<ArticleShowDTO> = new Subject();

  public get articlesSource$(): Observable<[ArticleShowDTO[], number]> {
    return this.articlesStream$.asObservable();
  }

  public get singleArticleSource$(): Observable<ArticleShowDTO> {
    return this.singleArticleStream$.asObservable();
  }

  private readonly serverAddress: string = `http://localhost:3000/`;

  public all(queryParams?: string): void {
    this.http
      .get<[ArticleShowDTO[], number]>(
        `${this.serverAddress}articles${queryParams ? `?${queryParams}` : ''}`
      )
      .subscribe(
        (data) => {
          data[1] === 0
            ? this.router.navigate(['not-found'])
            : this.articlesStream$.next(data);
        },
        (error) =>
          this.router.navigate(['server-error'], {
            queryParams: {
              errorMsg: error.message,
              errorCode: error.status,
            },
          })
      );
  }

  public getById(id: number): void {
    this.http
      .get<ArticleShowDTO>(`${this.serverAddress}articles/${id}`)
      .subscribe(
        (data) => this.singleArticleStream$.next(data),
        (error) =>
          this.router.navigate(['server-error'], {
            queryParams: {
              errorMsg: error.message,
              errorCode: error.status,
            },
          })
      );
  }

  public getFlagged(): void {
    this.http
      .get<[ArticleShowDTO[], number]>(`${this.serverAddress}articles/flagged`)
      .subscribe(
        (data) => {
          data[1] === 0
            ? this.router.navigate(['not-found'])
            : this.articlesStream$.next(data);
        },
        (error) =>
          this.router.navigate(['server-error'], {
            queryParams: {
              errorMsg: error.message,
              errorCode: error.status,
            },
          })
      );
  }

  public create(
    type: string,
    content: string,
    title?: string,
    articleId?: number
  ): Observable<ArticleShowDTO> {
    const body = title
      ? { title: title, content: content }
      : { content: content, articleId };
    return this.http.post<ArticleShowDTO>(`${this.serverAddress}${type}`, body);
  }

  public editContent(
    type: string,
    id: number,
    content: string
  ): Observable<ArticleShowDTO> {
    return this.http.put<ArticleShowDTO>(
      `${this.serverAddress}${type}/${id}/editContent`,
      { content: content }
    );
  }

  public editContentByAdmin(
    type: string,
    id: number,
    content: string
  ): Observable<ArticleShowDTO> {
    return this.http.put<ArticleShowDTO>(
      `${this.serverAddress}${type}/${id}/editContentByAdmin`,
      { content: content }
    );
  }

  public upVote(type: string, id: number): Observable<ArticleShowDTO> {
    return this.http.put<ArticleShowDTO>(
      `${this.serverAddress}${type}/${id}?vote=true`,
      {}
    );
  }

  public downVote(type: string, id: number): Observable<ArticleShowDTO> {
    return this.http.put<ArticleShowDTO>(
      `${this.serverAddress}${type}/${id}?vote=false`,
      {}
    );
  }

  public flag(type: string, id: number): Observable<ArticleShowDTO> {
    return this.http.put<ArticleShowDTO>(
      `${this.serverAddress}${type}/${id}?flag=true`,
      {}
    );
  }

  public toggleFlagByAdmin(
    type: string,
    id: number
  ): Observable<ArticleShowDTO> {
    return this.http.put<ArticleShowDTO>(
      `${this.serverAddress}${type}/${id}/flagByAdmin`,
      {}
    );
  }

  public delete(type: string, id: number): Observable<ArticleShowDTO> {
    return this.http.delete<ArticleShowDTO>(
      `${this.serverAddress}${type}/${id}`,
      {}
    );
  }

  public lock(type: string, id: number): Observable<ArticleShowDTO> {
    return this.http.put<ArticleShowDTO>(
      `${this.serverAddress}${type}/${id}/lockByAdmin`,
      {}
    );
  }

  public commentRemoveFlagByAdmin(id: number): Observable<ArticleShowDTO> {
    return this.http.put<ArticleShowDTO>(
      `${this.serverAddress}comments/${id}/removeFlagByAdmin`,
      {}
    );
  }
}
