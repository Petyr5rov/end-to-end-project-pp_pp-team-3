import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserRole } from '../../models/user/user-role';
import { NotificatorService } from '../services/notificator.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { UserReturnDTO } from '../../models/user/user-return-dto';

@Injectable({
    providedIn: 'root'
})
export class AdminGuard implements CanActivate {
    constructor(
        private readonly authService: AuthService,
        private readonly router: Router,
        private readonly notificator: NotificatorService,
    ) { }

    canActivate(): Observable<boolean> {
        return this.authService.loggedUser$
            .pipe(
                map((user: UserReturnDTO) => {
                    if (user.role !== UserRole.ADMIN) {
                        this.notificator.error(`You can't go on that page!`);
                        this.router.navigate(['/']);
                        return false;
                    } else {
                        return true;
                    }
                })
            );
    }
}
