import { StorageService } from './services/storage.service';
import { NotificatorService } from './services/notificator.service';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToastrModule } from 'ngx-toastr';
import { AuthService } from './services/auth.service';
import { SharedModule } from '../shared/shared.module';
import { AuthGuard } from './guard/auth.guard';
import { ArticlesService } from './services/articles.service';
import { ArticleCommentsService } from './services/article-comments.service';
import { ArticlesModule } from '../components/articles/articles.module';
import { UserModule } from '../components/user/user.module';
import { ArticleCommentsModule } from '../components/article-comments/article-comments.module';
import { UserService } from './services/user.service';


@NgModule({
  providers: [
    ArticlesService,
    StorageService,
    AuthService,
    AuthGuard,
    NotificatorService,
    UserService,
    ArticleCommentsService,
  ],
  declarations: [],
  imports: [
    ArticlesModule,
    UserModule,
    ArticleCommentsModule,
    CommonModule,
    HttpClientModule,
    SharedModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
      countDuplicates: true,
    }),
  ],
  exports: [
    HttpClientModule,
  ],
})
export class CoreModule {

  constructor(@Optional() @SkipSelf() parent: CoreModule) {
    if (parent) {
      throw new Error(`CoreModule has already been initialized!`);
    }
  }

}
