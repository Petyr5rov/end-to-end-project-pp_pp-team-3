import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { CommentShowDTO } from '../../../models/comment/comment-show-dto';
import { UserReturnDTO } from '../../../models/user/user-return-dto';

@Component({
  selector: 'app-single-comment',
  templateUrl: './single-comment.component.html',
  styleUrls: ['./single-comment.component.scss'],
})
export class SingleCommentComponent implements OnInit {
  @Input()
  public comment: CommentShowDTO;

  @Input()
  public loggedUser: UserReturnDTO;

  @Output()
  public editContent = new EventEmitter<[number, string]>();

  @Output()
  public editContentByAdmin = new EventEmitter<[number, string]>();

  @Output()
  public upVote = new EventEmitter<number>();

  @Output()
  public downVote = new EventEmitter<number>();

  @Output()
  public flag = new EventEmitter<number>();

  @Output()
  public removeFlagByAdmin = new EventEmitter<number>();

  @Output()
  public delete = new EventEmitter<number>();

  public votes = 0;
  public isFlagged = false;
  public contentToEdit: string;
  public editMode = false;
  public editAdmin = false;

  ngOnInit() {
    this.votes =
      this.comment.votes.filter((vote) => vote.positive === true).length -
      this.comment.votes.filter((vote) => vote.positive === false).length;
    if (
      this.comment.content ===
      `This Comment is flagged, please edit it's content or it will be deleted!`
    ) {
      this.isFlagged = true;
    }
    this.contentToEdit = this.comment.content;
  }

  emitUpVote(): void {
    this.upVote.emit(this.comment.id);
  }

  emitDownVote(): void {
    this.downVote.emit(this.comment.id);
  }

  emitFlag(): void {
    this.flag.emit(this.comment.id);
  }

  emitEdit() {
    this.editMode = false;
    if (this.contentToEdit.charCodeAt(0) === 10) {
      this.contentToEdit = '';
    }
    this.contentToEdit.trim();
    this.comment.content = this.contentToEdit;
    this.editContent.emit([this.comment.id, this.contentToEdit]);
  }

  emitEditByAdmin() {
    this.editAdmin = false;
    if (this.contentToEdit.charCodeAt(0) === 10) {
      this.contentToEdit = '';
    }
    this.contentToEdit.trim();
    this.comment.content = this.contentToEdit;
    this.editContentByAdmin.emit([this.comment.id, this.contentToEdit]);
  }

  emitRemoveFlagByAdmin(): void {
    this.removeFlagByAdmin.emit(this.comment.id);
  }

  emitDelete(): void {
    this.delete.emit(this.comment.id);
  }

  editModeToggler() {
    this.editMode = !this.editMode;
  }

  editAdminToggler() {
    this.editAdmin = !this.editAdmin;
  }
}
