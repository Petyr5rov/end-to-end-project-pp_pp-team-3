import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SingleCommentComponent } from './single-comment/single-comment.component';
import { ArticleCommentsComponent } from './article-comments.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { MaterialModule } from '../../shared/material/material.module';
import { ArticleCommentsService } from '../../core/services/article-comments.service';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [SingleCommentComponent, ArticleCommentsComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    SharedModule,
    MaterialModule,
    RouterModule,
    FormsModule,
  ],
  providers: [ArticleCommentsService],
  exports: [ArticleCommentsComponent],
})
export class ArticleCommentsModule {}
