import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArticlesService } from '../../core/services/articles.service';
import { ArticleShowDTO } from '../../models/article/article-show-dto';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from '../../core/services/auth.service';
import { UserReturnDTO } from '../../models/user/user-return-dto';
import { CommentShowDTO } from '../../models/comment/comment-show-dto';
import { NotificatorService } from '../../core/services/notificator.service';

@Component({
  selector: 'app-article-comments',
  templateUrl: './article-comments.component.html',
  styleUrls: ['./article-comments.component.scss'],
})
export class ArticleCommentsComponent implements OnInit, OnDestroy {
  public singleArticle: ArticleShowDTO;
  public loggedUser: UserReturnDTO;

  content: string;
  constructor(
    private activatedRoute: ActivatedRoute,
    private articlesService: ArticlesService,
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly notificator: NotificatorService
  ) {}
  private loggedUserSubscription: Subscription;
  private singleArticleSourceSubscription: Subscription;

  public votes: number;
  public commentsCount: number;
  public pageSize = 10;
  public pageSizeOptions: number[] = [5, 10, 25];
  public showComments: CommentShowDTO[];
  public contentToEdit: string;
  public editMode = false;
  public editAdmin = false;
  public commentMode = false;
  public isFlagged = false;
  public newComment = '';

  ngOnInit(): void {
    this.loggedUserSubscription = this.authService.loggedUser$.subscribe(
      (data) => (this.loggedUser = data)
    );
    const articleId = this.activatedRoute.snapshot.params.id;
    this.articlesService.getById(articleId);
    this.singleArticleSourceSubscription = this.articlesService.singleArticleSource$.subscribe(
      (article) => {
        this.singleArticle = article;
        this.votes =
          this.singleArticle.votes.filter((vote) => vote.positive === true)
            .length -
          this.singleArticle.votes.filter((vote) => vote.positive === false)
            .length;
        this.commentsCount = article.comments.length;
        this.showComments = article.comments.slice(0, 9);
        this.contentToEdit = this.singleArticle.content;
        if (
          this.singleArticle.content ===
          `This Article is flagged, please edit it's content or it will be deleted!`
        ) {
          this.isFlagged = true;
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
    this.singleArticleSourceSubscription.unsubscribe();
  }

  pageEvent(event: any): void {
    if (event.pageIndex === 0) {
      this.showComments = this.singleArticle.comments.slice(
        0,
        event.pageSize - 1
      );
    } else {
      this.showComments = this.singleArticle.comments.slice(
        event.pageSize * event.pageIndex,
        event.pageSize * (event.pageIndex + 1) - 1
      );
    }
  }

  postComment(): void {
    if (!this.newComment) {
      this.newComment = 'Please write something here!';
      this.notificator.warn('Please write something!');
    } else {
      this.commentMode = false;
      this.subscribeHandler(
        this.articlesService.create(
          'comments',
          this.newComment,
          undefined,
          this.singleArticle.id
        )
      );
      this.newComment = '';
    }
  }

  onEditContent(): void {
    if (this.contentToEdit.charCodeAt(0) === 10) {
      this.contentToEdit = '';
    }
    this.contentToEdit.trim();
    this.editMode = false;
    this.singleArticle.content = this.contentToEdit;
    this.subscribeHandler(
      this.articlesService.editContent(
        'articles',
        this.singleArticle.id,
        this.contentToEdit
      )
    );
  }

  onEditContentByAdmin(): void {
    this.editAdmin = false;
    if (this.contentToEdit.charCodeAt(0) === 10) {
      this.contentToEdit = '';
    }
    this.contentToEdit.trim();
    this.singleArticle.content = this.contentToEdit;
    this.subscribeHandler(
      this.articlesService.editContentByAdmin(
        'articles',
        this.singleArticle.id,
        this.contentToEdit
      )
    );
  }

  onUpVote(): void {
    if (!this.loggedUser) {
      this.router.navigate(['login'], {
        queryParams: { returnUrl: this.router.routerState.snapshot.url },
      });
    } else {
      this.subscribeHandler(
        this.articlesService.upVote('articles', this.singleArticle.id)
      );
    }
  }

  onDownVote(): void {
    if (!this.loggedUser) {
      this.router.navigate(['login'], {
        queryParams: { returnUrl: this.router.routerState.snapshot.url },
      });
    } else {
      this.subscribeHandler(
        this.articlesService.downVote('articles', this.singleArticle.id)
      );
    }
  }

  onFlag(): void {
    this.subscribeHandler(
      this.articlesService.flag('articles', this.singleArticle.id)
    );
  }

  onToggleFlagByAdmin(): void {
    this.subscribeHandler(
      this.articlesService.toggleFlagByAdmin('articles', this.singleArticle.id)
    );
    this.isFlagged = !this.isFlagged;
  }

  onLock(): void {
    this.subscribeHandler(
      this.articlesService.lock('articles', this.singleArticle.id)
    );
  }

  onDelete(): void {
    this.subscribeHandler(
      this.articlesService.delete('articles', this.singleArticle.id)
    );
  }

  onCommentEditContent(event: [number, string]): void {
    this.subscribeHandler(
      this.articlesService.editContent('comments', event[0], event[1])
    );
  }

  onCommentEditContentByAdmin(event: [number, string]): void {
    this.subscribeHandler(
      this.articlesService.editContentByAdmin('comments', event[0], event[1])
    );
  }

  onCommentUpVote(commentId: number): void {
    if (!this.loggedUser) {
      this.router.navigate(['login'], {
        queryParams: { returnUrl: this.router.routerState.snapshot.url },
      });
    } else {
      this.subscribeHandler(this.articlesService.upVote('comments', commentId));
    }
  }

  onCommentDownVote(commentId: number): void {
    if (!this.loggedUser) {
      this.router.navigate(['login'], {
        queryParams: { returnUrl: this.router.routerState.snapshot.url },
      });
    } else {
      this.subscribeHandler(
        this.articlesService.downVote('comments', commentId)
      );
    }
  }

  onCommentFlag(commentId: number): void {
    this.subscribeHandler(this.articlesService.flag('comments', commentId));
  }

  onCommentRemoveFlagByAdmin(commentId: number): void {
    this.subscribeHandler(
      this.articlesService.commentRemoveFlagByAdmin(commentId)
    );
  }

  onCommentDelete(commentId: number): void {
    this.subscribeHandler(this.articlesService.delete('comments', commentId));
  }

  editModeToggler() {
    this.editMode = !this.editMode;
  }

  editAdminToggler() {
    this.editAdmin = !this.editAdmin;
  }

  commentModeToggler() {
    this.commentMode = !this.commentMode;
  }

  private subscribeHandler(observable: Observable<any>): void {
    observable.subscribe(
      (data) => {
        this.articlesService.getById(this.singleArticle.id),
          this.notificator.success(data.message);
      },
      (error) =>
        this.router.navigate(['server-error'], {
          queryParams: {
            message: JSON.stringify(error.error),
            errorMsg: JSON.stringify(error.error.message),
            errorCode: error.status,
          },
        })
    );
  }
}
