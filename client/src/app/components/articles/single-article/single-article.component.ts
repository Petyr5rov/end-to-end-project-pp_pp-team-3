import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { ArticleShowDTO } from '../../../models/article/article-show-dto';
import { UserReturnDTO } from '../../../models/user/user-return-dto';

@Component({
  selector: 'app-single-article',
  templateUrl: './single-article.component.html',
  styleUrls: ['./single-article.component.scss'],
})
export class SingleArticleComponent implements OnInit {
  @Input()
  public article: ArticleShowDTO;
  @Input()
  public loggedUser: UserReturnDTO;

  public content = '';
  public showMore = false;
  public votes = 0;
  public contentToEdit: string;
  public editMode = false;
  public editAdmin = false;
  public isFlagged = false;

  @Output()
  public editContent = new EventEmitter<[number, string]>();

  @Output()
  public editContentByAdmin = new EventEmitter<[number, string]>();

  @Output()
  public upVote = new EventEmitter<number>();

  @Output()
  public downVote = new EventEmitter<number>();

  @Output()
  public flag = new EventEmitter<number>();

  @Output()
  public toggleFlagByAdmin = new EventEmitter<number>();

  @Output()
  public delete = new EventEmitter<number>();

  @Output()
  public lock = new EventEmitter<number>();

  ngOnInit() {
    this.votes =
      this.article.votes.filter((vote) => vote.positive === true).length -
      this.article.votes.filter((vote) => vote.positive === false).length;
    this.contentToEdit = this.article.content;
    if (
      this.article.content ===
      `This Article is flagged, please edit it's content or it will be deleted!`
    ) {
      this.isFlagged = true;
    }
  }

  addContent() {
    this.content = this.article.content.substr(0, 150);
    this.showMore = !this.showMore;
  }

  emitUpVote() {
    this.upVote.emit(this.article.id);
  }

  emitDownVote() {
    this.downVote.emit(this.article.id);
  }

  emitFlag() {
    this.flag.emit(this.article.id);
  }

  emitToggleFlagByAdmin() {
    this.toggleFlagByAdmin.emit(this.article.id);
    this.isFlagged = !this.isFlagged;
  }

  emitDelete() {
    this.delete.emit(this.article.id);
  }

  emitLock() {
    this.lock.emit(this.article.id);
  }

  emitEdit() {
    this.editMode = false;
    if (this.contentToEdit.charCodeAt(0) === 10) {
      this.contentToEdit = '';
    }
    this.contentToEdit.trim();
    this.article.content = this.contentToEdit;
    this.editContent.emit([this.article.id, this.contentToEdit]);
  }

  emitEditByAdmin() {
    this.editAdmin = false;
    if (this.contentToEdit.charCodeAt(0) === 10) {
      this.contentToEdit = '';
    }
    this.contentToEdit.trim();
    this.article.content = this.contentToEdit;
    this.editContentByAdmin.emit([this.article.id, this.contentToEdit]);
  }

  editModeToggler() {
    this.editMode = !this.editMode;
  }

  editAdminToggler() {
    this.editAdmin = !this.editAdmin;
  }
}
