import { Component, OnInit, OnDestroy } from '@angular/core';
import { ArticlesService } from '../../core/services/articles.service';
import { ArticleShowDTO } from '../../models/article/article-show-dto';
import { Observable, Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { UserReturnDTO } from '../../models/user/user-return-dto';
import { AuthService } from '../../core/services/auth.service';
import { PageEvent } from '@angular/material/paginator';
import { NotificatorService } from '../../core/services/notificator.service';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss'],
})
export class ArticlesComponent implements OnInit, OnDestroy {
  public articles: ArticleShowDTO[];
  public loggedUser: UserReturnDTO;
  private articlesSub: Subscription;
  private loggedUserSub: Subscription;
  private routeSub: Subscription;
  public allArticlesCount: number;
  public currentPage = 1;
  private navigatedQuery = '';
  public pageEvent: PageEvent;
  private returnArticleId: number;
  public createMode = false;
  public newArticleContent = '';
  public newArticleTitle = '';
  public titlePlaceholder = 'New Title...';
  public contentPlaceholder = 'New Content...';

  constructor(
    private readonly route: ActivatedRoute,
    private readonly articlesService: ArticlesService,
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly notificator: NotificatorService
  ) {}

  ngOnInit(): void {
    this.routeSub = this.route.queryParams.subscribe(
      (params) => (
        params.create
          ? ((this.createMode = true), this.router.navigate(['/home']))
          : true,
        params.getFlagged
          ? this.articlesService.getFlagged()
          : ((this.navigatedQuery = Object.keys(params)
              .map((key) => key + '=' + params[key])
              .join('&')),
            this.articlesService.all(this.navigatedQuery))
      )
    );
    this.loggedUserSub = this.authService.loggedUser$.subscribe(
      (data) => (this.loggedUser = data)
    );
    this.articlesSub = this.articlesService.articlesSource$.subscribe(
      (data) => ([this.articles, this.allArticlesCount] = data)
    );
  }

  ngOnDestroy(): void {
    this.loggedUserSub.unsubscribe();
    this.articlesSub.unsubscribe();
    this.routeSub.unsubscribe();
  }

  public changePage(event: any) {
    if (event.pageIndex === this.currentPage) {
      this.router.navigate(['/home'], {
        queryParams: { page: ++this.currentPage },
        queryParamsHandling: 'merge',
      });
    }

    if (event.pageIndex + 2 === this.currentPage) {
      this.router.navigate(['/home'], {
        queryParams: { page: --this.currentPage },
        queryParamsHandling: 'merge',
      });
    }
  }

  postArticle(): void {
    if (!this.newArticleContent || !this.newArticleTitle) {
      this.newArticleTitle
        ? ''
        : (this.titlePlaceholder = 'Please, give it a Title!'),
        this.notificator.warn('Please, give it a Title!');
      this.newArticleContent
        ? ''
        : (this.contentPlaceholder = 'Please, put some content!'),
        this.notificator.warn('Please, put some content!');
    } else {
      this.createMode = false;
      this.subscribeHandler(
        this.articlesService.create(
          'articles',
          this.newArticleContent,
          this.newArticleTitle
        )
      );
      this.newArticleTitle = '';
      this.newArticleContent = '';
      this.titlePlaceholder = 'New Title...';
      this.contentPlaceholder = 'New Content...';
    }
  }

  onEditContent(event: [number, string]): void {
    this.returnArticleId = event[0];
    this.subscribeHandlerSingleArticle(
      this.articlesService.editContent('articles', event[0], event[1])
    );
  }

  onEditContentByAdmin(event: [number, string]): void {
    this.returnArticleId = event[0];
    this.subscribeHandlerSingleArticle(
      this.articlesService.editContentByAdmin('articles', event[0], event[1])
    );
  }

  onUpVote(articleId: number): void {
    if (!this.loggedUser) {
      this.router.navigate(['login'], {
        queryParams: { returnUrl: this.router.routerState.snapshot.url },
      });
    } else {
      this.subscribeHandler(this.articlesService.upVote('articles', articleId));
    }
  }

  onDownVote(articleId: number): void {
    if (!this.loggedUser) {
      this.router.navigate(['login'], {
        queryParams: { returnUrl: this.router.routerState.snapshot.url },
      });
    } else {
      this.subscribeHandler(
        this.articlesService.downVote('articles', articleId)
      );
    }
  }

  onFlag(articleId: number): void {
    this.subscribeHandler(this.articlesService.flag('articles', articleId));
  }

  onToggleFlagByAdmin(articleId: number): void {
    this.subscribeHandler(
      this.articlesService.toggleFlagByAdmin('articles', articleId)
    );
  }
  onDelete(articleId: number): void {
    this.subscribeHandler(this.articlesService.delete('articles', articleId));
  }
  onLock(articleId: number): void {
    this.subscribeHandler(this.articlesService.lock('articles', articleId));
  }

  createModeToggler() {
    this.createMode = !this.createMode;
  }

  private subscribeHandler(observable: Observable<any>): void {
    observable.subscribe(
      (data) => {
        this.articlesService.all(), this.notificator.success(data.message);
      },
      (error) =>
        this.router.navigate(['server-error'], {
          queryParams: {
            message: JSON.stringify(error.error),
            errorMsg: JSON.stringify(error.error.message),
            errorCode: error.status,
          },
        })
    );
  }

  private subscribeHandlerSingleArticle(observable: Observable<any>): void {
    observable.subscribe(
      (data) => {
        this.router.navigate([`/article/${this.returnArticleId}`]),
          this.notificator.success(data.message);
      },
      (error) =>
        this.router.navigate(['server-error'], {
          queryParams: {
            message: JSON.stringify(error.error),
            errorMsg: JSON.stringify(error.error.message),
            errorCode: error.status,
          },
        })
    );
  }
}
