import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SingleArticleComponent } from './single-article/single-article.component';
import { ArticlesComponent } from './articles.component';
import { ArticlesService } from '../../core/services/articles.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { MaterialModule } from '../../shared/material/material.module';
import { FormsModule } from '@angular/forms';




@NgModule({
  declarations: [ SingleArticleComponent, ArticlesComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    SharedModule,
    MaterialModule,
    RouterModule,
    FormsModule
  ],
  providers: [ArticlesService],
  exports: [ArticlesComponent]
})
export class ArticlesModule { }
