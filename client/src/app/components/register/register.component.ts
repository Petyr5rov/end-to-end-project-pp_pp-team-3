import { Component, OnInit } from '@angular/core';
import { FormGroup,  Validators, FormBuilder } from '@angular/forms';
import { AuthService } from '../../core/services/auth.service';
import { NotificatorService } from '../../core/services/notificator.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public urlReturn = '' ;
  public registerForm: FormGroup;
  constructor(
    private readonly fb: FormBuilder,
    private readonly authService: AuthService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
   ) {
    this.registerForm = this.fb.group({
      username: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(20)])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(20), Validators.pattern(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/)])],
    });
   }

   ngOnInit() {
    this.route.queryParams
        .subscribe(params => this.urlReturn = params.returnUrl || '/');
   }


  public register( ) {
    this.authService.register(this.registerForm.value)
    .subscribe(
      () => {
        this.notificator.success(`Registration successful!`);
        // then login
        this.authService.login(this.registerForm.value)
        .subscribe(
          () => {
            this.notificator.success(`Login successful!`);
            this.router.navigateByUrl(this.urlReturn);
          },
          () => this.notificator.error(`Invalid email/password!`),
        );

      },
      () => this.notificator.error(`There was an error processing the request!`),
    );
  }
}
