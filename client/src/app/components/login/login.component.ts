import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from '../../core/services/auth.service';
import { NotificatorService } from '../../core/services/notificator.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {

  public urlReturn = '' ;
  public loginForm: FormGroup;
  constructor(
    private readonly authService: AuthService,
    private readonly notificator: NotificatorService,
    private readonly fb: FormBuilder,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
  ) {
    this.loginForm = this.fb.group({
      username: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(4)])],
    });
  }

 ngOnInit() {
  this.route.queryParams
      .subscribe(params => this.urlReturn = params.returnUrl || '/');
 }

 login() {
    this.authService.login(this.loginForm.value)
    .subscribe(
      () => {
        this.notificator.success(`Welcome back ${this.loginForm.value.username}!`);
        this.router.navigateByUrl(this.urlReturn);
      },
      (e) => {
        if (e.error.error.includes('BANNED')) {
          this.notificator.error(`Sorry, you are BANNED! Contact us for more information!`);
          this.router.navigate(['/']);
        } else {
          this.notificator.error(`${e.error.error}!`);
        }
      },
    );
  }
}
