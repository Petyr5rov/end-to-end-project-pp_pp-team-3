import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../../../core/services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserReturnDTO } from '../../../models/user/user-return-dto';
import { NotificatorService } from '../../../core/services/notificator.service';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../core/services/auth.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css'],
})
export class UsersListComponent implements OnInit, OnDestroy {
  public users: UserReturnDTO[];
  private loggedUserSub: Subscription;
  public loggedUser: UserReturnDTO;

  constructor(
    private readonly userService: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private readonly authService: AuthService,
    private readonly notificator: NotificatorService
  ) {}

  ngOnInit(): void {
    const userId = this.route.snapshot.params.id;
    if (userId) {
      this.userService.getUserById(userId).subscribe(
        (data) => (this.users = [data]),
        (error) =>
        this.router.navigate(['login'], {
          queryParams: { returnUrl: this.router.routerState.snapshot.url },
        })
      );
    } else {
      this.userService.getAll().subscribe({
        next: (data: any) => (this.users = data),
        error: (error: any) =>
          this.router.navigate(['server-error'], {
            queryParams: {
              message: JSON.stringify(error.error),
              errorMsg: JSON.stringify(error.error.message),
              errorCode: error.status,
            },
          }),
      });
    }
    this.loggedUserSub = this.authService.loggedUser$.subscribe(
      (data) => (this.loggedUser = data)
    );
  }

  ngOnDestroy(): void {
    this.loggedUserSub.unsubscribe();
  }

  onUserDeleted(user: UserReturnDTO) {
    this.userService.delateUser(user.id).subscribe(
      () => {
        this.notificator.success(`Delete successful!`);
      },
      (e) => {
        this.notificator.error(`Something is wrong`);
      }
    );
    this.users = this.users.filter((u) => u.id !== user.id);
  }

  onUserBanned(user: UserReturnDTO) {
    this.userService.banUser(user.id).subscribe(
      () => {
        if (user.isBanned) {
          this.notificator.success(`Banned successful!`);
        } else {
          this.notificator.success(`Unbanned successful!`);
        }
      },
      (e) => {
        this.notificator.error(`Something is wrong`);
      }
    );
    this.users[this.users.findIndex((u) => u.id === user.id)].isBanned = !this
      .users[this.users.findIndex((u) => u.id === user.id)].isBanned;
  }

  onRole(user: UserReturnDTO) {
    this.userService.editUserRole(user.role, user.id).subscribe(
      () => {
        (this.users[user.id - 1] = user),
          this.notificator.success(`User Role change successful!`);
      },
      (e) => {
        this.notificator.error(
          `Something is wrong! The roles can be Admin, Editor and Guest`
        );
      }
    );
    this.router.navigate(['users/allUsers'], {});
  }

}
