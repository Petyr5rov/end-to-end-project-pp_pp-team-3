import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { UserReturnDTO } from '../../models/user/user-return-dto';
import { UserService } from '../../core/services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FriendsReturnDTO } from '../../models/friends/friends-return-dto';
import { NotificatorService } from '../../core/services/notificator.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  @Input()
  public user: UserReturnDTO;

  @Input()
  public loggedUser: UserReturnDTO;

  @Output()
  userDeleted: EventEmitter<UserReturnDTO> = new EventEmitter<UserReturnDTO>();

  @Output()
  userBanned: EventEmitter<UserReturnDTO> = new EventEmitter<UserReturnDTO>();

  @Output()
  userRole: EventEmitter<UserReturnDTO> = new EventEmitter<UserReturnDTO>();

  public role: string;
  public isFriends: FriendsReturnDTO;
  private roles: any[] = [{ value: 'ADMIN' }, { value: 'EDITOR' }, { value: 'GUEST' }];

  constructor(
    private readonly userService: UserService,
    private readonly notificator: NotificatorService
  ) {}

  ngOnInit() {
    this.userService.loadFriends().subscribe((data) => {
      data.length !== 0
        ? (this.isFriends = data.find(
            (friend) => friend.user2.id === this.user.id
          ))
        : '';
    });
  }

  public deleteUser() {
    this.userDeleted.emit(this.user);
  }

  public banUser() {
    this.userBanned.emit(this.user);
  }

  public changeRole() {
    if (this.user.id === this.loggedUser.id) {
      this.notificator.error(`You cant edit your Role, ask another Admin`);
    } else {
      this.user.role = this.role.toUpperCase();
      this.userRole.emit(this.user);
      this.role = '';
    }
  }



  addFriend() {
    this.userService.addFriend(this.user.id).subscribe(
      () => {
        this.isFriends = { user2: this.user, pending: false };
        this.notificator.success(`Friendship added successfully!`);
      },
      () => {
        this.notificator.error(`Something is wrong!`);
      }
    );
  }

  removeFriend() {
    this.userService.deleteFriend(this.user.id).subscribe(
      () => {
        (this.isFriends = undefined),
          this.notificator.success(`Friendship removed successfully!`);
      },
      () => {
        this.notificator.error(`Something is wrong`);
      }
    );
  }

  acceptFriend() {
    this.userService.acceptFriend(this.user.id).subscribe(
      () => {
        (this.isFriends.pending = false),
          this.notificator.success(`Friendship accepted successfully!`);
      },
      (e) => {
        this.notificator.error(`Something is wrong`);
      }
    );
  }
}
