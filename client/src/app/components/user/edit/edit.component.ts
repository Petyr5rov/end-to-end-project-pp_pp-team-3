import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { UserReturnDTO } from '../../../models/user/user-return-dto';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../../../core/services/user.service';
import { NotificatorService } from '../../../core/services/notificator.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserUpdateDTO } from '../../../models/user/user-update-dto';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../core/services/auth.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  private userSubscription: Subscription;
  user: UserReturnDTO;
  public editForm: FormGroup;


  userEdit: UserUpdateDTO = {};
  urlReturn = '' ;
  constructor(
    private readonly fb: FormBuilder,
    private readonly userService: UserService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly authService: AuthService,
    private cd: ChangeDetectorRef
  ) {
    const reg = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
    this.editForm = this.fb.group({
      avatarUrl: ['', [Validators.required, Validators.pattern(reg)]],
    });
   }

  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => this.urlReturn = params.returnUrl || '/');

      this.userSubscription = this.authService.loggedUser$.subscribe(
        (user) => (this.user = user)
      );
  }

   editUserProfil( ) {
    this.userService.edit(this.editForm.value)
    .subscribe(
      () => {
        this.notificator.success(`Edit profil successful!`);
       this.router.navigateByUrl(this.urlReturn);
       this.authService.loggedUser$.subscribe(data => data.avatarUrl = this.editForm.value.avatarUrl)
      },
      () => this.notificator.error(`There was an error processing the request!`),
    );
  }
  onFileChange(event) {
    const reader = new FileReader();
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {
        this.editForm.patchValue({
          file: reader.result
        });
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      };
    }
  }

}
