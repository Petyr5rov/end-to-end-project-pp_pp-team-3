import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-ditails',
  templateUrl: './ditails.component.html',
  styleUrls: ['./ditails.component.scss'],
})
export class DitailsComponent implements OnInit {
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
        this.router.navigate(['/home'], {
          queryParams: { userId: +params.id },
        });
    });
  }
}
