import { Component, OnInit, Input } from '@angular/core';
import { UserReturnDTO } from '../../../models/user/user-return-dto';
import { UserService } from '../../../core/services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ActivityDTO } from '../../../models/activity.dto';
import { NotificatorService } from '../../../core/services/notificator.service';
import { FriendsReturnDTO } from '../../../models/friends/friends-return-dto';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css'],
})
export class FriendsComponent implements OnInit {
  @Input() user: UserReturnDTO;

  userFriends: FriendsReturnDTO[];
  activityUser: ActivityDTO;
  failedActivity: string;
  activityClick = false;
  isFriends = true;
  usernameActivity: string;
  constructor(
    private readonly userService: UserService,
    private router: Router,
    private actRouter: ActivatedRoute,
    private readonly notificator: NotificatorService
  ) {}

  ngOnInit(): void {
    this.userService.loadFriends().subscribe(
      (data) => {
        data.length === 0
          ? this.router.navigate(['not-found'])
          : (this.userFriends = data);
      },
      (error) =>
        this.router.navigate(['server-error'], {
          queryParams: {
            message: JSON.stringify(error.error),
            errorMsg: JSON.stringify(error.error.message),
            errorCode: error.status,
          },
        })
    );
  }

  selectUserHendler(userId: number) {
    this.router.navigate(['/users/details'], { queryParams: { id: userId } });
  }

  activity(userId: number, username: string) {
    this.activityClick = true;
    this.usernameActivity = username;
    this.userService.getUserActivity(+userId).subscribe(
      (data) => (this.activityUser = data),
      (error) => {
        (this.activityUser = undefined),
          (this.failedActivity = error.error.error);
      }
    );
  }

  clear() {
    this.activityClick = !this.activityClick;
  }

  cancelFriends(id: number) {
    this.isFriends = !this.isFriends;
    this.actRouter.params.subscribe((param) => {
      this.userService.deleteFriend(id).subscribe(
        () => {
          this.notificator.success(`Delete friends successful!`);
        },
        (e) => {
          this.notificator.error(`Something is wrong`);
        }
      );
      this.userFriends = this.userFriends.filter((u) => u.user2.id !== id);
      this.router.navigate([`/users/${param.id}/friends`], {});
    });
  }
}
