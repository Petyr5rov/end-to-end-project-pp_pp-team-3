import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user.component';
import { RouterModule } from '@angular/router';
import { FriendsComponent } from './friends/friends.component';
import { SharedModule } from '../../shared/shared.module';
import { MaterialModule } from '../../shared/material/material.module';
import { EditComponent } from './edit/edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { UsersListComponent } from './users-list/users-list.component';
import { DitailsComponent } from './friends/ditails/ditails.component';
import { AdminGuard } from '../../core/guard/admin.guard';




@NgModule({
  declarations: [UserComponent, FriendsComponent, EditComponent, UsersListComponent,DitailsComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    HttpClientModule,
    SharedModule,
    MaterialModule,
    RouterModule.forChild([
      { path: ':id/friends',  component: FriendsComponent },
      { path: ':id/edit',  component: EditComponent },
      { path: 'user/:id',  component: UsersListComponent },


      { path: 'allUsers',  component: UsersListComponent , canActivate: [AdminGuard]},

      { path: 'details',  component: DitailsComponent },
    ])
  ],
  exports: [RouterModule ],
})
export class UserModule { }

